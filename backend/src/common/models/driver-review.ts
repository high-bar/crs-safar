import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const DriverReviewSchema = new Schema( {
  scoreOfFive: {
    type:Number
  },
  comment: {
    type:String
  },
  dateTime: {
    type:Date
  },
  Driver: {
    type:Schema.Types.ObjectId, ref:'Driver'
  },
});
