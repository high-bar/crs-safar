export enum TransactionType {
  TRIP = 'trip',
  PAYMENT = 'payment',
  BONUS = 'bonus',
  TRIP_EXTRA = 'trip-extra-costs'
}
