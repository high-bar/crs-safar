import { Injectable } from '@angular/core';
import { UserRole } from '@common/enums';
import { Car, CarFilter } from '@common/models';
import { firestore, functions } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CarManagementService {
  constructor() {}

  public addNewCar(car: Partial<Car>): Promise<firestore.DocumentData> {
    car.joiningDate = new Date().getTime();

    return firestore()
      .collection('cars')
      .add(car);
  }

  public getCars(carFilter?: CarFilter): Observable<firestore.QuerySnapshot> {
    let query: firestore.Query = firestore().collection('cars');
    if (!!carFilter) {
      if (!!carFilter.brand) {
        query = query.where('brand', '==', carFilter.brand);
      }
      if (!!carFilter.model) {
        query = query.where('model', '==', carFilter.model);
      }
      if (!!carFilter.color) {
        query = query.where('color', '==', carFilter.color);
      }
      if (!!carFilter.plateNumber) {
        query = query.where('plateNumber', '==', carFilter.plateNumber);
      }
      if (!!carFilter.type) {
        query = query.where('type', '==', carFilter.type);
      }
      if (!!carFilter.status) {
        query = query.where('status', '==', carFilter.status);
      }
    }
    return Observable.create(
      (observer: Observer<Partial<firestore.QuerySnapshot>>) => {
        query.orderBy('joiningDate', 'desc').onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      }
    );
  }

  public searchCars(query: string): Promise<functions.HttpsCallableResult> {
    const freeSearch = functions().httpsCallable('freeSearch');
    return freeSearch({ query: query, collection: 'cars' });
  }

  public deleteCar(carId: string): Promise<void> {
    return firestore()
      .collection('cars')
      .doc(carId)
      .delete();
  }

  public editCar(car?: Partial<Car>): Promise<void> {
    return firestore()
      .collection('cars')
      .doc(car.id)
      .update(car);
  }

  public getGarageCars(garageId: string): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('cars')
        .where('garage.id', '==', garageId)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getGarageDrivers(
    garageId: string
  ): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('users')
        .where('garage.id', '==', garageId)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }
}
