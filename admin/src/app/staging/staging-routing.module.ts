import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'send-notifications',
    loadChildren:
      () => import('./send-notifications/send-notifications.module').then(m => m.SendNotificationsPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StagingRoutingModule {}
