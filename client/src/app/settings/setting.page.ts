import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language } from '@common/enums';
import { Localization } from '@common/models';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {
  public documentDir = 'ltr';
  public selectedLocalization: Localization = this.localizationService.getLocalization();
  public appVersionNumber: string;
  public isAuthorized: boolean;

  constructor(
    private appVersion: AppVersion,
    private localizationService: LocalizationService,
    public authService: AuthService
  ) {
    LocalizationService.localizationChange.subscribe(
      (newLocalization: Localization) => {
        this.documentDir = newLocalization.dir;
      }
    );

    this.authService.change.subscribe(user => {
      this.isAuthorized = !!user;
    });
    this.appVersion
      .getVersionNumber()
      .then(appVersionNumber => (this.appVersionNumber = appVersionNumber));
  }

  public changeLanguage(newLanguage: Language): void {
    this.localizationService.setLocalization({ lang: newLanguage });
  }
}
