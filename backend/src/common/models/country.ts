import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const CountrySchema = new Schema( {
  code: {
    type:String
  },
  name: {
    type:String
  },
  states: {
    type:Array
  }
});
