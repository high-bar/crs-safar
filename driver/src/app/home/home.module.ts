import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TripPageModule } from '@app/shared/pages/trip/trip.module';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { PickOnMapModalComponent } from './components/pick-on-map-modal/pick-on-map-modal.component';
import { PlaceSearchSelectComponent } from './components/place-search-select/place-search-select.component';
import { HomeRoutingModule } from './home-routing.module';
import { LoginPage } from './pages/login/login.page';
import { OverviewPage } from './pages/overview/overview.page';
import { SignUpPage } from './pages/sign-up/sign-up.page';
import { GoogleMapsService } from './services/google-maps/google-maps.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TripPageModule,
    HomeRoutingModule,
  ],
  declarations: [
    LoginPage,
    SignUpPage,
    PlaceSearchSelectComponent,
    OverviewPage,
    PickOnMapModalComponent,
    ForgotPasswordComponent,
  ],
  entryComponents: [PickOnMapModalComponent, ForgotPasswordComponent],
  providers: [GoogleMapsService],
})
export class HomePageModule {}
