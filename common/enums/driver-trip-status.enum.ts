export enum DriverTripStatus {
  QUEUED = 'queued',
  MEETING_CLIENT = 'meeting client',
  PICKING_CLIENT = 'picking client',
  STARTED = 'started',
  RETURNING = 'returning',
  FINISHED = 'finished',
}
