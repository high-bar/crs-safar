import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { NewDriversComponent } from './components/new-drivers/new-drivers.component';
import { DriversManagementPage } from './drivers-management.page';

const routes: Routes = [
  {
    path: '',
    component: DriversManagementPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DriversManagementPage, NewDriversComponent],
  entryComponents: [DriversManagementPage, NewDriversComponent],
})
export class DriversManagementPageModule {}
