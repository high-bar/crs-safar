import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CarManagementService } from '@app/car-management/services/car-management.service';
import { GarageService } from '@app/garages-management/services/garage.service';
import { CarStatus, CarType } from '@common/enums';
import { Car, Garage } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.page.html',
  styleUrls: ['./add-car.page.scss'],
})
export class AddCarPage implements OnInit {
  public readonly CarTypeEnum: typeof CarType = CarType;
  public garages: Partial<Garage>[] = [];
  @Input() public car: Car;

  public addCarForm: FormGroup = this.formBuilder.group({
    brand: [undefined, [Validators.required]],
    model: [undefined, [Validators.required]],
    color: [undefined, [Validators.required]],
    colorArabic: [undefined, [Validators.required]],
    colorHex: ['#6a6a6a'],
    plateNumber: [undefined, [Validators.required]],
    type: [undefined, [Validators.required]],
    garage: [undefined, [Validators.required]],
    notes: [undefined],
  });

  constructor(
    private formBuilder: FormBuilder,
    private carManagementService: CarManagementService,
    private garageService: GarageService,
    private modalController: ModalController
  ) {
    this.loadGarages();
  }

  public isLoading: boolean;

  ngOnInit() {
    if (!!this.car) {
      // pass only values used in the form
      this.addCarForm.patchValue({
        brand: this.car.brand,
        model: this.car.model,
        color: this.car.color,
        colorArabic: this.car.colorArabic,
        colorHex: this.car.colorHex,
        plateNumber: this.car.plateNumber,
        type: this.car.type,
        garage: this.car.garage.id,
        notes: this.car.notes,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitAddCar(): Promise<void> {
    if (this.addCarForm.valid) {
      this.isLoading = true;
      this.addCarForm.value.garage = this.garages.find(
        garage => this.addCarForm.value.garage === garage.id
      );

      if (!!this.car) {
        this.car = {
          ...this.car,
          ...this.addCarForm.value,
        };

        try {
          this.carManagementService.editCar(this.car);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      } else {
        try {
          this.addCarForm.value.status = CarStatus.GARAGE;
          this.carManagementService.addNewCar(this.addCarForm.value);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      }
    }
  }

  public async loadGarages(): Promise<void> {
    try {
      this.garages = (await this.garageService.getGaragesOnce()).docs.map(
        doc => {
          return { id: doc.id, name: doc.data().name };
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
}
