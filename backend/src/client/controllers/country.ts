import * as mongoose from 'mongoose';
import { CountrySchema } from '../../common/models/country';
import { Request, Response } from 'express';

const Country = mongoose.model('Country', CountrySchema);

export class CountryController {
  public getCountries(req: Request, res: Response) {
    // find Countries
    Country.find({}, function(err: any, Countries: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Countries);
      }
    }); // end find all countries
  }
  public SetCountry(req: Request, res: Response) {
    //set values
    let newCountry = new Country({
      code: req.body.code,
      name: req.body.name,
      states: req.body.states,
    });
    //save country
    newCountry.save((err, Country) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: "Country Added!",
        Country: Country,
      });
    }); //end save Country
  }
}
