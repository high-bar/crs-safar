import { Configs } from './configs.model';

export const CONFIGS: Configs = {
  pushOptions: {
    android: {
      forceShow: true,
      topics: ['client-users'],
      icon: 'fcm_push_icon',
      iconColor: '#00681e',
    },
  },
};
