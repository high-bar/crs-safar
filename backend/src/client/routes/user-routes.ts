import { ClientSignUpController } from '../controllers/signup';
import { ClientSignInController } from '../controllers/login';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class ClientRoutes {
  public ClientSignUpController: ClientSignUpController = new ClientSignUpController();
  public ClientSignInController: ClientSignInController = new ClientSignInController();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      let token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //
    // start client route
    // driver trips history
    app
      .route('/client/profile')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.ClientSignInController.UserProfile.bind(
        this.ClientSignInController
      ));
    ////
    // Create a new client
    app.route('/client/signup').post(this.ClientSignUpController.addClient);
    ////
    // client Login
    app.route('/client/signin').post(this.ClientSignInController.getClient);
    ////
    ///////////////
    //
    app
      .route('/client/tokenverify')
      .post(this.ClientSignInController.TokenVerify);
    ////
    //upload profile image
    app
      .route('/client/updateprofilepicture')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.ClientSignInController.UploadProfile.bind(
        this.ClientSignInController
      ));
    ////
    // end client route
  }

  //end class
}
