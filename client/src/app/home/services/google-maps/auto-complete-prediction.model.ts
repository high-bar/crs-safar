// tslint:disable-next-line:no-empty-interface
export interface AutocompletePrediction
  extends Partial<google.maps.places.AutocompletePrediction> {}
