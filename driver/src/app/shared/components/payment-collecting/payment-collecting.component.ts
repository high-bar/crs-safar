import { Component, Input, OnInit } from '@angular/core';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { CarType } from '@common/enums';
import { SystemFlags, Trip } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-payment-collecting',
  templateUrl: './payment-collecting.component.html',
  styleUrls: ['./payment-collecting.component.scss'],
})
export class PaymentCollectingComponent implements OnInit {
  @Input() public trip: Trip;
  public systemFlags: SystemFlags;
  public isCostsLoading = false;

  public actualPayment = 0;
  public isLoading = false;

  constructor(
    private tripsService: TripsService,
    private modalCtrl: ModalController
  ) {
    this.loadSystemFlags();
  }

  ngOnInit() {}

  private loadSystemFlags(): void {
    this.isCostsLoading = true;
    this.tripsService.getSystemFlags().subscribe(response => {
      this.systemFlags = response.data() as SystemFlags;
      this.isCostsLoading = false;
    });
  }

  public getExtraCosts(): number {
    let extraDistanceMovedInKm =
      this.trip.route[this.trip.route.length - 1].distanceMovedInKm -
      this.trip.distanceMatrix.distance.value;
    extraDistanceMovedInKm =
      extraDistanceMovedInKm < 0 ? 0 : extraDistanceMovedInKm;
    this.actualPayment =
      extraDistanceMovedInKm *
      (this.trip.carType === CarType.SUV
        ? this.systemFlags.kmPriceSUV
        : this.systemFlags.kmPrice);
    return this.actualPayment;
  }

  public async submitExtraPayment(): Promise<void> {
    this.isLoading = true;
    try {
      this.trip.pricing.actualExtra = this.getExtraCosts();
      this.trip.pricing.collectedExtra = this.actualPayment;
      await this.modalCtrl.dismiss(this.trip);
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }
}
