import * as mongoose from 'mongoose';
import { TripSchema } from '../../common/models/trip';
import { Request, Response } from 'express';
const https = require('https');

const Trip = mongoose.model('Trip', TripSchema);

export class TripController {
  constructor() {}
  // assigntrip
  public AssignTrip(req: Request, res: Response) {
    let tripid = req.body.tripid;
    let status = 'next';
    let driver = req.body.driverid;
    let car = req.body.carid;
    let priceInEGP = req.body.price;
    //Update
    Trip.findOneAndUpdate(
      { _id: tripid },
      {
        $set: {
          status: status,
          driver: driver,
          car: car,
          priceInEGP: priceInEGP,
        },
      },
      function(err, Trip) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json({
            message: 'Trip Updated!',
            Trip: Trip,
          });
        }
      }
    );
  } //end
  //
  public DeleteTrip(req: Request, res: Response) {
    let tripid = req.body.tripid;
    //delete
    Trip.findByIdAndRemove({ _id: tripid }, function(err, Trip) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json({
          message: 'Trip Deleted!',
          Trip: Trip,
        });
      }
    });
  }
  //

  ///////////////////////
  public GetTrips1(req: Request, res: Response) {
    Trip.find({}, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetTrips2(req: Request, res: Response) {
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({})
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  //////////////////////
  //////////////////////
  // Get Trip Details //
  public async adminTripDetails(req: Request, res: Response): Promise<void> {
    let tripid = req.body.tripid;
    Trip.find({_id: tripid}, function(err, trip){
      if(err){
        res.status(401).json({
          error: err,
        });
      }
      res.json(trip);
    });
  }
  // get all next trips with index//
  ///////////////////////
  public GetNextTrips1(req: Request, res: Response) {
    let status = 'next'
    Trip.find({status: status}, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetNextTrips2(req: Request, res: Response) {
    let status = 'next'
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({status: status})
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  //////////////////////
  // get all pending trips with index//
  ///////////////////////
  public GetPendingTrips1(req: Request, res: Response) {
    let status = 'pending'
    Trip.find({status: status}, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetPendingTrips2(req: Request, res: Response) {
    let status = 'pending'
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({status: status})
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  //////////////////////
  // get all Active trips with index//
  ///////////////////////
  public GetActiveTrips1(req: Request, res: Response) {
    let status = 'active'
    Trip.find({status: status}, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetActiveTrips2(req: Request, res: Response) {
    let status = 'active'
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({status: status})
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  //////////////////////
///////////////////////
public GetTripsDriverStatus1(req: Request, res: Response) {
  let driverStatus = req.body.driverstatus;
  Trip.find({driverStatus: driverStatus}, function(err: any, Trip: any) {
    if (err) {
      res.status(401).json({
        error: err,
      });
    } else {
      res.json(Trip);
    }
  })
    .limit(10)
    .sort({ _id: -1 }); // end find all
}
/////////////////////////////
public GetTripsDriverStatus2(req: Request, res: Response) {
  let driverStatus = req.body.driverstatus;
  let limit = parseInt(req.body.limit) || 10;
  let skip = parseInt(req.body.index) || 10;
  // find
  Trip.find({driverStatus: driverStatus})
    .skip(skip)
    .limit(limit)
    .sort({ _id: -1 })
    .exec(function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    }); // end find all
}
//////////////////////

} //end trip
