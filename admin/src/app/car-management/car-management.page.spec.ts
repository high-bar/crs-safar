import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarManagementPage } from './car-management.page';

describe('CarManagementPage', () => {
  let component: CarManagementPage;
  let fixture: ComponentFixture<CarManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarManagementPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
