import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { Driver } from '@common/models';
import { ModalController } from '@ionic/angular';

import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public user: Driver;
  public isEditing = false;

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private modalController: ModalController
  ) {
    this.authService.change.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {}

  public async openEditProfileModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: EditProfileComponent,
      componentProps: {
        profileData: this.user,
      },
    });
    return await modal.present();
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      this.user.pictureURL = window['Ionic'].WebView.convertFileSrc(
        await this.profileService.initPhotoPicking()
      );
    } catch (_error) {}
  }
}
