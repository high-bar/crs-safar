import { Injectable } from '@angular/core';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class DriverService {
  constructor() {}

  public getDriverByID(uid): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('users')
      .doc(uid)
      .get();
  }
}
