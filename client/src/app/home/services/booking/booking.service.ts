import { Injectable } from '@angular/core';
import { TripStatus } from '@common/enums';
import { Trip } from '@common/models';
import { firestore, Observer } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookingService {
  constructor() {}

  public book(
    tripRequest: Partial<Trip>
  ): Promise<firestore.DocumentReference> {
    tripRequest.status = TripStatus.NEW;
    tripRequest.requestDateTime = new Date().getTime();
    return firestore()
      .collection('trips')
      .add(tripRequest);
  }

  public getSystemFlags(): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('util')
          .doc('flags')
          .onSnapshot(
            querySnapshot => {
              observer.next(querySnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }

  public getCarInfo(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('cars')
        .where('type', '==', 'Luxury')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }
}
