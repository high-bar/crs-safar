import { PushOptions } from '@ionic-native/push/ngx';

export interface Configs {
  pushOptions: PushOptions;
}
