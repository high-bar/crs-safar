import { Component, Input, OnInit } from '@angular/core';
import { Complain } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-complain-details',
  templateUrl: './complain-details.page.html',
  styleUrls: ['./complain-details.page.scss'],
})
export class ComplainDetailsPage implements OnInit {
  @Input() public complain: Complain;

  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }
}
