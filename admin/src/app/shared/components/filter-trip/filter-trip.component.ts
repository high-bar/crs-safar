import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrentDriversComponent } from '@app/drivers-management/components/current-drivers/current-drivers.component';
import { CurrentClientsComponent } from '@app/shared/components/current-clients/current-clients.component';
import { UtilService } from '@app/shared/services/util/util.service';
import { Driver, TripFilter } from '@common/models';
import { Client } from '@common/models';
import { ModalController } from '@ionic/angular';
import { format } from 'date-fns';

@Component({
  selector: 'app-filter-trip',
  templateUrl: './filter-trip.component.html',
  styleUrls: ['./filter-trip.component.scss'],
})
export class FilterTripComponent implements OnInit {
  public states: string[] = [];
  public readonly dateFormat = 'YYYY-MM-DD';
  public readonly timeDisplayFormat = 'hh:mm A';
  public readonly timeFormat = 'HH:mm';
  public isLoading: boolean;
  public selectedDriver: Driver;
  public selectedClient: Client;
  @Input() public value: TripFilter;

  public filterTripForm: FormGroup = this.formBuilder.group({
    client: [undefined],
    driver: [undefined],
    fromPrice: [undefined],
    toPrice: [undefined],
    fromDate: [undefined],
    toDate: [undefined],
    fromState: [undefined],
    toState: [undefined],
  });

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private utilService: UtilService
  ) {
    this.loadCountryStates();
  }

  ngOnInit() {
    if (!!this.value) {
      // pass only values used in the form
      const fromRequestDateTime = !!this.value.fromRequestDateTime
        ? format(new Date(this.value.fromRequestDateTime), this.dateFormat)
        : undefined;
      const toRequestDateTime = !!this.value.toRequestDateTime
        ? format(new Date(this.value.toRequestDateTime), this.dateFormat)
        : undefined;

      this.selectedDriver = this.value.driver;
      this.selectedClient = this.value.client;
      this.filterTripForm.patchValue({
        client: this.selectedClient,
        driver: this.selectedDriver,
        fromPrice: this.value.fromPrice,
        toPrice: this.value.toPrice,
        fromDate: fromRequestDateTime,
        toDate: toRequestDateTime,
        fromState: this.value.fromState,
        toState: this.value.toState,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async openCurrentClientsModal(): Promise<void> {
    const selectClientModal = await this.modalController.create({
      component: CurrentClientsComponent,
    });
    await selectClientModal.present();
    this.selectedClient = (await selectClientModal.onWillDismiss()).data;
    this.filterTripForm.patchValue({
      client: this.selectedClient,
    });
  }

  public async openCurrentDriversModal(): Promise<void> {
    const selectDriverModal = await this.modalController.create({
      component: CurrentDriversComponent,
      componentProps: {
        isModal: true,
      },
    });
    await selectDriverModal.present();
    this.selectedDriver = (await selectDriverModal.onWillDismiss()).data;
    this.filterTripForm.patchValue({
      driver: this.selectedDriver,
    });
  }

  public async submitFilterTrip(): Promise<void> {
    const tripFilter: TripFilter = {
      driver: this.filterTripForm.value.driver,
      client: this.filterTripForm.value.client,
      fromPrice: this.filterTripForm.value.fromPrice,
      toPrice: this.filterTripForm.value.toPrice,
      fromRequestDateTime: new Date(
        this.filterTripForm.value.fromDate
      ).getTime(),
      toRequestDateTime: new Date(this.filterTripForm.value.toDate).getTime(),
      fromState: this.filterTripForm.value.fromState,
      toState: this.filterTripForm.value.toState,
    };

    await this.modalController.dismiss(tripFilter);
  }
}
