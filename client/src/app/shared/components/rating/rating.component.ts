import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {
  @Input() public value = 1;
  @Input() public readOnly = false;
  @Input() public user: Partial<User>;
  public isLoading: boolean;

  public reviewForm: FormGroup = this.formBuilder.group({
    rate: [this.value, [Validators.required]],
    comment: [undefined],
  });

  constructor(
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalCtrl.dismiss();
  }

  public async submitReview() {
    // this.isLoading = true;
    // if (this.reviewForm.valid) {
    console.log(this.reviewForm.value);
    //   this.isLoading = false;
    // }
  }
}
