import { Component } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language } from '@common/enums';
import { Localization } from '@common/models';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {
  public selectedLocalization: Localization;
  public appVersionNumber: string;

  constructor(
    private appVersion: AppVersion,
    private localizationService: LocalizationService,
    public authService: AuthService
  ) {
    this.selectedLocalization = this.localizationService.getLocalization();
    this.appVersion
      .getVersionNumber()
      .then(appVersionNumber => (this.appVersionNumber = appVersionNumber));
  }

  public changeLanguage(newLanguage: Language): void {
    this.localizationService.setLocalization({ lang: newLanguage });
  }
}
