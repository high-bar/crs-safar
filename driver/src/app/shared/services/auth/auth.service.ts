import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Driver } from '@common/models';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NavController } from '@ionic/angular';
import { auth, firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  private authChangeObserver: Observer<any>;
  public change: Observable<any>;

  constructor(
    private router: Router,
    private splashScreen: SplashScreen,
    private navController: NavController
  ) {
    let user: any = this.getSavedUser();
    if (!!user) {
      user = JSON.parse(user);
      this.onUserUpdate(user.uid);
    }

    this.change = Observable.create((observer: Observer<any>) => {
      this.authChangeObserver = observer;
      this.authChangeObserver.next(user);
    }).pipe(shareReplay());
    this.change.subscribe();

    auth().onAuthStateChanged(authUser => {
      user = undefined;
      if (!!authUser) {
        user = JSON.parse(this.getSavedUser());
      }
      this.authChangeObserver.next(user);
    });
  }

  private saveUser(user): void {
    if (!!user) {
      this.authChangeObserver.next(user);
      localStorage.setItem('user', JSON.stringify(user));
    }
  }

  public getSavedUser(): string {
    return localStorage.getItem('user');
  }

  public getUserOnce(uid: string): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('users')
      .doc(uid)
      .get();
  }

  private onUserUpdate(uid: string): void {
    firestore()
      .collection('users')
      .doc(uid)
      .onSnapshot(querySnapshot => {
        if (querySnapshot.exists) {
          this.saveUser(querySnapshot.data());
        }
      });
  }

  private addNewUser(user: Partial<Driver>): Promise<void> {
    user.joiningDate = new Date().getTime();

    return firestore()
      .collection('users')
      .doc(user.uid)
      .set(user);
  }

  public canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!!!this.getSavedUser()) {
      this.router.navigate(['home/login'], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
    return true;
  }

  public signUp(userData: Partial<Driver>): Promise<any> {
    return auth()
      .createUserWithEmailAndPassword(userData.email, userData.password)
      .then(async signUpData => {
        userData.uid = signUpData.user.uid;
        delete userData.password;
        delete userData['passwordConfirm'];
        delete userData['emailConfirm'];
        await this.addNewUser(userData);
        this.saveUser((await this.getUserOnce(userData.uid)).data());
        this.onUserUpdate(userData.uid);
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public login(loginReq: Partial<Driver>): Promise<any> {
    return auth()
      .signInWithEmailAndPassword(loginReq.email, loginReq.password)
      .then(async loginData => {
        try {
          this.saveUser((await this.getUserOnce(loginData.user.uid)).data());
          this.onUserUpdate(loginData.user.uid);
        } catch (error) {
          console.error(error);
          throw error;
        }
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public async logout(): Promise<void> {
    return auth()
      .signOut()
      .then(async () => {
        this.splashScreen.show();
        localStorage.clear();
        await this.navController.navigateRoot('home/login');
        this.splashScreen.hide();
      })
      .catch(error => {
        // An error happened.
      });
  }

  public resetPassword(email: string): Promise<void> {
    return auth().sendPasswordResetEmail(email);
  }

  public editProfile(profile?: Partial<Driver>): Promise<void> {
    return firestore()
      .collection('users')
      .doc(profile.uid)
      .update(profile);
  }
}
