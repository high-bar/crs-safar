import { Component, Input, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarManagementService } from '@app/car-management/services/car-management.service';
import { Car, Driver, Garage, Trip } from '@common/models';
import { IonSlides, ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-garage-details',
  templateUrl: './garage-details.page.html',
  styleUrls: ['./garage-details.page.scss'],
})
export class GarageDetailsPage {
  private _garage: Partial<Garage>;
  public isCarsLoading: boolean;
  public isDriversLoading: boolean;
  public cars: Partial<Car>[] = [];
  public drivers: Partial<Driver>[] = [];
  public currentTab = 0;
  @ViewChild('slider', { static: true }) slider: IonSlides;
  @Input() isModal = false;
  @Input() public select: string;
  @Input() public trip: Partial<Trip>;
  @Input() public set garage(newGarage: Partial<Garage>) {
    this._garage = newGarage;

    if (!!newGarage) {
      this.loadCarsData(newGarage.id);
      this.loadDriversData(newGarage.id);
    }
  }
  public get garage() {
    return this._garage;
  }

  constructor(
    private carManagementService: CarManagementService,
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute
  ) {
    if (!!this.activatedRoute.snapshot.params.garageId) {
      this.loadCarsData(this.activatedRoute.snapshot.params.garageId);
      this.loadDriversData(this.activatedRoute.snapshot.params.garageId);
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  public async loadCarsData(garageId: string): Promise<void> {
    this.isCarsLoading = true;
    this.carManagementService.getGarageCars(garageId).subscribe(
      data => {
        this.cars = data.docs.map(doc => {
          const car = doc.data();
          car.id = doc.id;
          return car;
        });
        this.isCarsLoading = false;
      },
      error => {
        console.error(error);
        this.isCarsLoading = false;
      }
    );
  }

  public async loadDriversData(garageId: string): Promise<void> {
    this.isDriversLoading = true;
    this.carManagementService.getGarageDrivers(garageId).subscribe(
      data => {
        this.drivers = data.docs.map(doc => {
          const driver = doc.data();
          driver.id = doc.id;
          return driver;
        });
        this.isDriversLoading = false;
      },
      error => {
        console.error(error);
        this.isDriversLoading = false;
      }
    );
  }

  public async selectDriver(driver: Driver): Promise<void> {
    if (!this.isModal) {
      await this.navController.navigateForward(['/driver/' + driver.uid], {
        relativeTo: this.route,
      });
    } else {
      if (!!this.trip) {
        driver.currentTripId = this.trip.id;
      }
      await this.modalController.dismiss(driver);
    }
  }

  public async selectCar(car: Car): Promise<void> {
    if (!this.isModal) {
    } else {
      if (!!this.trip) {
        car.currentTripId = this.trip.id;
      }
      await this.modalController.dismiss(car);
    }
  }
}
