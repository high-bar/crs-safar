import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Trip } from '@common/models';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.scss'],
})
export class TripComponent {
  @Input() public trip: Trip;

  constructor(private router: Router) {}

  public getStateParsedURL(stateName: string = ''): string {
    return `url(/assets/states/${stateName
      .replace(/\s/g, '_')
      .toLowerCase()}.jpeg)`;
  }
}
