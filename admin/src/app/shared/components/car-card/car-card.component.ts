import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Car } from '@common/models';

@Component({
  selector: 'app-car-card',
  templateUrl: './car-card.component.html',
  styleUrls: ['./car-card.component.scss'],
})
export class CarCardComponent implements OnInit {
  public isLoading: boolean;
  @Input() public car: Partial<Car>;
  @Output() public selected: EventEmitter<Partial<Car>> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public async selectCar(): Promise<void> {
    this.selected.emit(this.car);
  }
}
