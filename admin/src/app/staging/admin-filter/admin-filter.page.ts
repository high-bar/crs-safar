import { Component, OnInit } from '@angular/core';
import { addYears, format } from 'date-fns';
@Component({
  selector: 'app-admin-filter',
  templateUrl: './admin-filter.page.html',
  styleUrls: ['./admin-filter.page.scss'],
})
export class AdminFilterPage implements OnInit {
  public readonly dateFormat = 'YYYY-MM-DD';
  public readonly maxPickupDate: string = format(
    addYears(new Date(), 1),
    this.dateFormat
  );
  constructor() {}

  ngOnInit() {}
}
