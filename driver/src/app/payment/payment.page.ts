import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { AddCardModalComponent } from './components/add-card-modal/add-card-modal.component';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  public async openAddCardModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddCardModalComponent,
    });
    await modal.present();
  }
}
