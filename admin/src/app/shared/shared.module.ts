import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CarManagementPage } from '@app/car-management/car-management.page';
import { CurrentDriversComponent } from '@app/drivers-management/components/current-drivers/current-drivers.component';
import { FilterDriverPage } from '@app/drivers-management/pages/filter-driver/filter-driver.page';
import { GaragesManagementPage } from '@app/garages-management/garages-management.page';
import { GarageDetailsPage } from '@app/garages-management/pages/garage-details/garage-details.page';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { CarCardComponent } from './components/car-card/car-card.component';
import { CurrentClientsComponent } from './components/current-clients/current-clients.component';
import { DriverCardComponent } from './components/driver-card/driver-card.component';
import { FilterTripComponent } from './components/filter-trip/filter-trip.component';
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { LocalizationService } from './services/localization/localization.service';
import { UtilService } from './services/util/util.service';

@NgModule({
  declarations: [
    StarRatingComponent,
    ProfileHeaderComponent,
    CurrentDriversComponent,
    CurrentClientsComponent,
    FilterTripComponent,
    FilterDriverPage,
    TransactionComponent,
    CarCardComponent,
    CarManagementPage,
    DriverCardComponent,
    GaragesManagementPage,
    GarageDetailsPage,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule,
    NgxContentLoadingModule,
    ColorPickerModule,
  ],
  exports: [
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ProfileHeaderComponent,
    CurrentDriversComponent,
    TransactionComponent,
    CarCardComponent,
    CarManagementPage,
    DriverCardComponent,
    GaragesManagementPage,
    GarageDetailsPage,
    NgxContentLoadingModule,
    ColorPickerModule,
  ],
  entryComponents: [
    CurrentDriversComponent,
    CurrentClientsComponent,
    FilterTripComponent,
    FilterDriverPage,
    CarCardComponent,
    CarManagementPage,
    DriverCardComponent,
    GaragesManagementPage,
    GarageDetailsPage,
  ],
  providers: [LocalizationService, UtilService],
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
    };
  }
}
