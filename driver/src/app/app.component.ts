import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language, WritingDir } from '@common/enums';
import { Localization } from '@common/models';
import { Globalization } from '@ionic-native/globalization/ngx';
import { Push, PushObject } from '@ionic-native/push/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  private pushObject: PushObject;
  public documentDir = 'rtl';

  public appPages = [
    {
      title: 'الصفحة الشخصية',
      url: '/profile',
      icon: 'person',
      canActivate: false,
    },
    {
      title: 'تاريخ الرحلات',
      url: '/trips-history',
      icon: 'calendar',
      canActivate: false,
    },
    {
      title: 'الإعدادات',
      url: '/settings',
      icon: 'settings',
    },
  ];

  public user: any;
  public isAuthorized: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translateService: TranslateService,
    private globalization: Globalization,
    private localizationService: LocalizationService,
    private authService: AuthService,
    private push: Push,
    private navController: NavController
  ) {
    this.initializeApp();
    LocalizationService.localizationChange.subscribe(
      (newLocalization: Localization) => {
        this.documentDir = newLocalization.dir;
      }
    );
    this.authService.change.subscribe(user => {
      if (!this.isAuthorized && !!user) {
        this.user = user;

        this.platform.ready().then(() => {
          this.pushObject = this.push.init(environment.configs.pushOptions);
          this.pushObject.subscribe(this.user.uid);
          this.pushObject.on('notification').subscribe(notificationData => {
            if (!!notificationData.additionalData.clickAction) {
              this.navController.navigateForward(
                notificationData.additionalData.clickAction
              );
            }
          });
        });
      } else if (this.isAuthorized && !!!this.user) {
        this.pushObject.unsubscribe(this.user.uid);
      }
      this.isAuthorized = !!user;
    });
  }

  initializeApp() {
    this.translateService.setDefaultLang('ar');
    this.platform.ready().then(() => {
      this.setInitialLanguage();
      this.statusBar.backgroundColorByHexString('#780002'); // --ion-color-secondary
      this.statusBar.styleLightContent();
    });
  }

  public onVideoReady() {
    // might should replace with thumbnail
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
  }

  public async setInitialLanguage(): Promise<void> {
    let localization: Localization = this.localizationService.getLocalization();
    document.dir = 'rtl';
    try {
      if (!!!localization) {
        localization = {
          lang: (await this.globalization.getPreferredLanguage())
            .value as Language,
        };
        // localization.lang =
        //   localization.lang.split('-')[0].toLowerCase() === Language.ar
        //     ? Language.ar
        //     : Language.en;
        localization.lang = Language.ar;
        this.localizationService.setLocalization(localization);
      }
    } catch (error) {
      console.error(error);
      localization = { lang: Language.ar, dir: WritingDir[Language.ar] };
      this.localizationService.setLocalization(localization);
    }
  }
}
