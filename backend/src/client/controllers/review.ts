import * as mongoose from 'mongoose';
import { UserReviewSchema } from '../../common/models/user-review';
import { Request, Response } from 'express';

const UserReview = mongoose.model('UserReview', UserReviewSchema);

export class UserReviewController {
  // Get My USer Reviews
  public async GetMyUserReviews(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    UserReview.find({ User: userid }, function(err, UserReview) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(UserReview);
    });
  }
  // Get User Reviews
  public async GetUserReviews(req: Request, res: Response): Promise<void> {
    let userid = req.body.userid;
    UserReview.find({ User: userid }, function(err, UserReview) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(UserReview);
    });
  }
  //
  // add User Review
  public addUserReview(req: Request, res: Response) {
    //set values
    let newUserReview = new UserReview({
      scoreOfFive: req.body.scoreOfFive,
      comment: req.body.comment,
      dateTime: req.body.dateTime,
      User: req.body.userid,
    });
    //save UserReview
    newUserReview.save((err, UserReview) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: 'UserReview Added!',
        UserReview: UserReview,
      });
    }); //end save UserReview
  }

  ///

  // update User Review

  public UpdateUserReview(req: Request, res: Response) {
    //set values
    let scoreOfFive = req.body.scoreOfFive;
    let comment = req.body.comment;
    let dateTime = req.body.dateTime;
    let User = req.body.userid;
    //upadte UserReview
    UserReview.findOneAndUpdate(
      { User: User },
      {
        $set: {
          scoreOfFive: scoreOfFive,
          comment: comment,
          dateTime: dateTime,
        },
      },
      function(err, UserReview) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        }else{
        res.json({
          message: 'UserReview Updated!',
          UserReview: UserReview,
        });
      }
      }
    );
  } // end update user review
  //
  // delete User Review
  public async DeleteUserReview(req: Request, res: Response): Promise<void> {
    let reviewid = req.body.reviewid;
    UserReview.findByIdAndRemove({ _id: reviewid }, function(err, UserReview) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: 'UserReview Deleted!',
        UserReview: UserReview,
      });    });
  }
  //
}
