import { Component, OnInit } from '@angular/core';
import { TripsService } from '@app/home/services/trips.service';
import { TripStatus } from '@common/enums';
import { Trip } from '@common/models/trip.model';

@Component({
  selector: 'app-trips-history',
  templateUrl: './trips-history.page.html',
  styleUrls: ['./trips-history.page.scss'],
})
export class TripsHistoryPage implements OnInit {
  public trips: Partial<Trip>[] = [];
  public pendingPastTrips: Partial<Trip>[] = [];
  public pastTrips: Partial<Trip>[] = [];
  isPastLoading: boolean;

  constructor(private tripsService: TripsService) {
    this.loadPastTrips();
  }

  ngOnInit() {}

  public async loadPastTrips(): Promise<void> {
    this.isPastLoading = true;
    this.tripsService.getPastTrips().subscribe(
      data => {
        this.pastTrips = data.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          return trip;
        });
        this.isPastLoading = false;
      },
      error => {
        console.error(error);
        this.isPastLoading = false;
      }
    );

    this.tripsService.getPastTrips(TripStatus.PENDING_PAST).subscribe(
      data => {
        this.pendingPastTrips = data.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          return trip;
        });
        this.isPastLoading = false;
      },
      error => {
        console.error(error);
        this.isPastLoading = false;
      }
    );
  }
}
