import { User } from '@common/models';

export interface Complain {
  subject: string;
  note: string;
  sender: Partial<User>;
  joiningDate: number;
}
