import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceSearchSelectComponent } from './place-search-select.component';

describe('PlaceSearchSelectComponent', () => {
  let component: PlaceSearchSelectComponent;
  let fixture: ComponentFixture<PlaceSearchSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceSearchSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceSearchSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
