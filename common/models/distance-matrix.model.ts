export interface DistanceMatrix {
  distance: {
    text: string;
    value: number;
  };
  duration: {
    text: string;
    value: number;
  };
  duration_in_traffic: {
    text: string;
    value: number;
  };
  status: string;
  price: number;
}
