import { EventEmitter, Injectable } from '@angular/core';
import { WritingDir } from '@common/enums';
import { Localization } from '@common/models';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class LocalizationService {
  public static localizationChange: EventEmitter<
    Localization
  > = new EventEmitter();

  constructor(private translateService: TranslateService) {}

  public getLocalization(): Localization | undefined {
    return JSON.parse(localStorage.getItem('localization'));
  }

  public setLocalization(newLocalization: Localization): void {
    newLocalization.dir = WritingDir[newLocalization.lang];
    document.dir = WritingDir[newLocalization.lang];
    this.translateService.use(newLocalization.lang);
    LocalizationService.localizationChange.emit(newLocalization);
    localStorage.setItem('localization', JSON.stringify(newLocalization));
  }
}
