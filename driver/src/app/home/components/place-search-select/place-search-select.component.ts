import {
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { AutocompletePrediction } from '@app/home/services/google-maps/auto-complete-prediction.model';
import { GoogleMapsService } from '@app/home/services/google-maps/google-maps.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ModalController, Platform } from '@ionic/angular';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

import { PickOnMapModalComponent } from '../pick-on-map-modal/pick-on-map-modal.component';

@Component({
  selector: 'app-place-search-select',
  templateUrl: './place-search-select.component.html',
  styleUrls: ['./place-search-select.component.scss'],
})
export class PlaceSearchSelectComponent implements OnDestroy {
  private _value: AutocompletePrediction;
  private backButtonSubscription: Subscription;

  @Input()
  set value(newValue: AutocompletePrediction) {
    if (!!!newValue) {
      this._value = {
        description: '',
      };
    } else {
      this._value = newValue;
    }
  }
  get value() {
    return this._value;
  }
  @Output() public valueChange: EventEmitter<
    AutocompletePrediction
  > = new EventEmitter<AutocompletePrediction>();

  public isLoading = false;
  public isTypingAhead = false;
  @HostBinding('class') get hostClasses(): string {
    return [
      'place-search-select',
      this.isTypingAhead ? 'place-search-select--active' : '',
    ].join(' ');
  }

  public searchResults: AutocompletePrediction[] = [];

  public timeoutHandler;

  constructor(
    private elementRef: ElementRef,
    private googleMapsService: GoogleMapsService,
    private platform: Platform,
    private modalController: ModalController,
    private keyboard: Keyboard
  ) {
    this.keyboard.onKeyboardWillHide().subscribe(() => {
      if (this.isTypingAhead) {
        const el = document.querySelector(':focus');
        if (el) {
          el['blur']();
        }
      }
      this.onSearchBlur();
    });
  }

  ngOnDestroy() {
    if (this.backButtonSubscription) {
      this.backButtonSubscription.unsubscribe();
    }
  }

  public onSearchFocus(): void {
    this.isTypingAhead = true;

    this.keyboard.onKeyboardShow().subscribe(() => {
      this.elementRef.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    });
  }

  public onSearchBlur(): void {
    this.isTypingAhead = false;
    this.searchResults = [];
  }

  public searchPlaces(searchValue: string): void {
    if (searchValue.length >= 3) {
      this.isLoading = true;
      this.googleMapsService.searchPlaces(searchValue).subscribe(
        places => {
          this.searchResults = places.filter(place => {
            return !!place.place_id;
          });
        },
        () => {},
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.selectPlace(undefined);
    }
  }

  public selectPlace(newPlace: AutocompletePrediction): void {
    this.value = newPlace;
    this.valueChange.emit(newPlace);
  }

  public async openPickOnMapModal(): Promise<void> {
    const pickOnMapModal = await this.modalController.create({
      component: PickOnMapModalComponent,
      cssClass: 'modal--no-backdrop',
      componentProps: {
        searchPlace: this.value,
      },
    });
    pickOnMapModal.onWillDismiss().then(modalDismissEvent => {
      this.selectPlace(_.get(modalDismissEvent, 'data', this.value));
    });
    await pickOnMapModal.present();
  }
}
