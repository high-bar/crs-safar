export enum CarStatus {
  GARAGE = 'Garage',
  MAINTENANCE = 'Maintenance',
  TRIP = 'Trip',
}
