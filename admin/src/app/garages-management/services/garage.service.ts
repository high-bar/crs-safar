import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Garage } from '@common/models';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GarageService {
  constructor(private http: HttpClient) {}

  public addNewGarage(
    garage: Partial<Garage>
  ): Promise<firestore.DocumentData> {
    garage.joiningDate = new Date().getTime();

    return firestore()
      .collection('garages')
      .add(garage);
  }

  public getGarages(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('garages')
        .orderBy('joiningDate', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getGaragesOnce(): Promise<firestore.QuerySnapshot> {
    return firestore()
      .collection('garages')
      .orderBy('joiningDate', 'desc')
      .get();
  }

  public deleteGarage(garageId: string): Promise<void> {
    return firestore()
      .collection('garages')
      .doc(garageId)
      .delete();
  }

  public editGarage(garage?: Partial<Garage>): Promise<void> {
    return firestore()
      .collection('cars')
      .doc(garage.id)
      .update(garage);
  }
}
