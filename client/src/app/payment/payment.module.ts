import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { AddCardModalComponent } from './components/add-card-modal/add-card-modal.component';
import { PaymentPage } from './payment.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [PaymentPage, AddCardModalComponent],
  entryComponents: [AddCardModalComponent],
})
export class PaymentPageModule {}
