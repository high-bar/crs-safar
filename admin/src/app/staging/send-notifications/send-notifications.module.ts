import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CurrentClientsComponent } from '@app/shared/components/current-clients/current-clients.component';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { SendNotificationsPage } from './send-notifications.page';

const routes: Routes = [
  {
    path: '',
    component: SendNotificationsPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SendNotificationsPage],
  entryComponents: [SendNotificationsPage, CurrentClientsComponent],
})
export class SendNotificationsPageModule {}
