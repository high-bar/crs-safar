export interface TripHistory {
  past: string[];
  future: string[];
  active: string[];
}
