import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { PickOnMapModalComponent } from './components/pick-on-map-modal/pick-on-map-modal.component';
import { PlaceSearchSelectComponent } from './components/place-search-select/place-search-select.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';
import { BookingPage } from './pages/booking/booking.page';
import { LoginPage } from './pages/login/login.page';
import { OverviewPage } from './pages/overview/overview.page';
import { SignUpPage } from './pages/sign-up/sign-up.page';
import { GoogleMapsService } from './services/google-maps/google-maps.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HomeRoutingModule,
    MatButtonModule,
  ],
  declarations: [
    HomePage,
    LoginPage,
    SignUpPage,
    BookingPage,
    PlaceSearchSelectComponent,
    OverviewPage,
    PickOnMapModalComponent,
    ForgotPasswordComponent,
  ],
  entryComponents: [PickOnMapModalComponent, ForgotPasswordComponent],
  providers: [GoogleMapsService],
})
export class HomePageModule {}
