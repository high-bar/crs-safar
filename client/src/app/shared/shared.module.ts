import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {
  NgbDateAdapter,
  NgbDateNativeAdapter,
  NgbDatepickerModule,
  NgbTimepickerModule,
} from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { RatingComponent } from './components/rating/rating.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { TripComponent } from './components/trip/trip.component';
import { TimePipe } from './pipes/time/time.pipe';
import { LocalizationService } from './services/localization/localization.service';

@NgModule({
  declarations: [
    TripComponent,
    StarRatingComponent,
    RatingComponent,
    ProfileHeaderComponent,
    TimePipe,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgxContentLoadingModule,
  ],
  exports: [
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgxContentLoadingModule,
    TripComponent,
    ProfileHeaderComponent,
    StarRatingComponent,
    RatingComponent,
    TimePipe,
  ],
  entryComponents: [StarRatingComponent, RatingComponent],
  providers: [LocalizationService],
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
    };
  }
}
