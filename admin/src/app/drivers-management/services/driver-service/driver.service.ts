import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DriverStatus, UserRole } from '@common/enums';
import { Driver, DriverFilter, Garage } from '@common/models';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DriverService {
  constructor(private http: HttpClient) {}

  public getNewDrivers(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('users')
        .where('role', '==', UserRole.DRIVER)
        .where('isApproved', '==', false)
        .orderBy('joiningDate', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getCurrentDrivers(
    driverFilter?: DriverFilter
  ): Observable<firestore.QuerySnapshot> {
    let query: firestore.Query = firestore()
      .collection('users')
      .where('role', '==', UserRole.DRIVER)
      .where('isApproved', '==', true)
      .where('isBlocked', '==', false);
    if (!!driverFilter) {
      if (!!driverFilter.fullName) {
        query = query.where('fullName', '==', driverFilter.fullName);
      }
      if (!!driverFilter.email) {
        query = query.where('email', '==', driverFilter.email);
      }
      if (!!driverFilter.phone) {
        query = query.where('phone', '==', driverFilter.phone);
      }
      if (!!driverFilter.rate) {
        query = query.where('rate', '==', driverFilter.rate);
      }
      if (!!driverFilter.status) {
        query = query.where('status', '==', driverFilter.status);
      }
    }
    return Observable.create(
      (observer: Observer<Partial<firestore.QuerySnapshot>>) => {
        query.orderBy('joiningDate', 'desc').onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      }
    );
  }

  public approveDriver(driver: Driver): Promise<void> {
    return firestore()
      .collection('users')
      .doc(driver.uid)
      .update({
        isApproved: true,
        garage: driver.garage,
        joiningDate: new Date().getTime(),
      });
  }

  public changeDriverGarage(driver: Driver): Promise<void> {
    return firestore()
      .collection('users')
      .doc(driver.uid)
      .update({
        garage: {
          name: driver.garage.name,
          id: driver.garage.id,
        },
      });
  }

  public blockDriver(driver: Driver, isBlocked: boolean): Promise<void> {
    return firestore()
      .collection('users')
      .doc(driver.uid)
      .update({
        isBlocked: isBlocked,
        status: isBlocked ? DriverStatus.BLOCKED : DriverStatus.IDLE,
        blockDate: new Date().getTime(),
      });
  }

  public getDriverByID(uid): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('users')
          .doc(uid)
          .onSnapshot(
            documentSnapshot => {
              observer.next(documentSnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }
}
