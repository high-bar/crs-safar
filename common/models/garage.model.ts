export interface Garage {
  id: string;
  name: string;
  address: string;
  cars: string[];
  drivers: string[];
  joiningDate: number;
  phone: string;
  notes: string;
}
