import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserRole } from '@common/enums';
import { Client, ClientFilter } from '@common/models';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(private http: HttpClient) {}

  public getCurrentClient(
    clientFilter?: ClientFilter
  ): Observable<firestore.QuerySnapshot> {
    let query: firestore.Query = firestore()
      .collection('users')
      .where('role', '==', UserRole.CLIENT);
    if (!!clientFilter) {
      if (!!clientFilter.fullName) {
        query = query.where('fullName', '==', clientFilter.fullName);
      }
      if (!!clientFilter.email) {
        query = query.where('email', '==', clientFilter.email);
      }
      if (!!clientFilter.phone) {
        query = query.where('phone', '==', clientFilter.phone);
      }
      if (!!clientFilter.rate) {
        query = query.where('rate', '==', clientFilter.rate);
      }
    }
    return Observable.create(
      (observer: Observer<Partial<firestore.QuerySnapshot>>) => {
        query.orderBy('joiningDate', 'desc').onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      }
    );
  }

  public getClientByID(uid): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('users')
          .doc(uid)
          .onSnapshot(
            documentSnapshot => {
              observer.next(documentSnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }

  public blockDriver(
    client: Partial<Client>,
    isBlocked: boolean
  ): Promise<void> {
    return firestore()
      .collection('users')
      .doc(client.uid)
      .update({
        isBlocked: isBlocked,
        blockDate: new Date().getTime(),
      });
  }
}
