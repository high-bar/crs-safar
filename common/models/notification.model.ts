export interface Notification {
  title: string;
  body: string;
  clients?: string[];
  drivers?: string[];
  message: string;
}
