import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserReviewSchema = new Schema( {
  scoreOfFive: {
    type:Number
  },
  comment: {
    type:String
  },
  dateTime: {
    type:Date
  },
  User: {
    type:Schema.Types.ObjectId, ref:'User'
  },
});
