import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@app/settings/services/settings-service';
import { PriceList } from '@common/models';

@Component({
  selector: 'app-trips-salary',
  templateUrl: './trips-salary.page.html',
  styleUrls: ['./trips-salary.page.scss'],
})
export class TripsSalaryPage implements OnInit {
  public readonly ObjectKeys = Object.keys;
  public originalPriceList: PriceList = {};
  public priceList: PriceList = {};
  public isLoading: boolean;
  public isUpdatePricesLoading: boolean;

  constructor(private settingsService: SettingsService) {
    this.loadPriceList();
  }
  ngOnInit() {}

  public async loadPriceList(): Promise<void> {
    this.isLoading = true;
    try {
      this.priceList = (await this.settingsService.getPriceList()).data().states;
      this.originalPriceList = JSON.parse(JSON.stringify(this.priceList));
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async updatePrices(): Promise<void> {
    this.isUpdatePricesLoading = true;
    try {
      await this.settingsService.updatePrices(this.priceList);
      this.originalPriceList = JSON.parse(JSON.stringify(this.priceList));
    } catch (error) {
      console.error(error);
    } finally {
      this.isUpdatePricesLoading = false;
    }
  }

  public hasPricesChanged(): boolean {
    return (
      JSON.stringify(this.originalPriceList) !== JSON.stringify(this.priceList)
    );
  }
}
