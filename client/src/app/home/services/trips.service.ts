import { Injectable } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { TripStatus } from '@common/enums';
import { Client } from '@common/models';
import { firestore, Observer } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TripsService {
  constructor(private authService: AuthService) {}

  public getNewTrips(): Observable<firestore.QuerySnapshot> {
    const user: Client = JSON.parse(this.authService.getSavedUser());
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('client.uid', '==', user.uid)
        .where('status', '==', TripStatus.NEW)
        .orderBy('requestDateTime', 'desc')
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getFutureTrips(): Observable<firestore.QuerySnapshot> {
    const user: Client = JSON.parse(this.authService.getSavedUser());
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('client.uid', '==', user.uid)
        .where('status', '==', TripStatus.FUTURE)
        .orderBy('requestDateTime', 'desc')
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getActiveTrip(): Observable<firestore.QuerySnapshot> {
    const user: Client = JSON.parse(this.authService.getSavedUser());
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('client.uid', '==', user.uid)
        .where('status', '==', TripStatus.CURRENT)
        .orderBy('requestDateTime', 'desc')
        .limit(1)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getActiveTripOnce(): Promise<firestore.QuerySnapshot> {
    const user: Client = JSON.parse(
      this.authService.getSavedUser() || '{"uid": "0"}'
    );
    return firestore()
      .collection('trips')
      .where('client.uid', '==', user.uid)
      .where('status', '==', TripStatus.CURRENT)
      .orderBy('requestDateTime', 'desc')
      .limit(1)
      .get();
  }

  public getPastTrips(
    pastType: TripStatus = TripStatus.PAST
  ): Observable<firestore.QuerySnapshot> {
    const user: Client = JSON.parse(this.authService.getSavedUser());

    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('status', '==', pastType)
        .where('client.uid', '==', user.uid)
        .orderBy('conclusionDateTime', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getTripByID(tripId): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('trips')
      .doc(tripId)
      .get();
  }
}
