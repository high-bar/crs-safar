import { CarStatus, CarType } from '@common/enums';
import { Garage, TripHistory } from '@common/models';

export interface Car {
  id: string;
  plateNumber: string;
  type: CarType;
  model: string;
  brand: string;
  year: number;
  status: CarStatus;
  notes: string[];
  trips: TripHistory;
  joiningDate: number;
  color: string;
  colorArabic: string;
  colorHex: string;
  currentTripId?: string;
  garage?: Partial<Garage>;
}
