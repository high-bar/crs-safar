export enum CarType {
  SUV = 'SUV',
  SEDAN = 'Sedan',
  LUXURY = 'Luxury',
}
