import { Component, OnInit } from '@angular/core';
import { SystemFlags } from '@common/models';

import { RentalService } from './services/rental.service';

@Component({
  selector: 'app-rental',
  templateUrl: './rental.page.html',
  styleUrls: ['./rental.page.scss'],
})
export class RentalPage implements OnInit {
  public rentalPrice: number;
  public fastOrderNumber: number;
  public systemFlags: SystemFlags;
  public isLoading: boolean;

  constructor(private rentalService: RentalService) {
    this.loadSystemFlags();
  }

  ngOnInit() {}

  public loadSystemFlags(): void {
    this.isLoading = true;
    this.rentalService.getSystemFlags().subscribe(data => {
      this.systemFlags = data.data() as SystemFlags;
      this.isLoading = false;
    });
  }

  // public async loadPastTrips(): Promise<void> {
  //   this.rentalService.getPastTrips().subscribe(
  //     data => {
  //       this.systemFlags = data.docs.map(doc => {
  //         const trip = doc.data() as SystemFlags;
  //         return trip;
  //       });
  //     },
  //     error => {
  //       console.error(error);
  //     }
  //   );
  // }
}
