import { UserReviewController } from '../controllers/review';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class UserReviewRoutes {
  public UserReviewController: UserReviewController = new UserReviewController();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      var token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //

    ///////////////
    // start api
    app
      .route('/client/adduserreview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.UserReviewController.addUserReview.bind(
        this.UserReviewController
      ));
    //// end api

    ///////////////
    // start api get my reviews
    app
      .route('/client/getmyuserreview')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.UserReviewController.GetMyUserReviews.bind(
        this.UserReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/client/getuserreviews')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.UserReviewController.GetUserReviews.bind(
        this.UserReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/client/updateuserreview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.UserReviewController.UpdateUserReview.bind(
        this.UserReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/client/deleteuserreview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.UserReviewController.DeleteUserReview.bind(
        this.UserReviewController
      ));
    //// end api

    //end app route
  }

  //end class
}
