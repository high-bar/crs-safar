import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  Output,
  ViewChild,
} from '@angular/core';
import { AutocompletePrediction } from '@app/home/services/google-maps/auto-complete-prediction.model';
import { GoogleMapsService } from '@app/home/services/google-maps/google-maps.service';
import {
  CameraPosition,
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  ILatLng,
  LatLng,
  LocationService,
  MyLocation,
} from '@ionic-native/google-maps/ngx';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pick-on-map-modal',
  templateUrl: './pick-on-map-modal.component.html',
  styleUrls: ['./pick-on-map-modal.component.scss'],
})
export class PickOnMapModalComponent {
  private map: GoogleMap;
  private mapDragDebounce: number;
  private _searchPlace: AutocompletePrediction;
  private backButtonSubscription: Subscription;

  @Input()
  set searchPlace(newValue: AutocompletePrediction) {
    if (!!!newValue) {
      this._searchPlace = {
        description: '',
      };
    } else {
      this._searchPlace = newValue;
    }
  }
  get searchPlace() {
    return this._searchPlace;
  }
  @Output() public valueChange: EventEmitter<
    AutocompletePrediction
  > = new EventEmitter<AutocompletePrediction>();

  public isSearchFocused = false;
  public isLoading = false;
  public isCameraMoving = false;
  public isPlacePicked = false;

  @ViewChild('mapCanvas', { static: true }) public mapCanvas: ElementRef;

  constructor(
    private modalCtrl: ModalController,
    private platform: Platform,
    private alertController: AlertController,
    private googleMapsService: GoogleMapsService,
    private ngZone: NgZone
  ) {}

  async ionViewDidEnter() {
    await this.platform.ready();
    await this.initMap();
    if (!!this.searchPlace.place_id) {
      this.moveCameraToPlace(this.searchPlace.place_id);
    } else {
      this.handleMyLocationBtn();
    }

    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        this.close();
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
    // To fix map being in a modal
    const nodeList = document.querySelectorAll('._gmaps_cdv_');
    for (let k = 0; k < nodeList.length; ++k) {
      nodeList.item(k).classList.remove('_gmaps_cdv_');
    }
  }

  private async initMap(): Promise<void> {
    let currentLocation: MyLocation;
    try {
      currentLocation = await LocationService.getMyLocation();
    } catch (error) {}

    this.map = GoogleMaps.create(this.mapCanvas.nativeElement, {
      controls: {
        compass: true,
        myLocationButton: true,
        myLocation: true,
      },
      camera: {
        zoom: 15,
        target: _.get(currentLocation, 'latLng'),
      },
    });

    this.map
      .on(GoogleMapsEvent.MAP_DRAG_START)
      .subscribe(this.onMapDragStart.bind(this));

    this.map
      .on(GoogleMapsEvent.MAP_DRAG_END)
      .subscribe(this.onMapDragEnd.bind(this));

    this.map
      .on(GoogleMapsEvent.MY_LOCATION_BUTTON_CLICK)
      .subscribe(this.handleMyLocationBtn.bind(this));
  }

  private onMapDragStart(): void {
    this.ngZone.run(() => {
      this.isCameraMoving = true;
      this.isPlacePicked = false;
      this.isSearchFocused = false;
    });
  }

  private onMapDragEnd(): void {
    this.ngZone.run(() => {
      const cameraPosition: CameraPosition<
        ILatLng
      > = this.map.getCameraPosition();
      this.isCameraMoving = false;
      this.isLoading = true;

      clearTimeout(this.mapDragDebounce);
      this.mapDragDebounce = window.setTimeout(() => {
        this.googleMapsService
          .getPlaceByLocation(cameraPosition.target)
          .subscribe(
            place => {
              this.searchPlace = {
                description: place.formatted_address,
                place_id: place.place_id,
              };
              this.isPlacePicked = true;
            },
            () => {},
            () => {
              this.isLoading = false;
            }
          );
      }, 1500);
    });
  }

  private async handleMyLocationBtn(): Promise<void> {
    this.ngZone.run(async () => {
      try {
        await LocationService.getMyLocation();
        setTimeout(() => {
          this.onMapDragEnd();
        }, 1500);
      } catch (error) {
        (await this.alertController.create({
          message: 'Please turn on your GPS',
          buttons: ['OK'],
        })).present();
      }
    });
  }

  private async moveCamera(targetLocation: LatLng): Promise<void> {
    await this.map.moveCamera({ target: targetLocation });
  }

  private moveCameraToPlace(placeId): void {
    this.isLoading = true;
    this.googleMapsService.getPlace(placeId).subscribe(
      async (place: google.maps.places.PlaceResult) => {
        await this.moveCamera((place.geometry.location as unknown) as LatLng);
      },
      () => {},
      () => {
        this.isLoading = false;
      }
    );
  }

  public searchPlaces(searchValue: string): void {
    if (searchValue.length >= 3 && this.isSearchFocused) {
      this.isLoading = true;
      this.googleMapsService.searchPlaces(searchValue).subscribe(places => {
        this.searchPlace = _.get(
          places.filter(place => {
            return !!place.place_id;
          }),
          '0'
        );
        this.moveCameraToPlace(this.searchPlace.place_id);
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalCtrl.dismiss();
  }

  public async done(): Promise<void> {
    await this.modalCtrl.dismiss(this.searchPlace);
  }
}
