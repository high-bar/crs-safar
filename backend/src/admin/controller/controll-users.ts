import * as mongoose from 'mongoose';
import { AdminSchema } from '../../common/models/admin';
import { DriverSchema } from '../../common/models/driver';
import { UserSchema } from '../../common/models/user';
import { Request, Response } from 'express';
const https = require('https');

const Admin = mongoose.model('Admin', AdminSchema);
const Driver = mongoose.model('driver', DriverSchema);
const User = mongoose.model('User', UserSchema);

export class ControllUsers {
  constructor() {}
  //
  public async Admins(req: Request, res: Response): Promise<void> {
    Admin.find({}, function(err, Admins){
      if(err){
        res.status(401).json({
          error: err,
        });
      }
      res.json(Admins);
    });
  } //


  /////////////////
//
public async Drivers(req: Request, res: Response): Promise<void> {
  Driver.find({}, function(err, Drivers){
    if(err){
      res.status(401).json({
        error: err,
      });
    }
    res.json(Drivers);
  });
} //
///////
// get Drivers index first //
public Drivers1(req: Request, res: Response) {
  let isApproved = req.body.isApproved;
  Driver.find({isApproved : isApproved}, function(err: any, Driver: any) {
    if (err) {
      res.status(401).json({
        error: err,
      });
    } else {
      res.json(Driver);
    }
  })
    .limit(10)
    .sort({ _id: -1 }); // end find all
}
/////////////////////////////
public Drivers2(req: Request, res: Response) {
  let limit = parseInt(req.body.limit) || 10;
  let skip = parseInt(req.body.index) || 10;
  // find
  let isApproved = req.body.isApproved;
  Driver.find({isApproved : isApproved})
    .skip(skip)
    .limit(limit)
    .sort({ _id: -1 })
    .exec(function(err: any, Driver: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Driver);
      }
    }); // end find all
}
/////////////////////////////
//delete Driver
public DeleteDriver(req: Request, res: Response) {
  let driverid = req.body.driverid;
  //delete
  Driver.findByIdAndRemove({ _id: driverid }, function(err, Driver) {
    if (err) {
      res.status(401).json({
        error: err,
      });
    } else {
      res.json({
        message: 'Driver Deleted!',
        Driver: Driver,
      });
    }
  });
}

/////////////////
//
public async Users(req: Request, res: Response): Promise<void> {
  User.find({}, function(err, Users){
    if(err){
      res.status(401).json({
        error: err,
      });
    }
    res.json(Users);
  });
} //
///////
// get Users index first //
public Users1(req: Request, res: Response) {
  User.find({}, function(err: any, User: any) {
    if (err) {
      res.status(401).json({
        error: err,
      });
    } else {
      res.json(User);
    }
  })
    .limit(10)
    .sort({ _id: -1 }); // end find all
}
/////////////////////////////
public User2(req: Request, res: Response) {
  let limit = parseInt(req.body.limit) || 10;
  let skip = parseInt(req.body.index) || 10;
  // find
  console.log(limit);
  User.find({})
    .skip(skip)
    .limit(limit)
    .sort({ _id: -1 })
    .exec(function(err: any, User: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(User);
      }
    }); // end find all
}
/////////////////////////////
//delete User
public DeleteUser(req: Request, res: Response) {
  let userid = req.body.userid;
  //delete
  User.findByIdAndRemove({ _id: userid }, function(err, User) {
    if (err) {
      res.status(401).json({
        error: err,
      });
    } else {
      res.json({
        message: 'User Deleted!',
        User: User,
      });
    }
  });
}

/////////////////
//Aprove Driver//
public ApproveDriver(req: Request, res: Response) {
  let driverid = req.body.driverid;
  //Update
  Driver.findOneAndUpdate(
    { _id: driverid },
    {
      $set: {
        isApproved: "true",
      },
    },
    function(err, Driver) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json({
          message: 'Driver Approved!',
          Driver: Driver,
        });
      }
    }
  );
} //end
/////////////////

} //end booking trip class
