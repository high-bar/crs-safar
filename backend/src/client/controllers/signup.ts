import * as mongoose from 'mongoose';
import {UserSchema }from '../../common/models/user';
import {Request, Response }from 'express';
import {config }from '../../const/config';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

const User = mongoose.model('User', UserSchema);

export class ClientSignUpController {
  public addClient(req:Request, res:Response) {
    // let newUser = new User(req.body);
    //
    function createToken(User:any) {
      var token = jwt.sign( {
        id:User._id,
        fullName:User.fullName,
        email:User.email,
        phone:User.phone,
        gender:User.gender,
        country:User.country,
        state:User.state,
        rate:User.rate
      }, secretKey);
      return token;
    }
    //
    //check if user already registerd
    User.findOne({email : req.body.email}, function (err, checkexists) {
       if (checkexists) {
        res.status(401).json({
          error: 'email already registered',
        });
       }else {
    // get password and hashing it
    let password = req.body.password;
    let NewPassword = bcrypt.hashSync(password, 10);
    //setting up user data to save it
    let newUser = new User({
      fullName:req.body.fullName,
      email:req.body.email,
      phone:req.body.phone,
      gender:req.body.gender,
      country:req.body.country,
      state:req.body.state,
      rate:"5",
      password:NewPassword,
      joiningDate:req.body.joiningDate
    });

    // save user data
    newUser.save((err, User) =>  {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      let token = createToken(User);
      res.json( {
        User,
      token:token
    });

    });   //end save user

  }// end else

}) //end find one function

    //
  }
}