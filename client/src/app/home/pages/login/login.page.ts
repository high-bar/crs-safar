import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ForgotPasswordComponent } from '@app/home/components/forgot-password/forgot-password.component';
import { UtilService } from '@app/home/services/util/util.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ModalController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup = this.formBuilder.group({
    email: [undefined, [Validators.required, Validators.email]],
    password: [undefined, [Validators.required]],
  });

  public isLoading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  public async submitLogin() {
    if (this.loginForm.valid) {
      this.isLoading = true;

      try {
        await this.authService.login(this.loginForm.value);
        const returnUrl =
          this.activatedRoute.snapshot.queryParams['returnUrl'] ||
          'home/overview';
        await this.navController.navigateRoot(returnUrl);
      } catch (error) {
        if (
          error.code === 'auth/user-not-found' ||
          error.code === 'auth/wrong-password'
        ) {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.${error.code}`)
          );
        } else {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        }
      } finally {
        this.isLoading = false;
      }
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async openForgotPasswordModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: ForgotPasswordComponent,
    });
    return await modal.present();
  }

  public async submitGoogleLogin(): Promise<void> {
    this.isLoading = true;
    try {
      const didLogin = await this.authService.googleLogin();
      if (!!didLogin) {
        const returnUrl =
          this.activatedRoute.snapshot.queryParams['returnUrl'] ||
          'home/overview';
        await this.navController.navigateRoot(returnUrl);
      }
    } catch (error) {
      let errorMessage = '';
      switch (error) {
        // user rejected
        case 'not-google-email':
          errorMessage = 'errors.not-google-email';
          break;
        case 13:
          errorMessage = 'errors.13';
          break;
        case 12501:
          errorMessage = 'errors.12501';
          break;
        // connection error
        case 7:
          errorMessage = 'errors.7';
          break;
        default:
          errorMessage = 'errors.server';
      }

      await this.utilService.errorAlert(
        this.translateService.instant(errorMessage)
      );
    } finally {
      this.isLoading = false;
    }
  }

  public async submitFacebookLogin(): Promise<void> {
    this.isLoading = true;
    try {
      const didLogin = await this.authService.facebookLogin();
      if (!!didLogin) {
        const returnUrl =
          this.activatedRoute.snapshot.queryParams['returnUrl'] ||
          'home/overview';
        await this.navController.navigateRoot(returnUrl);
      }
    } catch (error) {
    } finally {
      this.isLoading = false;
    }
  }
}
