import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentCollectingComponent } from '@app/shared/components/payment-collecting/payment-collecting.component';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { UtilService } from '@app/shared/services/util/util.service';
import { DriverTripStatus, TripStatus } from '@common/enums';
import { Driver, Trip } from '@common/models';
import { BackgroundGeolocation as IBackgroundGeolocation } from '@common/models';
import {
  BackgroundGeolocation,
  BackgroundGeolocationEvents,
} from '@ionic-native/background-geolocation/ngx';
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from '@ionic-native/launch-navigator/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { ModalController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-trip-details',
  templateUrl: './trip.page.html',
  styleUrls: ['./trip.page.scss'],
})
export class TripPage implements OnInit {
  public readonly driverTripStatusEnum: typeof DriverTripStatus = DriverTripStatus;
  @Input() public trip: Trip;
  public isPage: boolean;
  public trueDriverStatus: DriverTripStatus;
  public isLoading: boolean;

  public isDriverStatusAboutToChange: boolean;
  public driverStatusChangeCountDown: number;
  public driverStatusChangeTimerHandler: number;
  public isChangeLoading: boolean;
  public isTrackingOn = false;
  public currentTripExist = true;
  public trackingStatuses: DriverTripStatus[] = [
    DriverTripStatus.MEETING_CLIENT,
    DriverTripStatus.PICKING_CLIENT,
    DriverTripStatus.STARTED,
    DriverTripStatus.RETURNING,
  ];

  public isTripRequest = false;
  public isTripRequestLoading = false;
  public driverStatusList = [
    DriverTripStatus.MEETING_CLIENT,
    DriverTripStatus.PICKING_CLIENT,
    DriverTripStatus.STARTED,
    DriverTripStatus.RETURNING,
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private tripsService: TripsService,
    private locationAccuracy: LocationAccuracy,
    private backgroundGeolocation: BackgroundGeolocation,
    private launchNavigator: LaunchNavigator,
    private authService: AuthService,
    private utilService: UtilService,
    private translateService: TranslateService,
    private navController: NavController,
    private modalController: ModalController
  ) {
    this.isPage = !!this.activatedRoute.snapshot.params.tripId;
    this.isTripRequest = !!this.activatedRoute.snapshot.queryParams
      .trip_request;

    if (this.isPage) {
      this.checkCurrent();
      this.loadTripDetails(this.activatedRoute.snapshot.params.tripId);
    }
  }

  ngOnInit() {
    if (!this.isPage) {
      this.prepareTracking();
    }
  }

  private getKmDistanceBetweenCoordinates(
    location1: Partial<IBackgroundGeolocation>,
    location2: Partial<IBackgroundGeolocation>
  ): number {
    location1 = JSON.parse(JSON.stringify(location1));
    location2 = JSON.parse(JSON.stringify(location2));
    function degreesToRadians(degrees) {
      return (degrees * Math.PI) / 180;
    }

    const earthRadiusKm = 6371;

    const dLat = degreesToRadians(location2.latitude - location1.latitude);
    const dLon = degreesToRadians(location2.longitude - location1.longitude);

    location1.latitude = degreesToRadians(location1.latitude);
    location2.latitude = degreesToRadians(location2.latitude);

    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) *
        Math.sin(dLon / 2) *
        Math.cos(location1.latitude) *
        Math.cos(location2.latitude);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return earthRadiusKm * c;
  }

  private validateLocationAndUpdateRoute(
    newLocation: IBackgroundGeolocation
  ): void {
    if (!!!this.trip.route) {
      this.trip.route = [];
    }
    const previousLocation: Partial<IBackgroundGeolocation> = this.trip.route[
      this.trip.route.length - 1
    ];
    newLocation.distanceMovedInKm = 0;
    if (!!previousLocation) {
      const distanceMoved = this.getKmDistanceBetweenCoordinates(
        previousLocation,
        newLocation
      );
      const timeUsedToMove =
        (newLocation.time - previousLocation.time) / 3600000;

      const speedUsedToMove = distanceMoved / timeUsedToMove;
      if (speedUsedToMove > 500) {
        newLocation.accuracy = 9999;
      }

      newLocation.distanceMovedInKm =
        distanceMoved + previousLocation.distanceMovedInKm;
    }
    this.trip.route.push(newLocation);
    this.tripsService.updateTripRoute(this.trip);
  }

  public async checkCurrent(): Promise<void> {
    this.currentTripExist = _.get(
      await this.tripsService.getActiveTripOnce(),
      'docs[0].exists',
      false
    );
  }

  private loadTripDetails(tripId): void {
    this.isLoading = true;
    this.tripsService.getTripById(tripId).subscribe(
      doc => {
        this.trip = doc.data() as Trip;
        this.trip.id = doc.id;
        this.isLoading = false;
        this.prepareTracking();
      },
      error => {
        console.error(error);
      }
    );
  }

  public prepareTracking(): void {
    this.trueDriverStatus = this.trip.driverStatus;

    if (this.trackingStatuses.indexOf(this.trip.driverStatus) >= 0) {
      if (!!!this.trip.route) {
        this.trip.route = [];
      }
      if (!this.isTrackingOn) {
        this.checkGPSStatus();
      }
    }
  }

  public async launchGoogleNavigator(location) {
    if (!this.isTripRequest) {
      const options: LaunchNavigatorOptions = {
        app: this.launchNavigator.APP.GOOGLE_MAPS,
      };

      await this.launchNavigator.navigate(location, options);
    }
  }

  public async launchDynamicGoogleNavigator(trip: Trip): Promise<void> {
    const options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    };
    let target: string;
    if (this.trip.driverStatus === DriverTripStatus.MEETING_CLIENT) {
      target = this.trip.pickupPlace.description;
    } else if (this.trip.driverStatus === DriverTripStatus.STARTED) {
      target = this.trip.dropPlace.description;
    }
    if (!!target) {
      await this.launchNavigator.navigate(target, options);
    }
  }

  public driverTripStatusChanged(newValue: DriverTripStatus): void {
    clearTimeout(this.driverStatusChangeTimerHandler);

    // chosen vs true
    if (
      this.driverStatusList.indexOf(newValue) >
      this.driverStatusList.indexOf(this.trueDriverStatus) + 1
    ) {
      this.cancelDriverTripStatusChange();
    } else if (this.trip.driverStatus !== this.trueDriverStatus) {
      this.driverStatusChangeCountDown = 0;
      this.isDriverStatusAboutToChange = true;
      this.driverStatusChangeTimerHandler = window.setInterval(async () => {
        this.driverStatusChangeCountDown += 1 / 20;
        if (this.driverStatusChangeCountDown >= 1) {
          clearTimeout(this.driverStatusChangeTimerHandler);
          if (this.trip.driverStatus === DriverTripStatus.RETURNING) {
            // open payment modal
            const modal = await this.modalController.create({
              component: PaymentCollectingComponent,
              componentProps: {
                trip: this.trip,
              },
            });
            await modal.present();
            const extraPaymentTrip = (await modal.onWillDismiss()).data as any;

            if (!!extraPaymentTrip) {
              this.trip = extraPaymentTrip;
              this.submitTripDriverStatusChange();
            } else {
              this.cancelDriverTripStatusChange();
            }
          } else {
            this.submitTripDriverStatusChange();
          }
        }
      }, 200);
    }
  }

  public cancelDriverTripStatusChange(): void {
    clearTimeout(this.driverStatusChangeTimerHandler);
    this.isDriverStatusAboutToChange = false;
    setTimeout(() => {
      this.trip.driverStatus = this.trueDriverStatus;
    }, 0);
  }

  public async submitTripDriverStatusChange(): Promise<void> {
    this.isChangeLoading = true;
    try {
      if (this.trip.driverStatus === DriverTripStatus.MEETING_CLIENT) {
        this.trip.status = TripStatus.CURRENT;
      } else if (this.trip.driverStatus === DriverTripStatus.RETURNING) {
        this.trip.status = TripStatus.PENDING_PAST;
      }
      await this.tripsService.updateDriverTripStatus(this.trip);
      this.trueDriverStatus = this.trip.driverStatus;
      this.isDriverStatusAboutToChange = false;
      this.prepareTracking();
    } catch (error) {
      console.error(error);
    } finally {
      this.isChangeLoading = false;
    }
  }

  public async checkGPSStatus(): Promise<void> {
    this.backgroundGeolocation.stop();
    try {
      this.isTrackingOn = false;
      const GPSStatus = await this.backgroundGeolocation.checkStatus();
      this.isTrackingOn = GPSStatus.isRunning;
      if (!GPSStatus.isRunning) {
        this.startWatchingGPS();
      }
      if (!GPSStatus.locationServicesEnabled) {
        this.requestGPSAccess();
      }
    } catch (error) {
      console.error(error);
      this.isTrackingOn = false;
      this.requestGPSAccess();
    }
  }

  public async requestGPSAccess(): Promise<void> {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
      .then(
        () => {
          this.startWatchingGPS();
        },
        error => console.log('Error requesting location permissions', error)
      );
  }

  public startWatchingGPS(): void {
    this.backgroundGeolocation
      .configure({
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 100,
        debug: !environment.production,
        stopOnTerminate: true,
        interval: 5000,
        fastestInterval: 5000,
        activitiesInterval: 5000,
        startForeground: true,
        notificationTitle: 'Safar Tracking',
      })
      .then(() => {
        this.backgroundGeolocation
          .on(BackgroundGeolocationEvents.location)
          .subscribe((location: any) => {
            this.isTrackingOn = true;
            this.validateLocationAndUpdateRoute(location);
            // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
            // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
            // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
            this.backgroundGeolocation.finish(); // FOR IOS ONLY
          });
      })
      .catch(error => {
        console.log('tracking', error);
      });

    // start recording location
    this.backgroundGeolocation.start();
  }

  public async acceptTripRequest(): Promise<void> {
    this.isTripRequestLoading = true;
    try {
      const user: Driver = JSON.parse(this.authService.getSavedUser());
      await this.tripsService.updateTripDriverRequest(this.trip.id, {
        email: user.email,
        fullName: user.fullName,
        garage: user.garage,
        phone: user.phone,
        pictureURL: user.pictureURL,
        rate: user.rate,
        uid: user.uid,
      });
      this.navController.pop();
    } catch (error) {
      console.error(error);
      if (error === 'trip/driver-already-assigned') {
        await this.utilService.errorAlert(
          this.translateService.instant(`errors.${error}`),
          this.translateService.instant(`errors.${error}--title`)
        );
        this.navController.pop();
      }
    } finally {
      this.isTripRequestLoading = false;
    }
  }

  public rejectTripRequest(): void {
    this.navController.navigateBack('home/overview');
  }

  public getStateParsedURL(stateName: string = ''): string {
    return `url(/assets/states/${stateName
      .replace(/\s/g, '_')
      .toLowerCase()}.jpeg)`;
  }
}
