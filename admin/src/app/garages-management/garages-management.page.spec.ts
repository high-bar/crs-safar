import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaragesManagementPage } from './garages-management.page';

describe('GaragesManagementPage', () => {
  let component: GaragesManagementPage;
  let fixture: ComponentFixture<GaragesManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaragesManagementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaragesManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
