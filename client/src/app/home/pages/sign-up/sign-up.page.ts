import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '@app/home/services/util/util.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { FormBuilderHelper } from '@common/classes/form-builder-helper.class';
import { UserRole } from '@common/enums';
import { GoogleLoginResponse, UserFacebookData } from '@common/models';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage {
  private googleLoginResponse: GoogleLoginResponse | string;
  private userFacebookData: UserFacebookData | string;
  public states: string[] = [];
  public isLoading: boolean;
  public isThirdPartyLogin: boolean;

  public signUpForm: FormGroup = this.formBuilder.group({
    fullName: [undefined, [Validators.required]],
    email: [undefined, [Validators.required, Validators.email]],
    emailConfirm: [
      undefined,
      [
        Validators.required,
        Validators.email,
        FormBuilderHelper.matchValues('email'),
      ],
    ],
    password: [undefined, [Validators.required, Validators.minLength(8)]],
    passwordConfirm: [
      undefined,
      [
        Validators.required,
        Validators.minLength(8),
        FormBuilderHelper.matchValues('password'),
      ],
    ],
    phone: [
      undefined,
      [Validators.required, Validators.pattern('(01)[0-9]{9}')],
    ],
    gender: [undefined, [Validators.required]],
    country: ['egypt', [Validators.required]],
    state: [undefined, [Validators.required]],
    pictureURL: [undefined],
  });

  constructor(
    private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private utilService: UtilService,
    private authService: AuthService,
    private translateService: TranslateService,
    private navController: NavController
  ) {
    this.loadCountryStates();
    this.googleLoginResponse = localStorage.getItem('googleLoginResponse');
    this.userFacebookData = localStorage.getItem('userFacebookData');

    if (!!this.googleLoginResponse) {
      this.googleLoginResponse = JSON.parse(
        this.googleLoginResponse
      ) as GoogleLoginResponse;
      this.isThirdPartyLogin = true;
      this.signUpForm.patchValue({
        fullName: this.googleLoginResponse.displayName,
        email: this.googleLoginResponse.email,
        pictureURL: this.googleLoginResponse.imageUrl,
      });
      this.signUpForm.controls.emailConfirm.disable();
      this.signUpForm.controls.password.disable();
      this.signUpForm.controls.passwordConfirm.disable();
    } else if (!!this.userFacebookData) {
      this.userFacebookData = JSON.parse(
        this.userFacebookData
      ) as UserFacebookData;
      this.isThirdPartyLogin = true;
      this.signUpForm.patchValue({
        fullName: this.userFacebookData.displayName,
        email: this.userFacebookData.email,
        pictureURL: this.userFacebookData.imageUrl,
      });
      this.signUpForm.controls.emailConfirm.disable();
      this.signUpForm.controls.password.disable();
      this.signUpForm.controls.passwordConfirm.disable();
    }
  }

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      const pictureURL = await this.profileService.initPhotoPicking();
      if (!!pictureURL) {
        this.signUpForm.patchValue({
          pictureURL: pictureURL,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async submitSignUp(): Promise<void> {
    if (this.signUpForm.valid) {
      this.isLoading = true;
      this.signUpForm.value.role = UserRole.CLIENT;

      try {
        if (!!this.googleLoginResponse) {
          await this.authService.firebaseGoogleSignup(
            this.googleLoginResponse as GoogleLoginResponse,
            this.signUpForm.value
          );
        } else if (!!this.userFacebookData) {
          await this.authService.firebaseFacebookSignup(
            this.userFacebookData as UserFacebookData,
            this.signUpForm.value
          );
        } else {
          await this.authService.signUp(this.signUpForm.value);
        }
        await this.navController.navigateRoot('home');
      } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.${error.code}`)
          );
        } else {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        }
      } finally {
        this.isLoading = false;
      }
    }
  }
}
