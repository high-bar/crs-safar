// tslint:disable:no-implicit-dependencies
import * as functions from 'firebase-functions';
import * as algoliasearch from 'algoliasearch';

const ALGOLIA_ID = functions.config().algolia.app_id;
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key;
const ALGOLIA_SEARCH_KEY = functions.config().algolia.search_key;

const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

const updateAlgoiaIndex = (
  object: any,
  objectId: string,
  collection: string,
  context: functions.EventContext
) => {
  // Add an 'objectID' field which Algolia requires
  object.objectID = objectId;

  const index = client.initIndex(collection);
  if (context.eventType === 'delete') {
    return index.deleteObject(object);
  } else {
    return index.saveObject(object);
  }
};

export const freeSearch = functions.https.onCall(async (data, _context) => {
  const searchClient = algoliasearch(ALGOLIA_ID, ALGOLIA_SEARCH_KEY);
  const index = searchClient.initIndex(data.collection);

  // Perform an Algolia search:
  // https://www.algolia.com/doc/api-reference/api-methods/search/#response-format

  try {
    return (await index.search({
      query: data.query,
    })).hits;
  } catch (error) {
    return new functions.https.HttpsError('internal', 'Algolia error');
  }
});

// Update the search index every time a blog post is written.
export const carIndexUpdate = functions.firestore
  .document('cars/{carId}')
  .onWrite((snapshot, context) => {
    return updateAlgoiaIndex(
      snapshot.after.data() as any,
      context.params.carId,
      'cars',
      context
    );
  });
