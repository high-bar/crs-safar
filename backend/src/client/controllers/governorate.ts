import * as mongoose from 'mongoose';
import { GovernorateSchema } from '../../common/models/governorate';
import { Request, Response } from 'express';

const Governorate = mongoose.model('Governorate', GovernorateSchema);

export class GovernorateController {
  public getGovernorates(req: Request, res: Response) {
    // find Governorates
    Governorate.find({}, function(err: any, Governorates: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Governorates);
      }
    }); // end find all Governorates
  }
  //
  public SetGovernorate(req: Request, res: Response) {
    //set values
    let newGovernorate = new Governorate({
      placeid: req.body.placeid,
      name: req.body.name
    });
    //save Governorate
    newGovernorate.save((err, Governorate) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: "Governorate Added!",
        governorate: Governorate,
      });
    }); //end save Governorate
  }
}
