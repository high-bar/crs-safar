import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { CurrentClientsComponent } from '@app/shared/components/current-clients/current-clients.component';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { FilterTripComponent } from '../shared/components/filter-trip/filter-trip.component';

import { PickOnMapModalComponent } from './components/pick-on-map-modal/pick-on-map-modal.component';
import { PlaceSearchSelectComponent } from './components/place-search-select/place-search-select.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';
import { GoogleMapsService } from './services/google-maps/google-maps.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HomeRoutingModule,
    MatButtonModule,
  ],
  declarations: [HomePage, PlaceSearchSelectComponent, PickOnMapModalComponent],
  entryComponents: [
    PickOnMapModalComponent,
    FilterTripComponent,
    CurrentClientsComponent,
  ],
  providers: [GoogleMapsService],
})
export class HomePageModule {}
