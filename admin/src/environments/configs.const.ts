import { Configs } from './configs.model';

export const CONFIGS: Configs = {
  pushOptions: {
    android: {
      forceShow: true,
      topics: ['admin-users'],
      icon: 'fcm_push_icon',
      iconColor: '#000000',
    },
  },
};
