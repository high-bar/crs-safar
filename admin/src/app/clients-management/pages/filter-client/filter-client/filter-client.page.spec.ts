import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterClientPage } from './filter-client.page';

describe('FilterClientPage', () => {
  let component: FilterClientPage;
  let fixture: ComponentFixture<FilterClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterClientPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
