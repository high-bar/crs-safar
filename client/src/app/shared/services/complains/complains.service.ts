import { Injectable } from '@angular/core';
import { Client, Complain } from '@common/models';
import { firestore } from 'firebase';

import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class ComplainsService {
  constructor(private authService: AuthService) {}

  public addNewComplain(
    complain: Partial<Complain>
  ): Promise<firestore.DocumentData> {
    complain.joiningDate = new Date().getTime();
    const user: Client = JSON.parse(this.authService.getSavedUser());
    complain.sender = {
      fullName: user.fullName,
      email: user.email,
      phone: user.phone,
      pictureURL: user.pictureURL,
      uid: user.uid,
    };
    return firestore()
      .collection('complains')
      .add(complain);
  }
}
