export const Localization = {
  speed_warning_title: (name: string, speedLimit: number) => {
    return `تخطى: ${name} حدّ السرعة! (${speedLimit}كم/س)`;
  },
  speed_warning_body: (
    name: string,
    currentSpeed: string,
    accuracy: number
  ) => {
    return `سرعة ${name} الحالية ${currentSpeed}كم/س. نسبة الخطأ: ${accuracy}م/ث`;
  },
};
