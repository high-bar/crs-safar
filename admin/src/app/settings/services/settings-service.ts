import { Injectable } from '@angular/core';
import { DriverStatus, UserRole } from '@common/enums';
import { Driver, PriceList, SystemFlags, User } from '@common/models';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  constructor() {}

  public getBlockedUsers(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('users')
        .where('isBlocked', '==', true)
        .orderBy('blockDate', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public unBlockUser(user: User): Promise<void> {
    const updatedInfo: Partial<User> = {
      isBlocked: false,
      blockDate: new Date().getTime(),
    };
    if (user.role === UserRole.DRIVER) {
      (updatedInfo as Driver).status = DriverStatus.IDLE;
    }
    return firestore()
      .collection('users')
      .doc(user.uid)
      .update(updatedInfo);
  }

  public getPriceList(): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('prices')
      .doc('cairo')
      .get();
  }

  public async updatePrices(priceList: PriceList): Promise<void> {
    await firestore()
      .collection('prices')
      .doc('cairo')
      .update({ states: priceList });
  }

  public getFlags(): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('util')
      .doc('flags')
      .get();
  }

  public async updateFlags(newFlags: SystemFlags): Promise<void> {
    await firestore()
      .collection('util')
      .doc('flags')
      .update(newFlags);
  }
}
