import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCarPage } from './filter-car.page';

describe('FilterCarPage', () => {
  let component: FilterCarPage;
  let fixture: ComponentFixture<FilterCarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterCarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterCarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
