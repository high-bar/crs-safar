import { TestBed } from '@angular/core/testing';

import { TripsHistoryService } from './trips-history.service';

describe('TripsHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TripsHistoryService = TestBed.get(TripsHistoryService);
    expect(service).toBeTruthy();
  });
});
