import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  constructor(
    private alertController: AlertController,
    private translateService: TranslateService
  ) {}

  public getCountryData(
    country: string = 'egypt'
  ): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('countries')
      .doc(country)
      .get();
  }

  public async errorAlert(errorMessage: string): Promise<void> {
    return (await this.alertController.create({
      header: this.translateService.instant('error_alert.title'),
      message: errorMessage,
      buttons: [this.translateService.instant('error_alert.ok')],
    })).present();
  }
}
