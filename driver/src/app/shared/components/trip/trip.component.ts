import { Component, Input } from '@angular/core';
import { Trip } from '@common/models';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.scss'],
})
export class TripComponent {
  @Input() trip: Trip;

  constructor() {}

  public getStateParsedURL(stateName: string = ''): string {
    return `url(/assets/states/${stateName
      .replace(/\s/g, '_')
      .toLowerCase()}.jpeg)`;
  }
}
