import { BookingTripController } from '../controllers/bookingtrip';
import { GetMyTrip } from '../controllers/mytrips';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class BookingTripRoutes {
  public bookingTripController: BookingTripController = new BookingTripController();
  public getMyTrip: GetMyTrip = new GetMyTrip();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      var token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //

    // start client route
    // Create a new trip
    app
      .route('/client/addbooktrip')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'Failed to authenticate',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.bookingTripController.bookTrip.bind(this.bookingTripController));
    ////

    // Create all trip
    app
      .route('/client/getmytrips')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.getMyTrip.mytrip.bind(this.getMyTrip));
    ////
    ////

    // get Active trip
    app
      .route('/client/getactivetrips')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.getMyTrip.mytripActive.bind(this.getMyTrip));
    ////

    // get next trip
    app
      .route('/client/getnexttrips')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.getMyTrip.mytripNext.bind(this.getMyTrip));
    ////

    // get next trip
    app
      .route('/client/gettrip')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.getMyTrip.mytrip0.bind(this.getMyTrip));
    ////

    // get Pending trip
    app
      .route('/client/getpendingtrips')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });
          ///////////
        }
      }, this.getMyTrip.PendingTrip.bind(this.getMyTrip));
    ////    ////    ////
    // get all trip by status //
    app.route('/admin/getallpendingtrips').get(this.getMyTrip.allpendingtrips);
    // // //
    app.route('/admin/getallnexttrips').get(this.getMyTrip.allnexttrips);
    // // //
    app.route('/admin/getallactivetrips').get(this.getMyTrip.allactivetrips);
    // // //
    // history with index //
    app
      .route('/client/getusertriphistory1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.getMyTrip.GetUserTripHistory1.bind(this.getMyTrip));
    //////////////
    app
      .route('/client/getusertriphistory2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.getMyTrip.GetUserTripHistory2.bind(this.getMyTrip));
    //////////////
    // // //
    // end route
  }

  //end class
}
