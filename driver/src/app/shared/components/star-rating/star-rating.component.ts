import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent {
  private numberOfStars = 5;
  public stars: number[] = new Array(this.numberOfStars)
    .fill(0)
    .map((_starNumber, starI) => {
      return starI + 1;
    });

  @Input() public value = 1;

  constructor() {}

  public getStarFillPercentage(star: number): number {
    return (
      Math.round((star / this.stars.length - 1 / this.stars.length / 2) * 10) /
      10
    );
  }
}
