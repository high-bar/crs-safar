import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language } from '@common/enums';
import { Localization, SystemFlags } from '@common/models';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ModalController } from '@ionic/angular';

import { BlockedUsersComponent } from './components/blocked-users/blocked-users.component';
import { SettingsService } from './services/settings-service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {
  public selectedLocalization: Localization;
  public appVersionNumber: string;
  public isAuthorized: boolean;

  public systemFlags: SystemFlags;
  public originalSystemFlags: SystemFlags;

  public isLoading: boolean;

  constructor(
    private appVersion: AppVersion,
    private localizationService: LocalizationService,
    public authService: AuthService,
    private settingsService: SettingsService,
    private modalController: ModalController
  ) {
    this.authService.change.subscribe(user => {
      this.isAuthorized = !!user;
    });

    this.selectedLocalization = this.localizationService.getLocalization();
    this.appVersion
      .getVersionNumber()
      .then(appVersionNumber => (this.appVersionNumber = appVersionNumber));
    this.loadFlags();
  }

  public changeLanguage(newLanguage: Language): void {
    this.localizationService.setLocalization({ lang: newLanguage });
  }

  private async loadFlags(): Promise<void> {
    this.systemFlags = (await this.settingsService.getFlags()).data() as SystemFlags;
    this.originalSystemFlags = JSON.parse(JSON.stringify(this.systemFlags));
  }

  public didSettingsChange(): boolean {
    return (
      !!this.systemFlags &&
      !!this.originalSystemFlags &&
      JSON.stringify(this.systemFlags) !==
        JSON.stringify(this.originalSystemFlags)
    );
  }

  public async saveNewFlags(): Promise<void> {
    this.isLoading = true;
    try {
      await this.settingsService.updateFlags(this.systemFlags);
      this.originalSystemFlags = JSON.parse(JSON.stringify(this.systemFlags));
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async openBlockedUsersModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: BlockedUsersComponent,
    });
    return await modal.present();
  }
}
