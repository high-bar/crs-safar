export enum TripStatus {
  NEW = 'new',
  FUTURE = 'future',
  CURRENT = 'current',
  PAST = 'past',
  PENDING_PAST = 'pending past',
  ARCHIVED = 'archived',
}
