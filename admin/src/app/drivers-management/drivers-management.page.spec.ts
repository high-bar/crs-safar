import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriversManagementPage } from './drivers-management.page';

describe('DriversManagementPage', () => {
  let component: DriversManagementPage;
  let fixture: ComponentFixture<DriversManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriversManagementPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
