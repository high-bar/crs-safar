import * as mongoose from 'mongoose';
import {AdminSchema}from '../../common/models/admin';
import {Request, Response }from 'express';
import {config }from '../../const/config';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

const Admin = mongoose.model('Admin', AdminSchema);

export class AdminSignUpController {
  public addAdmin(req:Request, res:Response) {
    // let newAdmin = new Admin(req.body);
    //
    function createToken(Admin:any) {
      var token = jwt.sign( {
        id:Admin._id,
        fullName:Admin.fullName,
        email:Admin.email,
      }, secretKey);
      return token;
    }
    //
    //check if Admin already registerd
    Admin.findOne({email : req.body.email}, function (err, checkexists) {
       if (checkexists) {
        res.status(401).json({
          error: 'invalid admin',
        });
     }else {
    // get password and hashing it
    let password = req.body.password;
    let NewPassword = bcrypt.hashSync(password, 10);
    //setting up Admin data to save it
    let newAdmin = new Admin({
      fullName:req.body.fullName,
      email:req.body.email,
      password:NewPassword,
      joiningDate:req.body.joiningDate
    });

    // save user data
    newAdmin.save((err, Admin) =>  {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      let token = createToken(Admin);
      res.json( {
        Admin,
      token:token
    });

    });   //end save Admin

  }// end else

}) //end find one function

    //
  }
}