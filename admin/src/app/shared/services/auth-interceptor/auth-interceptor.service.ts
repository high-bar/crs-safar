import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const matchArr = req.url.match(environment.ROOT_API_URL);
    // if (!!matchArr && matchArr.length >= 1) {
    //   const modified = req.clone({
    //     setHeaders: { 'x-access-token': this.authService.getToken() },
    //   });
    //   return next.handle(modified);
    // } else {
    //   return next.handle(req);
    // }
    return next.handle(req);
  }
}
