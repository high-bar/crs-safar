import { DriverStatus } from '@common/enums';

export interface DriverFilter {
  fullName: string;
  email: string;
  phone: string;
  rate: number;
  status: DriverStatus;
}
