export interface GoogleLoginResponse {
  accessToken: string;
  displayName: string;
  email: string;
  expires: number;
  expires_in: number;
  familyName: string;
  givenName: string;
  idToken: string;
  imageUrl: string;
  userId: string;
}
