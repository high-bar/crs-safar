import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '@app/home/services/util/util.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { Client } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  public editProfileForm: FormGroup = this.formBuilder.group({
    fullName: [undefined, [Validators.required]],
    phone: [
      undefined,
      [Validators.required, Validators.pattern('(01)[0-9]{9}')],
    ],
    state: [undefined, [Validators.required]],
    pictureURL: [undefined],
  });
  public isLoading: boolean;
  @Input() public profileData: Client;
  public states: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private profileService: ProfileService,
    private utilService: UtilService,
    private authService: AuthService
  ) {
    this.loadCountryStates();
  }

  ngOnInit() {
    if (!!this.profileData) {
      // pass only values used in the form
      this.editProfileForm.setValue({
        fullName: this.profileData.fullName,
        phone: this.profileData.phone,
        state: this.profileData.state || ''.trim(),
        pictureURL: this.profileData.pictureURL || '',
      });
    }
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      const pictureURL = await this.profileService.initPhotoPicking();
      if (!!pictureURL) {
        this.editProfileForm.patchValue({
          pictureURL: pictureURL,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async submitEditProfile(): Promise<void> {
    if (this.editProfileForm.valid) {
      this.isLoading = true;
      if (!!this.profileData) {
        this.profileData.fullName = this.editProfileForm.value.fullName;
        this.profileData.phone = this.editProfileForm.value.phone;
        this.profileData.state = this.editProfileForm.value.state;
        this.profileData.pictureURL = this.editProfileForm.value.pictureURL;
        try {
          await this.authService.editProfile(this.profileData);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      }
    }
  }
}
