import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const GovernorateSchema = new Schema( {
  placeid: {
    type:String
  },
  name: {
    type:String
  },
  gname: {
    type:String
  }
});
