import { CarType } from '@common/enums/car-type.enum';

export interface CarTypeItem {
  type: CarType;
  icon: string;
}
