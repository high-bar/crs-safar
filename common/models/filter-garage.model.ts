import { Car, Driver } from '@common/models';

export interface Garage {
  id: number;
  name: string;
  address: string;
  cars: Car[];
  drivers: Driver[];
  joiningDate: number;
  phone: string;
}
