import * as mongoose from 'mongoose';
import {DriverSchema}from '../../common/models/driver';
import {Request, Response }from 'express';
import {config }from '../../const/config';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

const Driver = mongoose.model('driver', DriverSchema);

export class DriverSignUpController {
  public addDriver(req:Request, res:Response) {
    // let newDriver = new Driver(req.body);
    //
    function createToken(Driver:any) {
      var token = jwt.sign( {
        id:Driver._id,
        fullName:Driver.fullName,
        email:Driver.email,
        phone:Driver.phone,
        gender:Driver.gender,
        country:Driver.country,
        state:Driver.state,
        rate:Driver.rate
      }, secretKey);
      return token;
    }
    //
    //check if Driver already registerd
    Driver.findOne({email : req.body.email}, function (err, checkexists) {
       if (checkexists) {
        res.status(401).json({
          error: 'email already registered',
        });
        }else {
    // get password and hashing it
    let password = req.body.password;
    let NewPassword = bcrypt.hashSync(password, 10);
    //setting up Driver data to save it
    let newDriver = new Driver({
      fullName:req.body.fullName,
      email:req.body.email,
      phone:req.body.phone,
      gender:"Male",
      country:req.body.country,
      state:req.body.state,
      rate:"5",
      balance:"0",
      salary:"0",
      password:NewPassword,
      joiningDate:req.body.joiningDate,
      isApproved :"false"
    });

    // save user data
    newDriver.save((err, Driver) =>  {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      let token = createToken(Driver);
      res.json( {
        Driver,
      token:token
    });

    });   //end save Driver

  }// end else

}) //end find one function

    //
  }
}