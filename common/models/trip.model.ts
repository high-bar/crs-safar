import { CarType, DriverTripStatus, TripStatus } from '@common/enums';
import {
  BackgroundGeolocation,
  Car,
  Client,
  DistanceMatrix,
  Driver,
  Place,
  PricingInEGP,
  Review,
} from '@common/models';

export interface Trip {
  id: string;
  pickupPlace: Place;
  dropPlace: Place;
  coverPictureURL: string;
  pickupDateTime: number;
  goingDateTime: number;
  isTwoWay: boolean;
  stayingPeriodInHours: number;
  pricing: Partial<PricingInEGP>;
  carType: CarType;
  car: Car;
  client: Partial<Client>;
  driver: Partial<Driver>;
  clientReview: Review;
  driverReview: Review;
  status: TripStatus;
  driverStatus: DriverTripStatus;
  isQueued: boolean;
  paymentMethod: any;
  requestDateTime: number;
  conclusionDateTime: number;
  route: BackgroundGeolocation[];
  distanceMatrix: DistanceMatrix;
  distanceMatrixEst: DistanceMatrix;
  driversRequest: {
    sentOnDateTime: number;
    driversUids: string[];
  };
  promo: any;
}
