export interface PricingInEGP {
  base: number;
  beforePromo: number;
  driverSalary: number;
  actualExtra?: number;
  collectedExtra?: number;
}
