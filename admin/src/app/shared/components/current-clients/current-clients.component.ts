import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '@app/clients-management/services/client-service/client.service';
import { Client } from '@common/models';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-current-clients',
  templateUrl: './current-clients.component.html',
  styleUrls: ['./current-clients.component.scss'],
})
export class CurrentClientsComponent implements OnInit {
  constructor(
    private clientService: ClientService,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute
  ) {}
  public currentClients: Client[] = [];
  public isLoading: boolean;
  @Input() public selectedClients: { [key: string]: boolean } = {};
  @Input() isMultiSelect: boolean;
  @Input() public tripId;
  @Input() isModal = false;

  ngOnInit() {
    this.loadCurrentClients();
  }

  private async loadCurrentClients(): Promise<void> {
    this.isLoading = true;
    this.clientService.getCurrentClient().subscribe(
      data => {
        this.currentClients = data.docs.map(doc => {
          const client = doc.data() as Client;
          client.uid = doc.id;
          return client;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async selectClient(client: Client): Promise<void> {
    if (this.isModal) {
      await this.navController.navigateForward(['/client/' + client.uid], {
        relativeTo: this.route,
      });
    } else if (!this.isMultiSelect) {
      // driver.currentTripId = this.tripId;
      await this.modalController.dismiss(client);
    }
  }

  public async selectMultiClient(): Promise<void> {
    await this.modalController.dismiss(this.selectedClients);
  }

  public selectAllCurrentClients(isAllSelected: boolean): void {
    this.currentClients.forEach(client => {
      this.selectedClients[client.uid] = isAllSelected;
    });
  }

  public getSelectedCount() {
    return Object.keys(this.selectedClients).filter(clientUID => {
      return !!this.selectedClients[clientUID];
    }).length;
  }
}
