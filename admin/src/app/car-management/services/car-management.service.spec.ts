import { TestBed } from '@angular/core/testing';

import { CarManagementService } from './car-management.service';

describe('CarManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarManagementService = TestBed.get(CarManagementService);
    expect(service).toBeTruthy();
  });
});
