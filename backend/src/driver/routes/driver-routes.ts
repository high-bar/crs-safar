import { DriverSignUpController } from '../controllers/signup';
import { DriverSignInController } from '../controllers/login';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class DriverRoutes {
  public DriverSignUpController: DriverSignUpController = new DriverSignUpController();
  public DriverSignInController: DriverSignInController = new DriverSignInController();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      var token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //
    // start client route
    // driver trips history
    app
      .route('/driver/profile')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverSignInController.DriverProfile.bind(
        this.DriverSignInController
      ));
    ////

    // Create a new client
    app.route('/driver/signup').post(this.DriverSignUpController.addDriver);
    ////
    // client Login
    app.route('/driver/signin').post(this.DriverSignInController.getDriver);
    ////
//upload profile image
app
.route('/driver/updateprofilepicture')
.post((req: Request, res: Response, next: NextFunction) => {
  // middleware
  let token = req.headers['x-access-token'];
  if (!token) {
    res.status(403).json({
      error: 'No Token Provided',
    });
  } else {
    /////////
    jwt.verify(token, secretKey, function(err: any, decoded: any) {
      if (err) {
        res.status(403).json({
          error: "Failed to authenticate"
        });
      } else {
        //
        (<any>req).decoded = decoded;

        next();
      }
    });

    ///////////
  }
}, this.DriverSignInController.UploadProfile.bind(
  this.DriverSignInController
));
////
    // end client route
  }

  //end class
}
