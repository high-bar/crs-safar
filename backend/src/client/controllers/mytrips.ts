import * as mongoose from 'mongoose';
import { TripSchema } from '../../common/models/trip';
import { Request, Response } from 'express';
const https = require('https');

const Trip = mongoose.model('Trip', TripSchema);
export class GetMyTrip {
  constructor() {}
  public async mytrip(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    Trip.find({ client: userid }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  public async mytripActive(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let status = 'active';
    Trip.find({ client: userid, status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  public async mytrip0(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let tripid = req.body.tripid;
    Trip.find({ client: userid, _id: tripid }, function(err, trip) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trip);
    });
  } //edn mytrip

  /////////////////

  public async mytripNext(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let status = 'next';
    Trip.find({ client: userid, status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  /////////////////

  public async PendingTrip(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let status = 'pending';
    Trip.find({ client: userid, status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  //////////////////////////////// get all trips /////////

  /////////////////

  public async allpendingtrips(req: Request, res: Response): Promise<void> {
    let status = 'pending';
    Trip.find({ status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////
  /////////////////

  public async allnexttrips(req: Request, res: Response): Promise<void> {
    let status = 'next';
    Trip.find({ status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  /////////////////

  public async allactivetrips(req: Request, res: Response): Promise<void> {
    let status = 'active';
    Trip.find({ status: status }, function(err, trips) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////
  // get trip history1
  public GetUserTripHistory1(req: Request, res: Response) {
    let userid = (<any>req).decoded.id;
    Trip.find({ client: userid }, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetUserTripHistory2(req: Request, res: Response) {
    let userid = (<any>req).decoded.id;
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({ client: userid })
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  /////////////////
} //end booking trip class
