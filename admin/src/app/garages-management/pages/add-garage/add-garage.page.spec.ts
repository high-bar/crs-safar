import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGaragePage } from './add-garage.page';

describe('AddGaragePage', () => {
  let component: AddGaragePage;
  let fixture: ComponentFixture<AddGaragePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGaragePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGaragePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
