import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GarageService } from '@app/garages-management/services/garage.service';
import { Garage } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-garage',
  templateUrl: './add-garage.page.html',
  styleUrls: ['./add-garage.page.scss'],
})
export class AddGaragePage implements OnInit {
  public addGarageForm: FormGroup = this.formBuilder.group({
    name: [undefined, [Validators.required]],
    phone: [undefined, [Validators.required]],
    address: [undefined, [Validators.required]],
    notes: [undefined, [Validators]],
  });
  public isLoading: boolean;
  @Input() public garage: Garage;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private garageService: GarageService
  ) {}

  ngOnInit() {
    if (!!this.garage) {
      // pass only values used in the form
      this.addGarageForm.setValue({
        name: this.garage.name,
        phone: this.garage.phone,
        address: this.garage.address,
        notes: this.garage.notes,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitAddGarage(): Promise<void> {
    if (this.addGarageForm.valid) {
      this.isLoading = true;
      if (!!this.garage) {
        this.garage.name = this.addGarageForm.value.name;
        this.garage.phone = this.addGarageForm.value.phone;
        this.garage.address = this.addGarageForm.value.address;
        this.garage.notes = this.addGarageForm.value.notes;
        try {
          this.garageService.editGarage(this.garage);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      } else {
        try {
          await this.garageService.addNewGarage(this.addGarageForm.value);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      }
    }
  }
}
