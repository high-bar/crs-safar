import * as mongoose from 'mongoose';
import { CarSchema } from '../../common/models/car';
import { Request, Response } from 'express';
const https = require('https');

const Car = mongoose.model('Car', CarSchema);

export class CarController {
  constructor() {}
  // add car
  public AddCar(req: Request, res: Response) {
    //set values
    let newCar = new Car({
      plateNumber: req.body.plateNumber,
      type: req.body.type,
      model: req.body.model,
      brand: req.body.brand,
      year: req.body.year,
      status: req.body.status,
      notes: req.body.notes,
    });
    //save Car
    newCar.save((err, Car) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: 'Car Added!',
        Car: Car,
      });
    }); //end save Car
  }
  //
  public getAllCars(req: Request, res: Response) {
    // find Car
    Car.find({}, function(err: any, Car: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Car);
      }
    }); // end find all Car
  }
  //
  //get cars first
  public getCars1(req: Request, res: Response) {
    // find Car
    Car.find({}, function(err: any, Car: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Car);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all Car
  }
  //
  //get cars next skip
  public getCars2(req: Request, res: Response) {
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find Car
    console.log(limit);
    Car.find({})
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Car: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Car);
        }
      }); // end find all Car
  }
  //
  // update car
  public UpdateCar(req: Request, res: Response) {
    let carid = req.body.carid;
    let plateNumber = req.body.plateNumber;
    let type = req.body.type;
    let model = req.body.model;
    let brand = req.body.brand;
    let year = req.body.year;
    let status = req.body.status;
    let notes = req.body.notes;
    //Update
    Car.findOneAndUpdate(
      { _id: carid },
      {
        $set: {
          plateNumber: plateNumber,
          type: type,
          model: model,
          brand: brand,
          year: year,
          status: status,
          notes: notes,
        },
      },
      function(err, Car) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json({
            message: 'Car Updated!',
            Car: Car,
          });
        }
      }
    );
  } //end
  //
  //delete car
  public DeleteCar(req: Request, res: Response) {
    let carid = req.body.carid;
    //delete
    Car.findByIdAndRemove({ _id: carid }, function(err, Car) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json({
          message: 'Car Deleted!',
          Car: Car,
        });
      }
    });
  }
  //
  //
} //end Car
