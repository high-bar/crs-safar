import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { UtilService } from '@app/shared/services/util/util.service';
import { FormBuilderHelper } from '@common/classes/form-builder-helper.class';
import { DriverStatus, UserRole } from '@common/enums';
import { Driver, Localization } from '@common/models';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage {
  public documentDir = 'rtl';
  public isLoading: boolean;
  public states: string[] = [];

  public signUpForm: FormGroup = this.formBuilder.group({
    fullName: [undefined, [Validators.required]],
    email: [undefined, [Validators.required, Validators.email]],
    password: [undefined, [Validators.required, Validators.minLength(8)]],
    confirmPassword: [
      undefined,
      [
        Validators.required,
        Validators.minLength(8),
        FormBuilderHelper.matchValues('password'),
      ],
    ],
    phone: [
      undefined,
      [Validators.required, Validators.pattern('(01)[0-9]{9}')],
    ],
    state: [undefined, Validators.required],
    address: [undefined, Validators.required],
    pictureURL: [undefined],
  });

  constructor(
    private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private navController: NavController,
    private translateService: TranslateService
  ) {
    LocalizationService.localizationChange.subscribe(
      (newLocalization: Localization) => {
        this.documentDir = newLocalization.dir;
      }
    );
    this.loadCountryStates();
  }

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      const pictureURL = await this.profileService.initPhotoPicking();
      if (!!pictureURL) {
        this.signUpForm.patchValue({
          pictureURL: pictureURL,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async submitSignUp(): Promise<void> {
    if (this.signUpForm.valid) {
      this.isLoading = true;
      const driverInfo: Partial<Driver> = {
        rate: 5,
        role: UserRole.DRIVER,
        status: DriverStatus.IDLE,
        isApproved: false,
      };

      try {
        await this.authService.signUp({
          ...this.signUpForm.value,
          ...driverInfo,
        });
        await this.navController.navigateRoot('home/overview');
      } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.${error.code}`)
          );
        } else {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        }
      } finally {
        this.isLoading = false;
      }
    }
  }
}
