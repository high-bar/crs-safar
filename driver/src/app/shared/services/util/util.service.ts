import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { firestore } from 'firebase';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  constructor(
    private http: HttpClient,
    private alertController: AlertController,
    private translateService: TranslateService
  ) {}

  public getCountryData(
    country: string = 'egypt'
  ): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('countries')
      .doc(country)
      .get();
  }

  public async errorAlert(message: string, title: string = this.translateService.instant('error_alert.title')): Promise<void> {
    return (await this.alertController.create({
      header: title,
      message: message,
      buttons: [this.translateService.instant('error_alert.ok')],
    })).present();
  }
}
