import { Country } from '@common/models';

export interface CountryState {
  name: string;
  code: string;
  country?: Country;
}
