import { CountryController } from "../controllers/country";

export class CountryRoutes {
  public CountryController: CountryController = new CountryController();

  public routes(app: any): void {
    // start countries route
    // get countries
    app.route('/getcountries').get(this.CountryController.getCountries);
    ////
    // countries set
    app.route('/setcountry').post(this.CountryController.SetCountry);
    ////
    // end countries route
  }

  //end class
}
