import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Driver } from '@common/models/driver.model';

import { DriverService } from '../services/driver.service';

@Component({
  selector: 'app-driver-profile',
  templateUrl: './driver-profile.page.html',
  styleUrls: ['./driver-profile.page.scss'],
})
export class DriverProfilePage implements OnInit {
  isProfileLoading: boolean;
  public driver: Driver;

  constructor(
    private driverService: DriverService,
    private activatedRoute: ActivatedRoute
  ) {
    this.loadDriverDetailsById(this.activatedRoute.snapshot.params.id);
  }

  ngOnInit() {}

  public async loadDriverDetailsById(uid): Promise<void> {
    this.isProfileLoading = true;
    try {
      this.driver = (await this.driverService.getDriverByID(
        uid
      )).data() as Driver;
    } catch (error) {
      console.error(error);
    } finally {
      this.isProfileLoading = false;
    }
  }
}
