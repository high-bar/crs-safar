import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFilterPage } from './admin-filter.page';

describe('AdminFilterPage', () => {
  let component: AdminFilterPage;
  let fixture: ComponentFixture<AdminFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
