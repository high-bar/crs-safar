import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { PaymentCollectingComponent } from './components/payment-collecting/payment-collecting.component';
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { TripComponent } from './components/trip/trip.component';
import { LocalizationService } from './services/localization/localization.service';

@NgModule({
  declarations: [
    TripComponent,
    StarRatingComponent,
    ProfileHeaderComponent,
    PaymentCollectingComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxContentLoadingModule,
  ],
  exports: [
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    TripComponent,
    ProfileHeaderComponent,
    NgxContentLoadingModule,
  ],
  providers: [LocalizationService],
  entryComponents: [PaymentCollectingComponent],
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
    };
  }
}
