import { Component, OnInit } from '@angular/core';
import { Complain } from '@common/models';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { ComplainDetailsPage } from './pages/complain-details/complain-details.page';
import { ComplainsService } from './services/complains.service';

@Component({
  selector: 'app-complains-management',
  templateUrl: './complains-management.page.html',
  styleUrls: ['./complains-management.page.scss'],
})
export class ComplainsManagementPage implements OnInit {
  private backButtonSubscription: Subscription;

  public isLoading: boolean;
  public complains: Partial<Complain>[] = [];

  constructor(
    private complainsService: ComplainsService,
    private modalController: ModalController,
    private platform: Platform,
    private navController: NavController
  ) {
    this.loadComplains();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async openComplainDetailsModal(complain: Complain): Promise<void> {
    const modal = await this.modalController.create({
      component: ComplainDetailsPage,
      componentProps: {
        complain: complain,
      },
    });
    return await modal.present();
  }

  public async loadComplains(): Promise<void> {
    this.isLoading = true;
    this.complainsService.getComplains().subscribe(
      data => {
        this.complains = data.docs.map(doc => {
          const complain = doc.data();
          complain.id = doc.id;
          return complain;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }
}
