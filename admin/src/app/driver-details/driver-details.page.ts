import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DriverService } from '@app/drivers-management/services/driver-service/driver.service';
import { GarageService } from '@app/garages-management/services/garage.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { Driver, Garage } from '@common/models';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.page.html',
  styleUrls: ['./driver-details.page.scss'],
})
export class DriverDetailsPage implements OnInit {
  @ViewChild('slider', { static: true }) slider: IonSlides;
  public currentTab = 0;
  public details;
  public isProfileLoading;
  public garages: Partial<Garage>[] = [];

  public driver: Driver;
  public isLoading: boolean;
  public isBlockLoading: boolean;

  constructor(
    private driverService: DriverService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private garageService: GarageService
  ) {
    this.loadDriverDetailsById(this.activatedRoute.snapshot.params.id);
    this.loadGarages();
  }

  ngOnInit() {}

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  public async loadDriverDetailsById(uid): Promise<void> {
    this.isProfileLoading = true;
    this.driverService.getDriverByID(uid).subscribe(
      doc => {
        this.driver = doc.data() as Driver;
        this.isProfileLoading = false;
      },
      error => {
        console.error(error);
        this.isProfileLoading = false;
      }
    );
  }

  public async toggleBlock(isBlocked: boolean): Promise<void> {
    this.isBlockLoading = true;
    try {
      await this.driverService.blockDriver(this.driver, isBlocked);
    } catch (error) {
      console.error(error);
    } finally {
      this.isBlockLoading = false;
    }
  }

  public async submitForgotPassword(): Promise<void> {
    this.isLoading = true;

    try {
      await this.authService.resetPassword(this.driver.email);
    } catch (error) {
    } finally {
      this.isLoading = false;
    }
  }

  public async loadGarages(): Promise<void> {
    try {
      this.garages = (await this.garageService.getGaragesOnce()).docs.map(
        doc => {
          return { id: doc.id, name: doc.data().name };
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  public async updateDriverGarage() {
    this.driver.garage = this.garages.find(
      garage => this.driver.garage.id === garage.id
    );
    try {
      await this.driverService.changeDriverGarage(this.driver);
    } catch (error) {
      console.log(error);
    }
  }
}
