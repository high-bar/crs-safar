import { Component, OnInit } from '@angular/core';
import { FilterTripComponent } from '@app/shared/components/filter-trip/filter-trip.component';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { Trip, TripFilter } from '@common/models';
import { NavController, Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { setDate } from 'date-fns';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-trips-history',
  templateUrl: './trips-history.page.html',
  styleUrls: ['./trips-history.page.scss'],
})
export class TripsHistoryPage implements OnInit {
  private backButtonSubscription: Subscription;

  public pastTrips: Partial<Trip>[] = [];
  public incomeTotal = 0;
  public isPastLoading: boolean;
  private currentTripsSubscription: Subscription;
  public tripFilter: Partial<TripFilter> = {
    fromRequestDateTime: setDate(new Date(), 1).getTime(),
    toRequestDateTime: new Date().getTime(),
  };

  constructor(
    private tripsService: TripsService,
    private platform: Platform,
    private navController: NavController,
    private modalController: ModalController
  ) {
    this.loadPastTrips();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async loadPastTrips(): Promise<void> {
    this.isPastLoading = true;
    if (!!this.currentTripsSubscription) {
      this.currentTripsSubscription.unsubscribe();
    }
    this.currentTripsSubscription = this.tripsService
      .getPastTrips(this.tripFilter)
      .subscribe(
        data => {
          this.incomeTotal = 0;
          this.pastTrips = data.docs.map(doc => {
            const trip = doc.data() as Trip;
            trip.id = doc.id;
            this.incomeTotal += _.get(trip, 'pricing.base', 0);
            return trip;
          });
          this.isPastLoading = false;
        },
        error => {
          console.error(error);
          this.isPastLoading = false;
        }
      );
  }

  // public async openFilterTripModal(): Promise<void> {
  //   const modal = await this.modalController.create({
  //     component: FilterTripComponent,
  //   });
  //   return await modal.present();

  //   modal.onWillDismiss().then(modalOutput => {
  //     this.tripFilter = modalOutput.data;
  //   });
  // }

  public async openFilterTripModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterTripComponent,
      componentProps: {
        value: this.tripFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.tripFilter = modalOutput.data;
      this.loadPastTrips();
    });
  }
}
