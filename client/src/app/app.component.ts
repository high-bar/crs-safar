import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language, WritingDir } from '@common/enums';
import { Localization } from '@common/models';
import { Globalization } from '@ionic-native/globalization/ngx';
import { Push, PushObject } from '@ionic-native/push/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  private pushObject: PushObject;
  public documentDir = 'ltr';

  public appPages = [
    {
      title: 'sideMenu.profile',
      url: '/profile',
      icon: 'person',
      canActivate: false,
    },
    {
      title: 'sideMenu.tripsHistory',
      url: '/trips-history',
      icon: 'calendar',
      canActivate: false,
    },
    {
      title: 'sideMenu.payment',
      url: '/payment',
      icon: 'card',
      canActivate: false,
    },

    {
      title: 'sideMenu.complains',
      url: '/complains',
      icon: 'help-circle-outline',
      canActivate: false,
    },
    {
      title: 'sideMenu.rental',
      url: '/rental',
      icon: 'car',
      canActivate: true,
    },
    {
      title: 'sideMenu.settings',
      url: '/settings',
      icon: 'settings',
      canActivate: true,
    },
  ];

  public isAuthorized: boolean;
  public user: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translateService: TranslateService,
    private globalization: Globalization,
    private localizationService: LocalizationService,
    private authService: AuthService,
    private push: Push,
    private navController: NavController
  ) {
    this.initializeApp();
    LocalizationService.localizationChange.subscribe(
      (newLocalization: Localization) => {
        this.documentDir = newLocalization.dir;
      }
    );
    this.authService.change.subscribe(user => {
      if (!this.isAuthorized && !!user) {
        this.user = user;

        this.platform.ready().then(() => {
          this.pushObject = this.push.init(environment.configs.pushOptions);
          this.pushObject.subscribe(this.user.uid);
          this.pushObject.subscribe('clients');
          this.pushObject.on('notification').subscribe(notificationData => {
            if (!!notificationData.additionalData.clickAction) {
              this.navController.navigateForward(
                notificationData.additionalData.clickAction
              );
            }
          });
        });
      } else if (this.isAuthorized && !!!this.user) {
        this.pushObject.unsubscribe(this.user.uid);
      }
      this.isAuthorized = !!user;
    });
  }

  public initializeApp(): void {
    this.translateService.setDefaultLang('en');
    this.platform.ready().then(() => {
      this.setInitialLanguage();
      this.statusBar.backgroundColorByHexString('#780002'); // --ion-color-secondary
      this.statusBar.styleLightContent();
    });
  }

  public onVideoReady() {
    // might should replace with thumbnail
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
  }

  public async setInitialLanguage(): Promise<void> {
    let localization: Localization = this.localizationService.getLocalization();
    try {
      if (!!!localization) {
        localization = {
          lang: (await this.globalization.getPreferredLanguage())
            .value as Language,
        };
        localization.lang =
          localization.lang.split('-')[0].toLowerCase() === Language.ar
            ? Language.ar
            : Language.en;
        this.localizationService.setLocalization(localization);
      }
    } catch (error) {
      console.error(error);
      localization = { lang: Language.en, dir: WritingDir[Language.en] };
      this.localizationService.setLocalization(localization);
    }
    this.localizationService.setLocalization(localization);
  }
}
