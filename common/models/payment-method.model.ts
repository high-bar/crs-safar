import { PaymentMethodType } from '@common/enums/payment-method-type.enum';

export interface PaymentMethod {
  type: PaymentMethodType;
  id: string;
  name: string;
}
