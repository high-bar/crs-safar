import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Driver } from '@common/models';

@Component({
  selector: 'app-driver-card',
  templateUrl: './driver-card.component.html',
  styleUrls: ['./driver-card.component.scss'],
})
export class DriverCardComponent implements OnInit {
  public isLoading: boolean;
  @Input() public driver: Partial<Driver>;
  @Output() public selected: EventEmitter<Partial<Driver>> = new EventEmitter();
  @Input() public shortend: boolean;

  constructor() {}

  ngOnInit() {}

  public async selectDriver(): Promise<void> {
    this.selected.emit(this.driver);
  }
}
