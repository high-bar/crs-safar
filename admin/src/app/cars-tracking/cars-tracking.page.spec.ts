import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsTrackingPage } from './cars-tracking.page';

describe('CarsTrackingPage', () => {
  let component: CarsTrackingPage;
  let fixture: ComponentFixture<CarsTrackingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsTrackingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsTrackingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
