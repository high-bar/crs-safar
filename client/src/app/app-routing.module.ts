import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from '@app/shared/services/auth/auth.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'trip/:tripId',
    loadChildren: () => import('./shared/pages/trip/trip.module').then(m => m.TripPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'trips-history',
    loadChildren: () => import('./trips-history/trips-history.module').then(m => m.TripsHistoryPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule),
  },
  {
    path: 'driver-profile/:id',
    loadChildren:
      () => import('./shared/pages/driver-profile/driver-profile.module').then(m => m.DriverProfilePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then(m => m.PaymentPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'complains',
    loadChildren: () => import('./complains/complains.module').then(m => m.ComplainsPageModule),
    canActivate: [AuthService],
  },
  { path: 'rental', loadChildren: () => import('./rental/rental.module').then(m => m.RentalPageModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
