import * as mongoose from 'mongoose';
import { AdminSchema } from '../../common/models/admin';
import { Request, Response } from 'express';
import { config } from '../../const/config';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

const Admin = mongoose.model('Admin', AdminSchema);

export class AdminSignInController {
  public getAdmin(req: Request, res: Response) {
    //
    function createToken(Admin: any) {
      let token = jwt.sign(
        {
          id: Admin._id,
          fullName: Admin.fullName,
          email: Admin.email,
        },
        secretKey
      );
      return token;
    }
    //
    // find Admin
    Admin.findOne({ email: req.body.email }, function(err: any, Admin: any) {
      if (err) throw err;

      if (!Admin) {
        res.status(401).json({
          error: 'invalid Admin',
        });
      } else if (Admin) {
        let password = req.body.password;
        bcrypt.compare(password, Admin.password).then(function(cresult: any) {
          if (cresult == true) {
            //create token
            let token = createToken(Admin);
            res.json({
              Admin,
              token: token,
            });
          } else {
            res.status(401).json({
              error: 'invalid Admin',
            });
          }
          //
        }); //end bcrypt compare
      } //end else if Admin found
    }); // end find one
  }
}
