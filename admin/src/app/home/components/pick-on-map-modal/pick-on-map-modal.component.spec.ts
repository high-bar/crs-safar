import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickOnMapModalComponent } from './pick-on-map-modal.component';

describe('PickOnMapModalComponent', () => {
  let component: PickOnMapModalComponent;
  let fixture: ComponentFixture<PickOnMapModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickOnMapModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickOnMapModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
