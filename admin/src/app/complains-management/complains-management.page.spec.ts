import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplainsManagementPage } from './complains-management.page';

describe('ComplainsManagementPage', () => {
  let component: ComplainsManagementPage;
  let fixture: ComponentFixture<ComplainsManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplainsManagementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplainsManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
