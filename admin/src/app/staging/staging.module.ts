import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StagingRoutingModule } from '@app/staging/staging-routing.module';

@NgModule({
  imports: [CommonModule, StagingRoutingModule],
})
export class StagingModule {}
