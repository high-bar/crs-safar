import * as mongoose from 'mongoose';
import { DriverReviewSchema } from '../../common/models/driver-review';
import { Request, Response } from 'express';

const DriverReview = mongoose.model('DriverReview', DriverReviewSchema);

export class DriverReviewController {
  // Get My DriverReview
  public async GetMyDriverReview(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    DriverReview.find({ Driver: userid }, function(err, DriverReview) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(DriverReview);
    });
  }
  // Get DriverReview
  public async GetDriverReview(req: Request, res: Response): Promise<void> {
    let userid = req.body.userid;
    DriverReview.find({ Driver: userid }, function(err, DriverReview) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(DriverReview);
    });
  }
  //
  // add DriverReview
  public addDriverReview(req: Request, res: Response) {
    //set values
    let newDriverReview = new DriverReview({
      scoreOfFive: req.body.scoreOfFive,
      comment: req.body.comment,
      dateTime: req.body.dateTime,
      User: req.body.userid,
    });
    //save DriverReview
    newDriverReview.save((err, DriverReview) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }else{
      res.json({
        message: 'DriverReview Added!',
        DriverReview: DriverReview,
      });
    } //end else
    }); //end save DriverReview
  }

  ///

  // update DriverReview

  public UpdateDriverReview(req: Request, res: Response) {
    //set values
    let scoreOfFive = req.body.scoreOfFive;
    let comment = req.body.comment;
    let dateTime = req.body.dateTime;
    let User = req.body.userid;
    //upadte DriverReview
    DriverReview.findOneAndUpdate(
      { Driver: User },
      {
        $set: {
          scoreOfFive: scoreOfFive,
          comment: comment,
          dateTime: dateTime,
        },
      },
      function(err, DriverReview) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json({
            message: 'DriverReview Updated!',
            DriverReview: DriverReview,
          });
        }
      }
    );
  } // end update DriverReview
  //
  // delete DriverReview
  public async DeleteDriverReview(req: Request, res: Response): Promise<void> {
    let reviewid = req.body.reviewid;
    DriverReview.findByIdAndRemove({ _id: reviewid }, function(
      err,
      DriverReview
    ) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json({
        message: 'DriverReview Deleted!',
        DriverReview: DriverReview,
      });
    });
  }
  //
}
