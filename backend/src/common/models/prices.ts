import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const PricesSchema = new Schema( {
  from: {
    type:String
  },
  gfrom: {
    type:String
  },
  to: {
    type:String
  },
  gto: {
    type:String
  },
  price: {
    type:Number
  },
});
