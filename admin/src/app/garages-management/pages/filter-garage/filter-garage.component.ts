import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-filter-garage',
  templateUrl: './filter-garage.component.html',
  styleUrls: ['./filter-garage.component.scss'],
})
export class FilterGarageComponent implements OnInit {
  public filterGarageForm: FormGroup = this.formBuilder.group({
    name: [undefined, [Validators]],
    phone: [undefined, [Validators]],
    address: [undefined, [Validators]],
    notes: [undefined, [Validators]],
  });
  public isLoading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController
  ) {}

  ngOnInit() {}
  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async filterGarage(): Promise<void> {
    // if (this.addGarageForm.valid) {
    //   this.isLoading = true;
    //   try {
    //     await this.garageService.addNewGarage(this.addGarageForm.value);
    //     await this.close();
    //   } catch (error) {
    //     console.log(error);
    //   } finally {
    //     this.isLoading = false;
    //   }
    // }
  }
}
