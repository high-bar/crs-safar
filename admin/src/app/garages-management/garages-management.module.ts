import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { GaragesManagementPage } from './garages-management.page';
import { AddGaragePage } from './pages/add-garage/add-garage.page';
import { FilterGarageComponent } from './pages/filter-garage/filter-garage.component';

const routes: Routes = [
  {
    path: '',
    component: GaragesManagementPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AddGaragePage, FilterGarageComponent],
  entryComponents: [AddGaragePage, FilterGarageComponent],
})
export class GaragesManagementPageModule {}
