import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicesPage } from './polices.page';

describe('PolicesPage', () => {
  let component: PolicesPage;
  let fixture: ComponentFixture<PolicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
