import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { UtilService } from '@app/shared/services/util/util.service';
import { Driver } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  public editProfileForm: FormGroup = this.formBuilder.group({
    fullName: [undefined, [Validators.required]],
    phone: [
      undefined,
      [Validators.required, Validators.pattern('(01)[0-9]{9}')],
    ],
    state: [undefined, [Validators.required]],
    pictureURL: [undefined],
    address: [undefined, [Validators.required]],
  });
  public isLoading: boolean;
  @Input() public profileData: Driver;
  public states: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private profileService: ProfileService,
    private utilService: UtilService,
    private authService: AuthService
  ) {
    this.loadCountryStates();
  }

  ngOnInit() {
    if (!!this.profileData) {
      // pass only values used in the form
      this.editProfileForm.setValue({
        fullName: this.profileData.fullName,
        phone: this.profileData.phone,
        state: this.profileData.state.trim(),
        pictureURL: this.profileData.pictureURL || '',
        address: this.profileData.address || '',
      });
    }
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      const pictureURL = await this.profileService.initPhotoPicking();
      if (!!pictureURL) {
        this.editProfileForm.patchValue({
          pictureURL: pictureURL,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async submitEditProfile(): Promise<void> {
    if (this.editProfileForm.valid) {
      this.isLoading = true;
      if (!!this.profileData) {
        this.profileData.fullName = this.editProfileForm.value.fullName;
        this.profileData.phone = this.editProfileForm.value.phone;
        this.profileData.state = this.editProfileForm.value.state;
        this.profileData.pictureURL = this.editProfileForm.value.pictureURL;
        this.profileData.address = this.editProfileForm.value.address;
        try {
          await this.authService.editProfile(this.profileData);
          await this.close();
        } catch (error) {
          console.log(error);
        } finally {
          this.isLoading = false;
        }
      }
    }
  }
}
