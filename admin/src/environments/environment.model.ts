import { Configs } from './configs.model';

export interface Environment {
  production: boolean;
  ROOT_API_URL: string;
  configs: Configs;
}
