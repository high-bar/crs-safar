import * as mongoose from 'mongoose';
import * as GenderEnums from '../../../../common/enums/gender.enum';

const Schema = mongoose.Schema;

export const DriverSchema = new Schema({
  fullName: {
    type: String,
  },
  firstName: {
    type: String,
  },
  secondName: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  phone: {
    type: String,
  },
  gender: {
    type: GenderEnums.Gender,
  },
  pictureURL: {
    type: String,
  },
  country: {
    type: String,
  },
  state: {
    type: String,
  },
  rate: {
    type: Number,
  },
  age: {
    type: Number,
  },
  balance: {
    type: Number,
  },
  salary: {
    type: Number,
  },
  trips: {
    type: String,
  },
  joiningDate: {
    type: String,
  },
  isApproved: {
    type: String,
  },
});
