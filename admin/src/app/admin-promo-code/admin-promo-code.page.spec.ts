import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPromoCodePage } from './admin-promo-code.page';

describe('AdminPromoCodePage', () => {
  let component: AdminPromoCodePage;
  let fixture: ComponentFixture<AdminPromoCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPromoCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPromoCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
