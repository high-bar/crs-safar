import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Notification, NotificationPayLoad } from '@common/models';
import { functions } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private http: HttpClient) {}

  public addNotification(
    newNotification: Notification,
    target: string
  ): Observable<any> {
    return this.http.post<any>(
      'https://fcm.googleapis.com/fcm/send',
      {
        data: {
          title: newNotification.title,
          message: newNotification.body,
          notId: Math.round(Math.random() * 9999),
          icon: 'fcm_push_icon',
        },
        to: `/topics/${target}`,
        priority: 'high',
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'key=AIzaSyBrNgBydr_TMHg-9G5b7DkxkxmOW_daV14',
        },
      }
    );
  }

  public async sendMassNotifications(
    targetsUids: string[],
    payload: NotificationPayLoad
  ): Promise<any> {
    const sendMassNotificationsFunction = functions().httpsCallable(
      'sendMassNotifications'
    );
    return (await sendMassNotificationsFunction({
      targetsUids: targetsUids,
      payload: payload,
    })).data;
  }
}
