import { User } from '@common/models';

export interface Client extends User {
  country: string;
  activePromo: any;
}
