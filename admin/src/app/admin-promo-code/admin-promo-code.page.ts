import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '@app/shared/services/util/util.service';
import { NotificationService } from '@app/staging/send-notifications/services/notification.service';
import { Notification } from '@common/models';

@Component({
  selector: 'app-admin-promo-code',
  templateUrl: './admin-promo-code.page.html',
  styleUrls: ['./admin-promo-code.page.scss'],
})
export class AdminPromoCodePage implements OnInit {
  public states: string[] = [];
  public isLoading: boolean;

  public promoCodeForm: FormGroup = this.formBuilder.group({
    country: ['egypt', [Validators.required]],
    trips: [undefined, [Validators.required]],
    money: [undefined],
    percentage: [undefined],
    message: [undefined, [Validators.required]],
    sendMessage: [undefined],
    sendNotification: [undefined],
  });

  constructor(
    private utilService: UtilService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService
  ) {
    this.loadCountryStates();
  }

  ngOnInit() {}

  private async loadCountryStates(): Promise<void> {
    this.isLoading = true;
    try {
      this.states = (await this.utilService.getCountryData()).data()
        .states as string[];
      this.states.sort((stateA, stateB) => {
        return stateA.localeCompare(stateB);
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async submitPromoCode() {
    this.isLoading = true;
    if (
      this.promoCodeForm.valid &&
      (!!this.promoCodeForm.value.percentage ||
        !!this.promoCodeForm.value.money)
    ) {
      const notification: Notification = {
        title: 'New promo',
        body: this.promoCodeForm.value.message,
        message: '',
      };
      this.notificationService
        .addNotification(notification, 'll9rwM1XrOc5ZOwcTg3tZOrBr4z1')
        .subscribe(() => {
          this.isLoading = false;
        });
    }
  }
}
