import { Component, OnInit } from '@angular/core';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { Trip } from '@common/models/trip.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-trips-history',
  templateUrl: './trips-history.page.html',
  styleUrls: ['./trips-history.page.scss'],
})
export class TripsHistoryPage implements OnInit {
  public isPastLoading: boolean;
  public pastTrips: Partial<Trip>[] = [];

  constructor(private tripsService: TripsService) {
    this.loadPastTrips();
  }

  ngOnInit() {}


  public async loadPastTrips(): Promise<void> {
    this.isPastLoading = true;
    this.tripsService.getPastTrips().subscribe(
      data => {
        this.pastTrips = data.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          return trip;
        });
        this.isPastLoading = false;
      },
      error => {
        console.error(error);
        this.isPastLoading = false;
      }
    );
  }
}
