import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { ClientsManagementPage } from './clients-management.page';
import { FilterClientPage } from './pages/filter-client/filter-client/filter-client.page';

const routes: Routes = [
  {
    path: '',
    component: ClientsManagementPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ClientsManagementPage, FilterClientPage],
  entryComponents: [ClientsManagementPage, FilterClientPage],
})
export class ClientsManagementPageModule {}
