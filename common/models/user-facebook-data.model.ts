export interface UserFacebookData {
  accessToken: string;
  displayName: string;
  email: string;
  expires_in: number;
  imageUrl: string;
  userId: string;
}
