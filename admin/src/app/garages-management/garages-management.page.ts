import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Garage, Trip } from '@common/models';
import {
  AlertController,
  ModalController,
  NavController,
  Platform,
} from '@ionic/angular';
import { Subscription } from 'rxjs';

import { AddGaragePage } from './pages/add-garage/add-garage.page';
import { FilterGarageComponent } from './pages/filter-garage/filter-garage.component';
import { GarageDetailsPage } from './pages/garage-details/garage-details.page';
import { GarageService } from './services/garage.service';

@Component({
  selector: 'app-garages-management',
  templateUrl: './garages-management.page.html',
  styleUrls: ['./garages-management.page.scss'],
})
export class GaragesManagementPage implements OnInit {
  private backButtonSubscription: Subscription;
  @Input() public select: string;
  @Input() public trip: Partial<Trip>;
  public garages: Partial<Garage>[] = [];
  public isLoading: boolean;

  constructor(
    private modalController: ModalController,
    private garageService: GarageService,
    private alertController: AlertController,
    private platform: Platform,
    private navController: NavController,
    private route: ActivatedRoute
  ) {
    this.loadGarages();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async selectGarage(garage: Garage): Promise<void> {
    if (!!!this.trip) {
      await this.navController.navigateForward(['/garage/' + garage.id], {
        relativeTo: this.route,
      });
    } else {
      const garageDetailsModal = await this.modalController.create({
        component: GarageDetailsPage,
        componentProps: {
          isModal: true,
          select: this.select,
          garage: garage,
          trip: this.trip,
        },
      });

      garageDetailsModal.onDidDismiss().then(async garageDetailsModalRes => {
        const selectedCarGarage = garageDetailsModalRes.data;
        if (!!selectedCarGarage) {
          setTimeout(async () => {
            await this.modalController.dismiss(selectedCarGarage);
          }, 250);
        }
      });

      await garageDetailsModal.present();
    }
  }

  public async openAddEditGarageModal(garage?: Garage): Promise<void> {
    const addGarageModal = await this.modalController.create({
      component: AddGaragePage,
      componentProps: {
        garage: garage,
      },
    });
    return await addGarageModal.present();
  }

  public async loadGarages(): Promise<void> {
    this.isLoading = true;
    this.garageService.getGarages().subscribe(
      data => {
        this.garages = data.docs.map(doc => {
          const garage = doc.data();
          garage.id = doc.id;
          return garage;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  public async openAddGarageModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddGaragePage,
    });
    return await modal.present();
  }
  public async openFilterGarageModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: FilterGarageComponent,
    });
    return await modal.present();
  }

  async deleteGarage(garage: Garage) {
    const alert = await this.alertController.create({
      header: 'تأكيد',
      message: `حذف الجراج: <strong>${garage.name}</strong>`,
      buttons: [
        {
          text: 'إلغاء',
          role: 'cancel',
          cssClass: 'color-dark',
        },
        {
          text: 'حذف',
          role: '',
          cssClass: 'color-danger',
          handler: async () => {
            await this.garageService.deleteGarage(garage.id);
          },
        },
      ],
    });

    await alert.present();
  }
}
