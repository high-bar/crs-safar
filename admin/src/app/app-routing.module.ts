import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from '@app/shared/services/auth/auth.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'drivers-management',
    loadChildren:
      () => import('./drivers-management/drivers-management.module').then(m => m.DriversManagementPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'driver/:id',
    loadChildren:
      () => import('./driver-details/driver-details.module').then(m => m.DriverDetailsPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'clients-management',
    loadChildren:
      () => import('./clients-management/clients-management.module').then(m => m.ClientsManagementPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'trips-history',
    loadChildren: () => import('./trips-history/trips-history.module').then(m => m.TripsHistoryPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'trip/:tripId',
    loadChildren: () => import('./shared/pages/trip/trip.module').then(m => m.TripPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'staging',
    loadChildren: () => import('./staging/staging.module').then(m => m.StagingModule),
    canActivate: [AuthService],
  },
  {
    path: 'trips-history',
    loadChildren: () => import('./trips-history/trips-history.module').then(m => m.TripsHistoryPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'driver-profile',
    loadChildren:
      () => import('./shared/pages/driver-profile/driver-profile.module').then(m => m.DriverProfilePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'promo-codes',
    loadChildren:
      () => import('./admin-promo-code/admin-promo-code.module').then(m => m.AdminPromoCodePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'garages-management',
    loadChildren:
      () => import('./garages-management/garages-management.module').then(m => m.GaragesManagementPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'client/:id',
    loadChildren:
      () => import('./client-profile/client-profile.module').then(m => m.ClientProfilePageModule),
    canActivate: [AuthService],
  },
  {
    path: 'car-management',
    loadChildren:
      () => import('./car-management/car-management.module').then(m => m.CarManagementPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'cars-tracking',
    loadChildren: () => import('./cars-tracking/cars-tracking.module').then(m => m.CarsTrackingPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'complains-management',
    loadChildren:
      () => import('./complains-management/complains-management.module').then(m => m.ComplainsManagementPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'garage/:garageId',
    loadChildren:
      () => import('./garages-management/pages/garage-details/garage-details.module').then(m => m.GarageDetailsPageModule),
    canActivate: [AuthService],
  },
  {
    path: 'statistics',
    loadChildren: () => import('./statistics/statistics.module').then(m => m.StatisticsPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
