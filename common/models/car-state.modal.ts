export interface CarStats {
  Luxury: number;
  SUV: number;
  Sedan: number;
  total: number;
}
