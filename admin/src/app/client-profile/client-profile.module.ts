import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { ClientProfilePage } from './client-profile.page';

const routes: Routes = [
  {
    path: '',
    component: ClientProfilePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ClientProfilePage],
})
export class ClientProfilePageModule {}
