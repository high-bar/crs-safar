import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PolicyAggrementsPage } from './policy-aggrements.page';

const routes: Routes = [
  {
    path: '',
    component: PolicyAggrementsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PolicyAggrementsPage]
})
export class PolicyAggrementsPageModule {}
