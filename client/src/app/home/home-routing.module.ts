import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from '@app/shared/services/auth/auth.service';

import { HomePage } from './home.page';
import { BookingPage } from './pages/booking/booking.page';
import { LoginPage } from './pages/login/login.page';
import { OverviewPage } from './pages/overview/overview.page';
import { SignUpPage } from './pages/sign-up/sign-up.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'sign-up',
    component: SignUpPage,
  },
  {
    path: 'booking',
    component: BookingPage,
  },
  {
    path: 'overview',
    component: OverviewPage,
    canActivate: [AuthService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
