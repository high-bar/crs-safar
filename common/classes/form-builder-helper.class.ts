import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Place } from '@common/models';

export class FormBuilderHelper {
  public static matchValues(
    matchTo: string
  ): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: false };
    };
  }

  public static placeStateLimiter(
    state: string
  ): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.value &&
        ((control.value as Place).state || '').toLowerCase() ===
          state.toLowerCase()
        ? null
        : { isStateValid: false };
    };
  }

  public static minDateValue(
    minValue: number
  ): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.value &&
        minValue - new Date(control.value).getTime() <= 0
        ? null
        : { isDateValid: false };
    };
  }

  public static minTimeValue(
    minValue: number,
    dateControl: string
  ): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        minValue -
          new Date(
            `${control.parent.controls[dateControl].value} ${control.value}`
          ).getTime() <=
          0
        ? null
        : { isMatching: false };
    };
  }
}
