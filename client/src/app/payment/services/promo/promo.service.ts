import { Injectable } from '@angular/core';
import { auth, firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class PromoService {
  constructor() {}

  public getPromoById(promoId: string): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('promos')
      .doc(promoId)
      .get();
  }

  public activatePromo(newPromo: any) {
    return firestore()
      .collection('users')
      .doc(auth().currentUser.uid)
      .update({
        activePromo: newPromo,
      });
  }
}
