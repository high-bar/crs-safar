import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsSalaryPage } from './trips-salary.page';

describe('TripsSalaryPage', () => {
  let component: TripsSalaryPage;
  let fixture: ComponentFixture<TripsSalaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripsSalaryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsSalaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
