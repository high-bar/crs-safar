import * as mongoose from 'mongoose';
import { TripSchema } from '../../common/models/trip';
import { Request, Response } from 'express';
const https = require('https');


const Trip = mongoose.model('Trip', TripSchema);
export class DriverTrips {
  constructor() {}

  //driver get all trips
  public async DriverTripHistory(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    Trip.find({driver : userid}, function(err, trips){
      if(err){
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn


  //////////////
  ///driver get all active trips

  public async DriverpendingTrips(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let status = 'pending'
    Trip.find({driver : userid,status: status}, function(err, trips){
      if(err){
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////

  ///driver get all active trips

  public async DriverActiveTrips(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    let status = 'active'
    Trip.find({driver : userid,status: status}, function(err, trips){
      if(err){
        res.status(401).json({
          error: err,
        });
      }
      res.json(trips);
    });
  } //edn mytrips

  /////////////////
  // get trip details by it's id (driver)
    public async DriverTripDetails(req: Request, res: Response): Promise<void> {
      let userid = (<any>req).decoded.id;
      let tripid = req.body.tripid;
      Trip.find({driver : userid,_id: tripid}, function(err, trip){
        if(err){
          res.status(401).json({
            error: err,
          });
        }
        res.json(trip);
      });
    }

/////////////////
  // get trip history1
  public GetDriverTripHistory1(req: Request, res: Response) {
    let driverid = (<any>req).decoded.id;
    Trip.find({ driver: driverid }, function(err: any, Trip: any) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json(Trip);
      }
    })
      .limit(10)
      .sort({ _id: -1 }); // end find all
  }
  /////////////////////////////
  public GetDriverTripHistory2(req: Request, res: Response) {
    let driverid = (<any>req).decoded.id;
    let limit = parseInt(req.body.limit) || 10;
    let skip = parseInt(req.body.index) || 10;
    // find
    Trip.find({ driver: driverid })
      .skip(skip)
      .limit(limit)
      .sort({ _id: -1 })
      .exec(function(err: any, Trip: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(Trip);
        }
      }); // end find all
  }
  /////////////////
  public DriverUpdateTrip(req: Request, res: Response) {
    let tripid = req.body.tripid;
    let driverStatus = req.body.driverstatus;
    let driverLocation = req.body.driverlocation;
    //Update
    Trip.findOneAndUpdate(
      { _id: tripid },
      {
        $set: {
          driverStatus: driverStatus,
          driverLocation: driverLocation,
        },
      },
      function(err, Trip) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json({
            message: 'Trip Updated!',
            Trip: Trip,
          });
        }
      }
    );
  } //end
  //////////////////////////////// get all trips /////////

} //end trip class
