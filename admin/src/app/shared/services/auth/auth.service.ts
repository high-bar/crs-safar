import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Client, Transaction } from '@common/models';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NavController } from '@ionic/angular';
import { auth, firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  private authChangeObserver: Observer<any>;
  public change: Observable<any>;

  constructor(
    private router: Router,
    private splashScreen: SplashScreen,
    private navController: NavController
  ) {
    let user = this.getSavedUser();
    if (!!user) {
      user = JSON.parse(user);
    }

    this.change = Observable.create((observer: Observer<any>) => {
      this.authChangeObserver = observer;
      this.authChangeObserver.next(user);
    }).pipe(shareReplay());
    this.change.subscribe();

    auth().onAuthStateChanged(authUser => {
      user = undefined;
      if (!!authUser) {
        user = JSON.parse(this.getSavedUser());
      }
      this.authChangeObserver.next(user);
    });
  }

  private saveUser(user): void {
    if (!!user) {
      this.authChangeObserver.next(user);
      localStorage.setItem('user', JSON.stringify(user));
    }
  }

  public getSavedUser(): string {
    return localStorage.getItem('user');
  }

  private getUser(uid: string): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('users')
      .doc(uid)
      .get();
  }

  public canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!!!this.getSavedUser()) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
    return true;
  }

  public login(loginReq: Partial<Client>): Promise<any> {
    return auth()
      .signInWithEmailAndPassword(loginReq.email, loginReq.password)
      .then(async loginData => {
        try {
          this.saveUser((await this.getUser(loginData.user.uid)).data());
        } catch (error) {
          console.error(error);
          throw error;
        }
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public async logout(): Promise<void> {
    return auth()
      .signOut()
      .then(async () => {
        this.splashScreen.show();
        localStorage.clear();
        await this.navController.navigateRoot('home');
        this.splashScreen.hide();
      })
      .catch(error => {
        // An error happened.
      });
  }

  public resetPassword(email: string): Promise<void> {
    return auth().sendPasswordResetEmail(email);
  }

  public addTransaction(
    uid,
    transaction: Partial<Transaction>
  ): Promise<firestore.DocumentReference> {
    return firestore()
      .collection('users')
      .doc(uid)
      .collection('transactions')
      .add(transaction);
  }

  public getTransactions(uid): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('users')
        .doc(uid)
        .collection('transactions')
        .orderBy('dateTime', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }
}
