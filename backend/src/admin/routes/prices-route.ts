import { PricesController } from '../controller/price';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class PricesRoutes {
  public PricesController: PricesController = new PricesController();

  public routes(app: any): void {
    // start admin prices
    // getPrices
    app.route('/admin/getPrices').get(this.PricesController.getPrices);

    // Create setPrices
    app
      .route('/admin/setPrices')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.PricesController.updatePrices.bind(this.PricesController));
    ////
    // Create a new Prices
    app
      .route('/admin/NewPrices')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.PricesController.newPrices.bind(this.PricesController));
    //

    // Create a new Prices
    app
      .route('/admin/delPrices')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.PricesController.deletePrices.bind(this.PricesController));
    //

    // Create a new Prices
    app
      .route('/replacefrom')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.PricesController.replace1.bind(this.PricesController));
    //

    // Create a new Prices
    app
      .route('/replaceto')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.PricesController.replace2.bind(this.PricesController));
    //

    //////////////
  }
  //end class
}
