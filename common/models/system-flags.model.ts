export interface SystemFlags {
  isCashEnabled: boolean;
  isFwariEnabled: boolean;
  isBookingEnabled: boolean;
  maxAllowedSpeed: number;
  minTimeBeforeOrder: number;
  waitingHourPrice: number;
  kmPrice: number;
  minTripDistance: number;
  fastOrderNumber: string;
  dayRentalPrice: number;
  dayRentalKm: number;
  kmPriceSUV: number;
}
