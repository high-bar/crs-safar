import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time',
})
export class TimePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    value = value / 60 / 60;
    const hour = Math.trunc(value);
    const minute = Math.round((value - hour) * 60);
    return hour + ':' + minute;
  }
}
