import { Component, OnInit } from '@angular/core';
import { CarStats, UserStats } from '@common/models';
import { ModalController } from '@ionic/angular';

import { StatisticsService } from './services/statistics.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.page.html',
  styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {
  public isCarNumberLoading: boolean;
  public userStats: Partial<UserStats>;
  public carStats: Partial<CarStats>;

  public chartOptions = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 20,
        fontSize: 16,
      },
    },
  };

  public carsChartData = [
    {
      data: [0, 0, 0],
      backgroundColor: ['#b50003', '#00BCD4', '#2196F3'],
    },
  ];
  public usersChartData = [
    {
      data: [0, 0, 0],
      backgroundColor: ['#b50003', '#009688', '#2196F3'],
    },
  ];
  public carsChartLabels = ['Luxury', 'SUV', 'Sedan'];
  public usersChartLabels = ['Admin', 'Client', 'Drivers'];
  usersCount: number;
  isUsersStatsLoading: boolean;
  isCarsStatsLoading: boolean;

  constructor(
    private modalController: ModalController,
    private statisticsService: StatisticsService
  ) {
    this.loadCarStats();
    this.loadUserStats();
  }

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  // public loadCarStats() {
  //   this.isCarStatsLoading = true;
  //   this.statisticsService.getCarStats().subscribe(
  //     doc => {
  //       this.carStats = doc.data() as CarStats;
  //       this.isCarStatsLoading = false;
  //       return carStats;
  //     },
  //     error => {
  //       console.error(error);
  //       this.isCarStatsLoading = false;
  //     }
  //   );
  // }

  public async loadUserStats(): Promise<void> {
    this.isUsersStatsLoading = true;
    try {
      this.userStats = (await this.statisticsService.getUserStats()).data();
      this.usersChartData[0].data = [
        this.userStats.admin,
        this.userStats.client,
        this.userStats.driver,
      ];
      this.usersChartLabels = [
        `أدمن ${this.userStats.admin}`,
        `عميل ${this.userStats.client}`,
        `سواق ${this.userStats.driver}`,
      ];
    } catch (error) {
      console.error(error);
    } finally {
      this.isUsersStatsLoading = false;
    }
  }

  public async loadCarStats(): Promise<void> {
    this.isCarsStatsLoading = true;
    try {
      this.carStats = (await this.statisticsService.getCarStats()).data();
      this.carsChartData[0].data = [
        this.carStats.Luxury,
        this.carStats.SUV,
        this.carStats.Sedan,
      ];
      this.carsChartLabels = [
        `فخمة ${this.carStats.Luxury}`,
        `SUV ${this.carStats.SUV}`,
        `سيدان ${this.carStats.Sedan}`,
      ];
    } catch (error) {
      console.error(error);
    } finally {
      this.isCarsStatsLoading = false;
    }
  }
}
