import { Component, OnInit, ViewChild } from '@angular/core';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { Trip } from '@common/models';
import { Toast } from '@ionic-native/toast/ngx';
import { IonSlides, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {
  private backButtonSubscription: Subscription;
  private lastTimeBackPress = 0;
  private exitBackTimeFrame = 2000;

  @ViewChild('slider', { static: true }) public slider: IonSlides;
  public currentTab = 0;

  public activeTrip: Partial<Trip>;
  public futureTrips: Partial<Trip>[] = [];

  public isActiveLoading;
  public isFutureLoading: boolean;

  constructor(
    private platform: Platform,
    private toast: Toast,
    private tripsService: TripsService
  ) {
    this.loadActiveTrip();
    this.loadFutureTrips();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        if (
          new Date().getTime() - this.lastTimeBackPress <
          this.exitBackTimeFrame
        ) {
          navigator['app'].exitApp();
        } else {
          this.toast
            .show(`Press back again to exit`, '2000', 'bottom')
            .subscribe();

          this.lastTimeBackPress = new Date().getTime();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  public loadFutureTrips(): void {
    this.isFutureLoading = true;

    this.tripsService.getFutureTrips().subscribe(
      data => {
        this.futureTrips = data.docs.map(doc => {
          return doc.data();
        });
        this.isFutureLoading = false;
      },
      error => {
        console.error(error);
        this.isFutureLoading = false;
      }
    );
  }

  public loadActiveTrip(): void {
    this.isActiveLoading = true;
    this.tripsService.getActiveTrip().subscribe(
      doc => {
        if (!!doc.docs[0]) {
          this.activeTrip = doc.docs[0].data();
          this.activeTrip.id = doc.docs[0].id;
        }
        this.isActiveLoading = false;
      },
      error => {
        console.error(error);
        this.isActiveLoading = false;
      }
    );
  }
}
