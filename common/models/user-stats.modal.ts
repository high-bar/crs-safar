export interface UserStats {
  admin: number;
  client: number;
  driver: number;
  total: number;
}
