import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverStatus } from '@common/enums';
import { DriverFilter } from '@common/models';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-filter-driver',
  templateUrl: './filter-driver.page.html',
  styleUrls: ['./filter-driver.page.scss'],
})
export class FilterDriverPage implements OnInit {
  public isLoading: boolean;
  @Input() public value: DriverFilter;
  public readonly driverStatusEnum: typeof DriverStatus = DriverStatus;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController
  ) {}

  public filterDriverForm: FormGroup = this.formBuilder.group({
    fullName: [undefined],
    email: [undefined],
    phone: [undefined],
    rate: [undefined],
    status: [undefined],
  });

  ngOnInit() {
    if (!!this.value) {
      // pass only values used in the form
      this.filterDriverForm.patchValue({
        fullName: this.value.fullName,
        email: this.value.email,
        phone: this.value.phone,
        rate: this.value.rate,
        status: this.value.status,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitFilterDriver(): Promise<void> {
    const driverFilter: DriverFilter = {
      fullName: this.filterDriverForm.value.fullName,
      email: this.filterDriverForm.value.email,
      phone: this.filterDriverForm.value.phone,
      rate: this.filterDriverForm.value.rate,
      status: this.filterDriverForm.value.status,
    };
    await this.modalController.dismiss(driverFilter);
  }
}
