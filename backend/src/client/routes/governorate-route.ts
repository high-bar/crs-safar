import { GovernorateController } from "../controllers/governorate";

export class GovernorateRoutes {
  public GovernorateController: GovernorateController = new GovernorateController();

  public routes(app: any): void {
    // start governorate route
    // get countries
    app.route('/getgovernorate').get(this.GovernorateController.getGovernorates);
    ////
    // governorate set
    app.route('/setgovernorate').post(this.GovernorateController.SetGovernorate);
    ////
    // end governorate route
  }
  //end class
}
