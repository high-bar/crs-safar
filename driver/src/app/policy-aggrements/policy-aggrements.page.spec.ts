import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyAggrementsPage } from './policy-aggrements.page';

describe('PolicyAggrementsPage', () => {
  let component: PolicyAggrementsPage;
  let fixture: ComponentFixture<PolicyAggrementsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyAggrementsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyAggrementsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
