import * as mongoose from 'mongoose';
import { DriverSchema } from '../../common/models/driver';
import { Request, Response } from 'express';
import { config } from '../../const/config';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

let formidable = require('formidable');
import * as fs from 'fs';
import * as path from 'path';

const Driver = mongoose.model('driver', DriverSchema);

export class DriverSignInController {
  public getDriver(req: Request, res: Response) {
    //
    function createToken(Driver: any) {
      let token = jwt.sign(
        {
          id: Driver._id,
          fullName: Driver.fullName,
          email: Driver.email,
          phone: Driver.phone,
          gender: Driver.gender,
          pictureURL: Driver.pictureURL,
          country: Driver.country,
          state: Driver.state,
          rate: Driver.rate,
        },
        secretKey
      );
      return token;
    }
    //
    // find Driver
    Driver.findOne({ email: req.body.email }, function(err: any, Driver: any) {
      if (err) throw err;

      if (!Driver) {
        res.status(401).json({
          error: 'invalid Driver',
        });
      } else if (Driver) {
        let password = req.body.password;
        bcrypt.compare(password, Driver.password).then(function(cresult: any) {
          if (cresult == true) {
            //create token
            let token = createToken(Driver);
            res.json({
              Driver,
              token: token,
            });
          } else {
            res.status(401).json({
              error: 'invalid Admin',
            });
          }
          //
        }); //end bcrypt compare
      } //end else if Driver found
    }); // end find one
  } // end get driver

  /////
  public async DriverProfile(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    Driver.find({ _id: userid }, function(err, Driver) {
      if (err) {
        res.status(401).json({
          error: err,
        });
      }
      res.json(Driver);
    });
  } //end
  /////

  /////UploadProgile Image///////
  public UploadProfile(req: Request, res: Response) {
    let userid = (<any>req).decoded.id;
    let form = new formidable.IncomingForm();
    form.parse(req, function(err: any, fields: any, files: any) {
      let oldpath = files.filetoupload.path;
      let newpath = path.join(__dirname, '/../../public/driver/') + files.filetoupload.name;
      let pictureURL: any;
      fs.rename(oldpath, newpath, function(err) {
        pictureURL = newpath;
        let url = pictureURL.split('\\');
        let imgurl = 'https://safarprivatetaxi.com/driver/' + url[6];
        if (err) throw err;
        Driver.findOneAndUpdate(
          { _id: userid },
          {
            $set: {
              pictureURL: imgurl,
            },
          },
          function(err, Driver) {
            if (err) {
              res.status(401).json({
                error: err,
              });
            } else {
              res.json({
                message: 'User Profile PIC Update!',
                Driver: Driver,
                imgurl: imgurl
              });
            }
          }
        ); // end update
      });
    });
  }

  //
}
