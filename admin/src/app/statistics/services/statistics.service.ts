import { Injectable } from '@angular/core';
import { firestore, Observer } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StatisticsService {
  constructor() {}

  // public getCarStats(): Observable<firestore.DocumentSnapshot> {
  //   return Observable.create(
  //     (observer: Observer<firestore.DocumentSnapshot>) => {
  //       firestore()
  //         .collection('cars')
  //         .doc('--stats--')
  //         .onSnapshot(
  //           documentSnapshot => {
  //             observer.next(documentSnapshot);
  //           },
  //           error => {
  //             observer.error(error);
  //           },
  //           () => {
  //             observer.complete();
  //           }
  //         );
  //     }
  //   );
  // }

  public getUserStats(): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('users')
      .doc('--stats--')
      .get();
  }

  public getCarStats(): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('cars')
      .doc('--stats--')
      .get();
  }
}
