import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterGarageComponent } from './FilterGarageComponent';

describe('FilterGarageComponent', () => {
  let component: FilterGarageComponent;
  let fixture: ComponentFixture<FilterGarageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilterGarageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterGarageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
