import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '@common/models';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss'],
})
export class ProfileHeaderComponent {
  public readonly avatarPlaceHolder =
    '/assets/images/profile-picture-placeholder.png';
  @Input() public user: User;
  @Input() public size: string;
  @Input() public isConnectable: boolean;

  constructor(public sanitizer: DomSanitizer) {}

  public avatarLoadError(): void {
    this.user.pictureURL = this.avatarPlaceHolder;
  }
}
