import { Component, OnInit, ViewChild } from '@angular/core';
import { GarageService } from '@app/garages-management/services/garage.service';
import { Driver, Garage } from '@common/models';
import { ActionSheetController } from '@ionic/angular';
import { ActionSheetButton } from '@ionic/core';

import { DriverService } from '../../services/driver-service/driver.service';

@Component({
  selector: 'app-new-drivers',
  templateUrl: './new-drivers.component.html',
  styleUrls: ['./new-drivers.component.scss'],
})
export class NewDriversComponent implements OnInit {
  public isApproveLoading;
  public isNewDriverLoading;
  public newDrivers: Driver[] = [];

  public garages: Partial<Garage>[] = [];

  constructor(
    private driverService: DriverService,
    private actionSheetController: ActionSheetController,
    private garageService: GarageService
  ) {
    this.loadGarages();
    this.loadNewDrivers();
  }

  ngOnInit() {}

  private async loadNewDrivers(): Promise<void> {
    this.isNewDriverLoading = true;
    this.driverService.getNewDrivers().subscribe(
      data => {
        this.newDrivers = data.docs.map(doc => {
          const driver = doc.data() as Driver;
          driver.uid = doc.id;
          return driver;
        });
        this.isNewDriverLoading = false;
      },
      error => {
        console.error(error);
        this.isNewDriverLoading = false;
      }
    );
  }

  private async loadGarages(): Promise<void> {
    this.garageService.getGarages().subscribe(
      data => {
        this.garages = data.docs.map(doc => {
          const garage = doc.data();
          garage.id = doc.id;
          return garage;
        });
      },
      error => {
        console.error(error);
      }
    );
  }

  public async assignDriverToGarage(newDriver: Driver): Promise<void> {
    const assignToGarageActionSheet = await this.actionSheetController.create({
      header: 'أختار جراج',
      buttons: [
        ...this.garages.map(garage => {
          return {
            text: garage.name,
            handler: () => {
              newDriver.garage = {
                name: garage.name,
                id: garage.id,
              };
              this.approveAndUpdateDriver(newDriver);
            },
          };
        }),
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
        },
      ],
    });

    await assignToGarageActionSheet.present();
  }

  public async approveAndUpdateDriver(newDriver: Driver): Promise<void> {
    this.isApproveLoading = true;
    try {
      await this.driverService.approveDriver(newDriver);
    } catch (error) {
      console.error(error);
    } finally {
      this.isApproveLoading = false;
    }
  }
}
