import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TripsService } from '@app/home/services/trips.service';
import { Trip } from '@common/models';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.page.html',
  styleUrls: ['./trip.page.scss'],
})
export class TripPage implements OnInit {
  public trip: Partial<Trip>;
  public isLoading;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tripsService: TripsService
  ) {
    this.loadTripData(this.activatedRoute.snapshot.params.tripId);
  }

  ngOnInit() {}

  // TODO: change to real time later
  private async loadTripData(tripId: string): Promise<void> {
    this.isLoading = true;
    try {
      const doc = await this.tripsService.getTripByID(tripId);
      this.trip = doc.data();
      this.trip.id = doc.id;
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  public getStateParsedURL(stateName: string = ''): string {
    return `url(/assets/states/${stateName
      .replace(/\s/g, '_')
      .toLowerCase()}.jpeg)`;
  }
}
