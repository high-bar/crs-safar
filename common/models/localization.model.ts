import { Language, WritingDir } from '@common/enums';

export interface Localization {
  dir?: WritingDir;
  lang: Language;
}
