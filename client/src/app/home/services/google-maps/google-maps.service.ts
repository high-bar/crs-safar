import { Injectable } from '@angular/core';
import { DistanceMatrix, Place } from '@common/models';
import { ILatLng } from '@ionic-native/google-maps/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import * as _ from 'lodash';
import { Observable, Observer } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GoogleMapsService {
  private readonly placeQueryAutoCompleteAPI =
    'https://maps.googleapis.com/maps/api/place/queryautocomplete/json';
  private readonly placeDetailsAPI =
    'https://maps.googleapis.com/maps/api/place/details/json';

  private readonly locationGeoCodingAPI =
    'https://maps.googleapis.com/maps/api/geocode/json';

  private readonly distanceMatrixAPI =
    'https://maps.googleapis.com/maps/api/distancematrix/json';

  constructor(private http: HTTP) {}

  public searchPlaces(query: string): Observable<Partial<Place>[]> {
    return Observable.create((observer: Observer<Partial<Place>[]>) => {
      this.http
        .get(
          this.placeQueryAutoCompleteAPI,
          {
            key: environment.googleAPIKey,
            input: query,
            location: '26.749795,29.988752',
            radius: '550'
          },
          {}
        )
        .then(response => {
          const data = JSON.parse(response.data);
          if (data.status !== 'OK' && data.status !== 'ZERO_RESULTS') {
            throw data;
          } else {
            observer.next(data.predictions);
            observer.complete();
          }
        })
        .catch(error => {
          console.error(error);
          observer.error(error);
          observer.complete();
        });
    });
  }

  public getPlace(placeId: string): Observable<google.maps.places.PlaceResult> {
    return Observable.create(
      (observer: Observer<google.maps.places.PlaceResult>) => {
        this.http
          .get(
            this.placeDetailsAPI,
            {
              key: environment.googleAPIKey,
              placeid: placeId,
            },
            {}
          )
          .then(response => {
            const data = JSON.parse(response.data);
            if (data.status !== 'OK') {
              throw data;
            } else {
              observer.next(data.result);
              observer.complete();
            }
          })
          .catch(error => {
            console.error(error);
            observer.error(error);
            observer.complete();
          });
      }
    );
  }

  public getPlaceByLocation(
    latLng: ILatLng
  ): Observable<google.maps.places.PlaceResult> {
    return Observable.create(
      (observer: Observer<google.maps.places.PlaceResult>) => {
        this.http
          .get(
            this.locationGeoCodingAPI,
            {
              key: environment.googleAPIKey,
              latlng: `${latLng.lat},${latLng.lng}`,
            },
            {}
          )
          .then(response => {
            const data = JSON.parse(response.data);
            if (data.status !== 'OK') {
              throw data;
            } else {
              observer.next(data.results[0]);
              observer.complete();
            }
          })
          .catch(error => {
            console.error(error);
            observer.error(error);
            observer.complete();
          });
      }
    );
  }

  public getDistanceBetweenPlaces(
    place1: Partial<Place>,
    place2: Partial<Place>
  ): Observable<DistanceMatrix> {
    return Observable.create(
      (observer: Observer<DistanceMatrix>) => {
        this.http
          .get(
            this.distanceMatrixAPI,
            {
              key: environment.googleAPIKey,
              origins: `place_id:${place1.place_id}`,
              destinations: `place_id:${place2.place_id}`,
            },
            {}
          )
          .then(response => {
            const data = JSON.parse(response.data);
            if (data.status !== 'OK') {
              throw data;
            } else {
              observer.next(_.get(data, 'rows[0].elements[0]'));
              observer.complete();
            }
          })
          .catch(error => {
            console.error(error);
            observer.error(error);
            observer.complete();
          });
      }
    );
  }
}
