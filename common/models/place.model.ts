export interface Place {
  place_id: string;
  description: string;
  name: string;
  state: string;
  country: string;
  lat: number;
  lng: number;
}
