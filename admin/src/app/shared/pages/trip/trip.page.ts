import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarManagementPage } from '@app/car-management/car-management.page';
import { CurrentDriversComponent } from '@app/drivers-management/components/current-drivers/current-drivers.component';
import { GaragesManagementPage } from '@app/garages-management/garages-management.page';
import { GoogleMapsService } from '@app/home/services/google-maps/google-maps.service';
import { SettingsService } from '@app/settings/services/settings-service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { NotificationService } from '@app/staging/send-notifications/services/notification.service';
import {
  CarType,
  DriverTripStatus,
  TransactionType,
  TripStatus,
} from '@common/enums';
import { DistanceMatrix, SystemFlags, Trip } from '@common/models';
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from '@ionic-native/launch-navigator/ngx';
import { ModalController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase';
import * as _ from 'lodash';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.page.html',
  styleUrls: ['./trip.page.scss'],
})
export class TripPage implements OnInit {
  private systemFlags: SystemFlags;
  public readonly driverTripStatusEnum: typeof DriverTripStatus = DriverTripStatus;
  public trip: Partial<Trip>;
  public isLoading;

  public isInfoUpdateLoading;
  public isPriceBaseLoading: boolean;
  public isPriceSalaryLoading: boolean;
  public isDriversRequestLoading: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tripsService: TripsService,
    private launchNavigator: LaunchNavigator,
    private settingsService: SettingsService,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute,
    private googleMapsService: GoogleMapsService,
    private authService: AuthService,
    private notificationService: NotificationService,
    private translateService: TranslateService
  ) {
    this.loadTripData(this.activatedRoute.snapshot.params.tripId);
  }

  ngOnInit() {}

  // TODO: change to real time later
  private async loadTripData(tripId: string): Promise<void> {
    this.isLoading = true;
    try {
      const doc = await this.tripsService.getTripByID(tripId);
      this.trip = doc.data();
      this.trip.id = doc.id;
      if (!!!this.trip.pricing) {
        this.trip.pricing = {};
      }

      this.loadTripDistanceMatrix(this.trip);
    } catch (error) {
      console.error(error);
    } finally {
      this.isLoading = false;
    }
  }

  private async loadTripDistanceMatrix(trip: Partial<Trip>): Promise<void> {
    this.systemFlags = (await this.settingsService.getFlags()).data() as SystemFlags;

    this.googleMapsService
      .getDistanceBetweenPlaces(trip.pickupPlace, trip.dropPlace)
      .subscribe((distanceMatrix: DistanceMatrix) => {
        this.trip.distanceMatrix = distanceMatrix;
        this.trip.distanceMatrix.distance.value =
          distanceMatrix.distance.value / 1000;
        this.trip.distanceMatrix.price =
          distanceMatrix.distance.value *
          (this.trip.carType === CarType.SUV
            ? this.systemFlags.kmPriceSUV
            : this.systemFlags.kmPrice);

        if (!!!this.trip.pricing.beforePromo) {
          this.loadSystemFlagsAndPriceEstimation();
        }
        if (!!!this.trip.pricing.driverSalary) {
          this.loadDriverSalaryList();
        }
      });
  }

  private async loadSystemFlagsAndPriceEstimation(): Promise<void> {
    this.isPriceBaseLoading = true;
    try {
      this.trip.pricing.beforePromo = this.trip.distanceMatrix.price || 0;
      if (this.trip.isTwoWay) {
        this.trip.pricing.beforePromo =
          this.trip.pricing.beforePromo * 1.5 +
          this.trip.stayingPeriodInHours * this.systemFlags.waitingHourPrice;
      }

      this.trip.pricing.beforePromo = Math.round(this.trip.pricing.beforePromo);
    } catch (error) {
      console.error(error);
    } finally {
      this.isPriceBaseLoading = false;
    }
  }

  private async loadDriverSalaryList(): Promise<void> {
    this.isPriceSalaryLoading = true;
    try {
      const priceList = (await this.settingsService.getPriceList()).data()
        .states;
      if (this.trip.dropPlace.state === 'cairo') {
        this.trip.pricing.driverSalary =
          priceList[this.trip.pickupPlace.state].toDriverSalary;
      } else {
        this.trip.pricing.driverSalary =
          priceList[this.trip.dropPlace.state].fromDriverSalary;
      }
    } catch (error) {
      console.error(error);
    } finally {
      this.isPriceSalaryLoading = false;
    }
  }

  public async launchGoogleNavigator(location) {
    const options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    };

    await this.launchNavigator.navigate(location, options);
  }

  public async openCarManagementModal(): Promise<void> {
    const selectCarModal = await this.modalController.create({
      component: CarManagementPage,
      componentProps: {
        tripId: this.trip.id,
      },
    });
    await selectCarModal.present();
    const selectedCar = (await selectCarModal.onWillDismiss()).data;
    if (!!selectedCar) {
      this.trip.car = selectedCar;
      delete this.trip.car.notes;
    }
  }

  public async selectCarFromGarage(): Promise<void> {
    const selectCarModal = await this.modalController.create({
      component: GaragesManagementPage,
      componentProps: {
        select: 'car',
        trip: this.trip,
      },
    });
    await selectCarModal.present();
    const selectedCar = (await selectCarModal.onWillDismiss()).data;
    if (!!selectedCar) {
      this.trip.car = selectedCar;
      delete this.trip.car.notes;
    }
  }

  public async selectDriverFromGarage(): Promise<void> {
    const selectCarModal = await this.modalController.create({
      component: GaragesManagementPage,
      componentProps: {
        select: 'driver',
        trip: this.trip,
      },
    });
    await selectCarModal.present();
    const selectedDriver = (await selectCarModal.onWillDismiss()).data;
    if (!!selectedDriver) {
      this.trip.driver = {
        fullName: selectedDriver.fullName,
        uid: selectedDriver.uid,
        email: selectedDriver.email,
        rate: selectedDriver.rate,
        pictureURL: selectedDriver.pictureURL || '',
        phone: selectedDriver.phone,
        garage: selectedDriver.garage,
      };
    }
  }

  public clearDriver(): void {
    this.trip.driver = firebase.firestore.FieldValue.delete() as any;
  }

  public async openDriverManagementModal(): Promise<void> {
    const selectDriverModal = await this.modalController.create({
      component: CurrentDriversComponent,
      componentProps: {
        tripId: this.trip.id,
        isModal: true,
        isMultiSelect: true,
      },
    });
    await selectDriverModal.present();
    const selectedDriversUids: any = (await selectDriverModal.onWillDismiss())
      .data;
    if (!!selectedDriversUids) {
      this.sendDriversRequest(selectedDriversUids);
    }
  }

  public async sendDriversRequest(selectedDriversUidsObj: {
    [uid: string]: boolean;
  }): Promise<void> {
    this.isDriversRequestLoading = true;
    const selectedDriversUids: string[] = Object.keys(
      selectedDriversUidsObj
    ).filter(uid => {
      return !!selectedDriversUidsObj[uid];
    });

    if (selectedDriversUids.length > 0) {
      try {
        await this.notificationService.sendMassNotifications(
          selectedDriversUids,
          {
            title:
              'طلب رحلة جديد لـ' +
              this.translateService.instant(
                `governorates.${this.trip.dropPlace.state}`
              ) +
              ' مقابل ' +
              this.trip.pricing.driverSalary +
              'جم',
            message: 'أقبل الرحلة فى أسرع وقت.',
            clickAction: `/trip/${this.trip.id}?trip_request=true`,
            color: '#00681E',
          }
        );

        this.trip.driversRequest = {
          sentOnDateTime: new Date().getTime(),
          driversUids: _.get(this.trip, 'driversRequest.driversUids', []),
        };
        this.trip.driversRequest.driversUids = this.trip.driversRequest.driversUids.concat(
          selectedDriversUids
        );

        this.updateTripInfo(false);
      } catch (error) {
        console.error(error);
      } finally {
        this.isDriversRequestLoading = false;
      }
    } else {
      this.isDriversRequestLoading = false;
    }
  }

  public async updateTripInfo(shouldBack = true): Promise<void> {
    this.isInfoUpdateLoading = true;
    try {
      if (!!this.trip.pricing.beforePromo) {
        this.trip.pricing.base = this.getAppliedPromotion(
          this.trip.pricing.base,
          this.trip.promo
        );
      }
      if (!!this.trip.driver && !!this.trip.car) {
        this.trip.status = TripStatus.FUTURE;
        this.trip.isQueued = true;
        this.trip.driverStatus = !!this.trip.driverStatus
          ? this.trip.driverStatus
          : DriverTripStatus.QUEUED;
      } else {
        this.trip.status = TripStatus.NEW;
      }

      if (this.trip.driverStatus === DriverTripStatus.RETURNING) {
        this.trip.status = TripStatus.PENDING_PAST;
      }

      if (this.trip.driverStatus === DriverTripStatus.FINISHED) {
        this.trip.status = TripStatus.PAST;
        this.trip.isQueued = false;
        this.trip.conclusionDateTime = new Date().getTime();
        await this.authService.addTransaction(this.trip.driver.uid, {
          amount: this.trip.pricing.driverSalary,
          type: TransactionType.TRIP,
          method: '',
          dateTime: new Date().getTime(),
        });
      } else if (this.trip.driverStatus === DriverTripStatus.MEETING_CLIENT) {
        this.trip.status = TripStatus.CURRENT;
      }
      console.log(this.trip);
      await this.tripsService.updateTripInfo(this.trip);
      if (shouldBack) {
        await this.navController.navigateBack(['../../'], {
          relativeTo: this.route,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      this.isInfoUpdateLoading = false;
    }
  }

  public getStateParsedURL(stateName: string = ''): string {
    return `url(/assets/states/${stateName
      .replace(/\s/g, '_')
      .toLowerCase()}.jpeg)`;
  }

  public getAppliedPromotion(price: number = 0, promo: any): number {
    const percentage = (promo.percentage / 100) * price;
    if (promo.money < percentage) {
      return price - promo.money;
    } else {
      return price - percentage;
    }
  }
}
