export interface ClientFilter {
  fullName: string;
  email: string;
  phone: string;
  rate: number;
  // inTrip: string;
  // role: UserRole.CLIENT;
}
