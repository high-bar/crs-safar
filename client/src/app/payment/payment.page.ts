import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ModalController } from '@ionic/angular';

import { AddCardModalComponent } from './components/add-card-modal/add-card-modal.component';
import { PromoService } from './services/promo/promo.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  public activePromo: any;

  public newPromo: any;
  public isPromoLoading: boolean;
  public manualPromoCode: string;

  public isActivationLoading: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private promoService: PromoService,
    private authService: AuthService
  ) {
    this.loadActivePromo();
    this.loadPromoData();
  }

  ngOnInit() {}

  public async openAddCardModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddCardModalComponent,
    });
    await modal.present();
  }

  public async loadActivePromo(): Promise<void> {
    this.activePromo = JSON.parse(this.authService.getSavedUser()).activePromo;
  }

  public async loadPromoData(): Promise<void> {
    const newPromoCodeId = this.activatedRoute.snapshot.queryParams.newPromoCode;

    if (!!newPromoCodeId) {
      this.isPromoLoading = true;
      try {
        const newPromoData = await this.promoService.getPromoById(
          newPromoCodeId
        );
        this.newPromo = newPromoData.data();
        this.newPromo.id = newPromoData.id;
      } catch (error) {
        console.error(error);
      } finally {
        this.isPromoLoading = false;
      }
    }
  }

  public async activatePromo(): Promise<void> {
    if (this.newPromo) {
      this.isActivationLoading = true;
      try {
        await this.promoService.activatePromo(this.newPromo);
        this.activePromo = this.newPromo;
        this.newPromo = undefined;
      } catch (error) {
      } finally {
        this.isActivationLoading = false;
      }
    }
  }
}
