import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientManagementPage } from './client-management.page';

describe('ClientManagementPage', () => {
  let component: ClientManagementPage;
  let fixture: ComponentFixture<ClientManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientManagementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
