export interface Governorate {
  _id: string;
  placeid: string;
  name: string;
  gname: string;
  __v: number;
}
