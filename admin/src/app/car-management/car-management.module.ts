import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { CarManagementPage } from './car-management.page';
import { AddCarPage } from './pages/add-car/add-car.page';
import { FilterCarPage } from './pages/filter-car/filter-car.page';

const routes: Routes = [
  {
    path: '',
    component: CarManagementPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AddCarPage, FilterCarPage],
  entryComponents: [AddCarPage, FilterCarPage],
  exports: [AddCarPage, FilterCarPage],
})
export class CarManagementPageModule {}
