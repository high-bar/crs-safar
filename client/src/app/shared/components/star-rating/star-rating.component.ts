import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent {
  private numberOfStars = 5;
  public stars: number[] = new Array(this.numberOfStars)
    .fill(0)
    .map((_starNumber, starI) => {
      return starI + 1;
    });

  @Input() public value = 1;
  @Output() public valueChange: EventEmitter<number> = new EventEmitter<
    number
  >();
  @Input() public readOnly = false;

  constructor() {}

  public getStarFillPercentage(star: number): number {
    return (
      Math.round((star / this.stars.length - 1 / this.stars.length / 2) * 10) /
      10
    );
  }

  @HostListener('click', ['$event'])
  public async onTouchStart(e: MouseEvent) {
    if (!this.readOnly) {
      const offsetX = e.clientX - (e.target as any).offsetLeft;
      this.value = parseFloat(
        (offsetX / (e.target as any).offsetWidth).toFixed(1)
      );
      this.value = this.value > 1 ? 1 : this.value;
      this.valueChange.emit(this.value);
    }
  }

  @HostListener('touchmove', ['$event'])
  public async onTouchMove(e: TouchEvent) {
    if (!this.readOnly) {
      const offsetX = e.touches['0'].clientX - (e.target as any).offsetLeft;
      this.value = parseFloat(
        (offsetX / (e.target as any).offsetWidth).toFixed(1)
      );
      this.value = this.value > 1 ? 1 : this.value;
      this.valueChange.emit(this.value);
    }
  }
}
