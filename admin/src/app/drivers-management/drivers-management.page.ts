import { Component, OnInit, ViewChild } from '@angular/core';
import { DriverFilter } from '@common/models';
import {
  IonSlides,
  ModalController,
  NavController,
  Platform,
} from '@ionic/angular';
import { Subscription } from 'rxjs';

import { FilterDriverPage } from './pages/filter-driver/filter-driver.page';

@Component({
  selector: 'app-drivers-management',
  templateUrl: './drivers-management.page.html',
  styleUrls: ['./drivers-management.page.scss'],
})
export class DriversManagementPage implements OnInit {
  private backButtonSubscription: Subscription;
  public driverFilter: DriverFilter;

  @ViewChild('slider', { static: true }) slider: IonSlides;
  public currentTab = 0;

  constructor(
    private modalController: ModalController,
    private platform: Platform,
    private navController: NavController
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async openFilterDriverModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterDriverPage,
      componentProps: {
        value: this.driverFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.driverFilter = modalOutput.data;
    });
  }
}
