import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TripStatus } from '@common/enums';
import { UserRole } from '@common/enums';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TripsHistoryService {
  constructor(private http: HttpClient) {}

  public getPastTrips(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        // need a filter for same client in users
        .collection('trips')
        .where('status', '==', TripStatus.PAST)
        .where('client.uid', '==', 'user.uid')
        .orderBy('requestDateTime', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }
}
