import { Component, ViewChild } from '@angular/core';
import { TripsService } from '@app/home/services/trips.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { Trip } from '@common/models';
import { Toast } from '@ionic-native/toast/ngx';
import { IonSlides, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage {
  private backButtonSubscription: Subscription;
  private lastTimeBackPress = 0;
  private exitBackTimeFrame = 2000;

  @ViewChild('overviewSlides', { static: true }) public overviewSlides: IonSlides;
  public currentSlideIndex = 0;

  public activeTrip: Partial<Trip>;
  public newTrips: Partial<Trip>[] = [];
  public nextTrips: Partial<Trip>[] = [];

  public isActiveLoading: boolean;
  public isNextLoading: boolean;

  constructor(
    private tripsService: TripsService,
    private authService: AuthService,
    private platform: Platform,
    private toast: Toast,
  ) {
    const authServiceSubscription = this.authService.change.subscribe(user => {
      if (!!user) {
        this.loadActiveTrip();
        this.loadNewTrips();
        this.loadFutureTrips();
        // authServiceSubscription.unsubscribe();
      }
    });
  }

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        if (
          new Date().getTime() - this.lastTimeBackPress <
          this.exitBackTimeFrame
        ) {
          navigator['app'].exitApp();
        } else {
          this.toast
            .show(`Press back again to exit`, '2000', 'bottom')
            .subscribe();

          this.lastTimeBackPress = new Date().getTime();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  private async loadNewTrips(): Promise<void> {
    this.isNextLoading = true;

    this.tripsService.getNewTrips().subscribe(
      data => {
        this.newTrips = data.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          return trip;
        });
        this.isNextLoading = false;
      },
      error => {
        console.error(error);
        this.isNextLoading = false;
      }
    );
  }

  private async loadFutureTrips(): Promise<void> {
    this.isNextLoading = true;

    this.tripsService.getFutureTrips().subscribe(
      data => {
        this.nextTrips = data.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          return trip;
        });
        this.isNextLoading = false;
      },
      error => {
        console.error(error);
        this.isNextLoading = false;
      }
    );
  }

  private loadActiveTrip(): void {
    this.isActiveLoading = true;
    this.tripsService.getActiveTrip().subscribe(
      data => {
        this.activeTrip = data.docs.map(doc => {
          return doc.data();
        })[0];
        this.isActiveLoading = false;
      },
      error => {
        console.error(error);
        this.isActiveLoading = false;
      }
    );
  }

  public async slideChanged(): Promise<void> {
    this.currentSlideIndex = await this.overviewSlides.getActiveIndex();
  }
}
