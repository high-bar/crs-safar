import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterDriverPage } from './filter-driver.page';

describe('FilterDriverPage', () => {
  let component: FilterDriverPage;
  let fixture: ComponentFixture<FilterDriverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterDriverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterDriverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
