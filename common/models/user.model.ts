import { Gender, UserRole } from '@common/enums';
import { TripHistory } from '@common/models';

export interface User {
  uid: string;
  fullName: string;
  email: string;
  phone: string;
  gender: Gender;
  pictureURL: string;
  rate: number;
  trips: TripHistory;
  role: UserRole;
  password?: string;
  joiningDate?: number;
  currentTripId?: string;
  state: string;
  isBlocked: boolean;
  blockDate: number;
  balance: number;
  provider: string;
}
