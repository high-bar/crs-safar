import { Injectable } from '@angular/core';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ComplainsService {
  constructor() {}

  public getComplains(): Observable<firestore.QuerySnapshot> {
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('complains')
        .orderBy('joiningDate', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }
}
