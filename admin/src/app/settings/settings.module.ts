import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { BlockedUsersComponent } from './components/blocked-users/blocked-users.component';
import { TripsSalaryPage } from './pages/trips-salary/trips-salary.page';
import { SettingsPage } from './setting.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage,
  },
  {
    path: 'trips-salary',
    component: TripsSalaryPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SettingsPage, TripsSalaryPage, BlockedUsersComponent],
  entryComponents: [BlockedUsersComponent],
})
export class SettingsPageModule {}
