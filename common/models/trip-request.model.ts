import { CarType } from '@common/enums';

import { PaymentMethod } from './payment-method.model';

export interface TripRequest {
  id: string;
  pickupPlace: google.maps.places.AutocompletePrediction;
  dropPlace: google.maps.places.AutocompletePrediction;
  pickupDateTime: Date;
  isTwoWay: boolean;
  stayingPeriodInHours?: number;
  carType: CarType;
  paymentMethod: PaymentMethod;
}
