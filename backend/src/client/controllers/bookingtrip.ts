import * as mongoose from 'mongoose';
import { TripSchema } from '../../common/models/trip';
import { Request, Response } from 'express';
const https = require('https');
import { config } from '../../const/config';
import * as request from 'request-promise-native';
import { PricesSchema } from '../../common/models/prices';
import { GovernorateSchema } from '../../common/models/governorate';

const Trip = mongoose.model('Trip', TripSchema);
const Prices = mongoose.model('Prices', PricesSchema);
const Governorate = mongoose.model('Governorate', GovernorateSchema);

export class BookingTripController {
  constructor() {}

  public async bookTrip(req: Request, res: Response): Promise<void> {
    let FromPlaceId = req.body.FromPlaceId;
    let ToPlaceId = req.body.ToPlaceId;
    //
    let data = await this.calculateDistance(FromPlaceId, ToPlaceId);
    let data1 = JSON.parse(data);
    //
    let governorateFrom = await this.Getgovernorate(FromPlaceId);
    let xy = JSON.parse(governorateFrom);
    // console.log(xy.result.address_components);

    let Governorates: any = await this.listGovernorates();
    let wewe = xy.result.address_components.find(
      async (address_component: any) => {
        let i: any;
        for (i = 0; i <= Governorates.length; i++) {
          return address_component.long_name.indexOf(Governorates[i].name) >= 0;
        }
      }
    );
    //  console.log(wewe.long_name);
    ////
    let governorateTo = await this.Getgovernorate(ToPlaceId);
    let xy2 = JSON.parse(governorateTo);

    let wewe2 = xy2.result.address_components.find(
      async (address_component: any) => {
        let i: any;
        for (i = 0; i <= Governorates.length; i++) {
          return address_component.long_name.indexOf(Governorates[i].name) >= 0;
        }
      }
    );
    // console.log(wewe2.long_name);
    ///
    let price: any = await this.getPrice(wewe.long_name, wewe2.long_name);
    console.log(price);

    let GoDistance = data1.rows[0].elements[0].distance.text;
    let GoDuration = data1.rows[0].elements[0].duration.text;
    let status = 'pending';
    //////////
    //    set values
    let newTrip = new Trip({
      FromPlaceId: req.body.FromPlaceId,
      ToPlaceId: req.body.ToPlaceId,
      goingDateTime: req.body.goingDateTime,
      goingEstimatedDurationInMinutes: GoDuration,
      goingEstimatedDistance: GoDistance,
      priceInEGP: price.price,
      client: (<any>req).decoded.id,
      status: status,
    });

    //   save Trip
    newTrip.save((err, Trip) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json({
          message: 'Trip Added!',
          Trip: Trip,
        });
      } //end else
    }); //end save trip
  } //edn booktrip

  //
  public calculateDistance(
    FromPlaceId: any,
    ToPlace_Id: any
  ): request.RequestPromise<any> {
    let options = {
      uri: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=place_id:${FromPlaceId}&destinations=place_id:${ToPlace_Id}&key=${
        config.MapApikey
      }`,
    };
    return request.get(options);
  }
  //
  //
  public Getgovernorate(PlaceId: any): request.RequestPromise<any> {
    let options = {
      uri: `https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAMeIayb9KnPXwK-VJ6Poq4vv-h5n9LkQE&placeid=${PlaceId}`,
    };
    return request.get(options);
  }
  //
  /////// get price ///
  public getPrice(governorateFrom: any, governorateTo: any) {
    return new Promise(function(resolve, reject) {
      Prices.findOne({ gfrom: governorateFrom, gto: governorateTo }, function(
        err,
        price
      ) {
        if (err) {
          reject(err);
        } else if (price === undefined || price === null) {
          reject('undefind or null');
        } else {
          resolve(price);
        }
        //
      });
      //
    }); // end promise
  }
  ///////////////////

  /////// get governorates ///
  public listGovernorates() {
    return new Promise(function(resolve, reject) {
      Governorate.find({}, 'name', function(err: any, Governorates: any) {
        if (err) {
          reject(err);
        } else {
          resolve(Governorates);
        }
      }); // end find all Governorates
    }); // end promise
  }
  ///////////////////
  //
} //end booking trip class
