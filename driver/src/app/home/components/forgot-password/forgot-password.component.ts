import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { UtilService } from '@app/shared/services/util/util.service';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  public forgotPasswordForm: FormGroup = this.formBuilder.group({
    email: [undefined, [Validators.required, Validators.email]],
  });
  public isLoading: boolean;
  public isRestMailSent: boolean;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }
  public async submitForgotPassword(): Promise<void> {
    if (this.forgotPasswordForm.valid) {
      this.isLoading = true;
      try {
        await this.authService.resetPassword(
          this.forgotPasswordForm.value.email
        );
        this.isRestMailSent = true;
      } catch (error) {
        if (error.code === 'auth/user-not-found') {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.reset.${error.code}`)
          );
        } else {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        }
      } finally {
        this.isLoading = false;
      }
    }
  }
}
