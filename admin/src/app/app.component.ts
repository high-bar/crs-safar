import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { LocalizationService } from '@app/shared/services/localization/localization.service';
import { Language, WritingDir } from '@common/enums';
import { Localization } from '@common/models';
import { Push } from '@ionic-native/push/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public documentDir;

  public appPages = [
    {
      title: 'الرئيسية',
      url: '/home',
      icon: 'home',
    },
    {
      title: 'إدارة السائقين',
      url: '/drivers-management',
      icon: 'im icon-drivers',
    },
    {
      title: 'إدارة العملاء',
      url: '/clients-management',
      icon: 'im icon-clients',
    },
    {
      title: 'إدارة السيارات',
      url: '/car-management',
      icon: 'im icon-cars',
    },
    {
      title: 'تعقب السائقين',
      url: '/cars-tracking',
      icon: 'im icon-cars-tracking',
    },
    {
      title: 'تاريخ الرحلات',
      url: '/trips-history',
      icon: 'calendar',
    },
    {
      title: 'إدارة الجراجات',
      url: '/garages-management',
      icon: 'car',
    },
    {
      title: 'إرسال الإشعارات',
      url: '/send-notifications',
      icon: 'paper-plane',
    },
    {
      title: ' أكواد البرومو',
      url: '/promo-codes',
      icon: 'gift',
    },
    {
      title: 'ادارة الشكاوي',
      url: '/complains-management',
      icon: 'gift',
    },
    {
      title: 'الإحصائيات',
      url: '/statistics',
      icon: 'stats',
    },
    {
      title: 'الإعدادات ',
      url: '/settings',
      icon: 'settings',
    },
  ];

  public user: any;
  public isAuthorized: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translateService: TranslateService,
    private localizationService: LocalizationService,
    private authService: AuthService,
    private push: Push
  ) {
    this.authService.change.subscribe(user => {
      this.user = user;
      this.isAuthorized = !!user;
    });
    this.initializeApp();
    LocalizationService.localizationChange.subscribe(
      (newLocalization: Localization) => {
        this.documentDir = newLocalization.dir;
      }
    );
  }

  initializeApp() {
    this.translateService.setDefaultLang('ar');
    this.platform.ready().then(() => {
      this.documentDir = WritingDir[Language.ar];
      this.setInitialLanguage();
      this.push.init(environment.configs.pushOptions);
      this.statusBar.backgroundColorByHexString('#780002'); // --ion-color-secondary
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }

  public async setInitialLanguage(): Promise<void> {
    this.localizationService.setLocalization({
      lang: Language.ar,
      dir: WritingDir[Language.ar],
    });
  }
}
