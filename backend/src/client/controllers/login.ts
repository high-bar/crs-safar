import * as mongoose from 'mongoose';
import { UserSchema } from '../../common/models/user';
import { Request, Response } from 'express';
import { config } from '../../const/config';
import { json } from 'body-parser';

// import * as multer from 'multer'
// const multer = require("multer");
import * as fs from 'fs';
import * as path from 'path';
let formidable = require('formidable');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

const User = mongoose.model('User', UserSchema);

export class ClientSignInController {
  public getClient(req: Request, res: Response) {
    //
    function createToken(User: any) {
      let token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
          phone: User.phone,
          gender: User.gender,
          country: User.country,
          state: User.state,
          rate: User.rate,
        },
        secretKey
      );
      return token;
    }
    //
    // find user
    User.findOne({ email: req.body.email }, function(err: any, User: any) {
      if (err) throw err;

      if (!User) {
        res.status(401).json({
          error: 'invalid user',
        });
      } else if (User) {
        let password = req.body.password;
        bcrypt.compare(password, User.password).then(function(cresult: any) {
          if (cresult == true) {
            //create token
            let token = createToken(User);
            res.json({
              User,
              token: token,
            });
          } else {
            res.status(401).json({
              error: 'invalid Admin',
            });
          }
          //
        }); //end bcrypt compare
      } //end else if user found
    }); // end find one
  } //

  /////
  public async UserProfile(req: Request, res: Response): Promise<void> {
    let userid = (<any>req).decoded.id;
    User.find({ _id: userid }, function(err, User) {
      if (err) {
        res.send(err);
      }
      res.json(User);
    });
  } //end
  /////

  /////verify token///////
  public TokenVerify(req: Request, res: Response) {
    let token = req.body.token;
    jwt.verify(
      token,
      secretKey,
      { clockTimestamp: new Date().getTime() },
      function(err: any, decoded: any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        }
        res.json({
          decoded: decoded,
        });
        console.log(decoded);
      }
    );
  } //end
  /////

  /////UploadProgile Image///////
  public UploadProfile(req: Request, res: Response) {
    let userid = (<any>req).decoded.id;
    let form = new formidable.IncomingForm();
    form.parse(req, function(err: any, fields: any, files: any) {
      let oldpath = files.filetoupload.path;
      let newpath = path.join(__dirname, '/../../public/client/') + files.filetoupload.name;
      let pictureURL:any;
      fs.rename(oldpath, newpath, function(err) {
        pictureURL = newpath;
        let url = pictureURL.split("\\");
        let imgurl = "https://safarprivatetaxi.com/client/" + url[6];
        if (err) throw err;
        User.findOneAndUpdate(
          { _id: userid },
          {
            $set: {
              pictureURL: imgurl
            },
          },
          function(err, User) {
            if (err) {
              res.status(401).json({
                error: err,
              });
            } else {
              res.json({
                message: 'User Profile PIC Update!',
                User: User,
                imgurl: imgurl
              });
            }
          }
        ); // end update
      });
    });
  }

  //end
  ///
}
