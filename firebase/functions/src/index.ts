// tslint:disable:no-implicit-dependencies
import * as functions from 'firebase-functions';
import { firestore, initializeApp, messaging } from 'firebase-admin';
import {
  Transaction,
  User,
  Trip,
  Driver,
  BackgroundGeolocation,
  SystemFlags,
  NotificationPayLoad,
  Car,
  Garage,
} from '@common/models';
import { Localization } from './localization.const';

initializeApp();

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
// firebase deploy --only functions:onPromoCreate

export * from './algolia-search';

const sendNotification = async (
  topic: string,
  payload: NotificationPayLoad
): Promise<messaging.MessagingTopicResponse> => {
  return messaging().sendToTopic(topic, {
    // notification: {
    //   title: 'Hey',
    //   message: 'No',
    //   icon: 'fcm_push_icon',
    //   color: '#b50003',
    //   clickAction: 'tracking'
    // },
    data: {
      icon: 'fcm_push_icon',
      color: '#b50003',
      notId: `${Math.round(Math.random() * 999999)}`,
      ...payload,
    },
  });
};

const updateStats = async function(
  collection: firestore.CollectionReference,
  context: functions.EventContext,
  snapshot: functions.Change<firestore.DocumentSnapshot>,
  targetCounter: string
) {
  const beforeData = snapshot.before.data() as any;
  const afterData = snapshot.after.data() as any;

  const statsRef = collection.doc('--stats--');
  const newStats: any = {};

  if (context.eventType === 'create') {
    newStats[afterData[targetCounter]] = firestore.FieldValue.increment(1);
    newStats.total = firestore.FieldValue.increment(1);
  } else if (context.eventType === 'delete') {
    newStats[beforeData[targetCounter]] = firestore.FieldValue.increment(-1);
    newStats.total = firestore.FieldValue.increment(-1);
  } else if (afterData[targetCounter] !== beforeData[targetCounter]) {
    newStats[afterData[targetCounter]] = firestore.FieldValue.increment(1);
    newStats[beforeData[targetCounter]] = firestore.FieldValue.increment(-1);
  }

  await statsRef.update(newStats);
};

export const helloWorld = functions.https.onRequest(
  async (_request, response) => {
    (await firestore()
      .collection('users')
      .get()).docs.forEach(async doc => {
      await doc.ref.update({
        provider: 'password',
      });
    });
    response.send('Hello from Firebase!');
  }
);

export const onUserCreate = functions.firestore
  .document('/users/{uid}')
  .onCreate(async (snapshot, context) => {
    await updateStats(
      firestore().collection('users'),
      context,
      {
        before: snapshot,
        after: snapshot,
      },
      'role'
    );

    return snapshot.ref.update({
      balance: 0,
      isBlocked: false,
      rate: 5,
      trips: {
        past: [],
        future: [],
        active: [],
      },
    });
  });

export const onPromoCreate = functions.firestore
  .document('/promos/{promoId}')
  .onCreate(async (snapshot, context) => {
    const newPromoData: any = snapshot.data();
    // send notification

    return sendNotification('clients', {
      title: 'New Promo',
      message: newPromoData.message,
      color: '#b50003',
      clickAction: `/payment?newPromoCode=${context.params.promoId}`,
    });
  });

export const onUserUpdate = functions.firestore
  .document('/users/{uid}')
  .onUpdate(async (snapshot, _context) => {
    const newUserData: User = snapshot.after.data() as User;

    (await firestore()
      .collection('trips')
      .where('client.uid', '==', newUserData.uid)
      .get()).docs.forEach(async doc => {
      await doc.ref.update({
        client: {
          email: newUserData.email,
          fullName: newUserData.fullName,
          phone: newUserData.phone,
          pictureURL: newUserData.pictureURL,
          rate: newUserData.rate,
          uid: newUserData.uid,
        },
      });
    });

    (await firestore()
      .collection('trips')
      .where('driver.uid', '==', newUserData.uid)
      .get()).docs.forEach(async doc => {
      await doc.ref.update({
        driver: {
          email: newUserData.email,
          fullName: newUserData.fullName,
          phone: newUserData.phone,
          pictureURL: newUserData.pictureURL,
          rate: newUserData.rate,
          uid: newUserData.uid,
        },
      });
    });
  });

export const onUserTransaction = functions.firestore
  .document('/users/{uid}/transactions/{transactionId}')
  .onCreate(async (snapshot, context) => {
    const moneyTransaction: Transaction = snapshot.data() as Transaction;
    const userRef = firestore()
      .collection('users')
      .doc(context.params.uid);

    return firestore().runTransaction(transaction => {
      return transaction.get(userRef).then(userDoc => {
        const userBalance: number = parseFloat(
          `${(userDoc.data() as User).balance || 0}`
        );
        transaction.update(userRef, {
          balance: userBalance + (moneyTransaction.amount || 0),
        });
      });
    });
  });

export const onTripUpdated = functions.firestore
  .document('/trips/{tripId}')
  .onUpdate(async (change, context) => {
    const tripBefore: Trip = change.before.data() as Trip;
    const tripAfter: any = change.after.data() as Trip;
    tripAfter.id = change.after.id;

    // has scheduled or driver changed
    if (
      (!!!tripBefore.driver ||
        tripBefore.driver.uid !== tripAfter.driver.uid) &&
      !!tripAfter.car
    ) {
      // send notification to driver
      await sendNotification(tripAfter.driver.uid as string, {
        title: 'تم تعيين رحلة جديدة لك',
        message: ` ${tripAfter.dropPlace.state} لديك رحلة جديدة إلى`,
        clickAction: `/trip/${tripAfter.id}`,
        color: '#00681E',
      });

      // send notification to client
      await sendNotification(tripAfter.client.uid as string, {
        title: `Your ${
          tripAfter.dropPlace.state
        } trip has been assigned a driver & a car.`,
        message: `${tripAfter.driver.fullName} will pick you up in ${
          tripAfter.car.brand
        } ${tripAfter.car.model}.`,
        clickAction: `/trip/${tripAfter.id}`,
        color: '#b50003',
      });

      tripAfter.status = 'future';
      tripAfter.isQueued = true;
      tripAfter.driverStatus = !!tripAfter.driverStatus
        ? tripAfter.driverStatus
        : 'queued';

      await change.after.ref.update(tripAfter);
    } // has completed
    else if (tripAfter.status === 'past' && tripBefore.status !== 'past') {
      const driverRef = firestore()
        .collection('users')
        .doc(tripAfter.driver.uid);
      const clientRef = firestore()
        .collection('users')
        .doc(tripAfter.client.uid);

      await sendNotification(tripAfter.client.uid as string, {
        title: `You have reached your destination`,
        message: `Thank you for traveling with Safar, please review our service.`,
        clickAction: `/trip/${tripAfter.id}?isReview=true`,
        color: '#b50003',
      });

      await firestore().runTransaction(transaction => {
        return transaction.get(driverRef).then(driverDoc => {
          const driverTrips = (driverDoc.data() as Driver).trips;
          driverTrips.past = !!driverTrips.past ? driverTrips.past : [];
          driverTrips.past.push(context.params.tripId);
          transaction.update(driverRef, {
            trips: driverTrips,
          });
        });
      });

      return firestore().runTransaction(transaction => {
        return transaction.get(clientRef).then(clientDoc => {
          const clientTrips = (clientDoc.data() as Driver).trips;
          clientTrips.past = !!clientTrips.past ? clientTrips.past : [];
          clientTrips.past.push(context.params.tripId);
          transaction.update(clientRef, {
            trips: clientTrips,
          });
        });
      });
    }

    // route updated
    if ((tripBefore.route || []).length !== (tripAfter.route || []).length) {
      const mostRecentLocation: BackgroundGeolocation = tripAfter.route.pop() as BackgroundGeolocation;
      const maxAllowedSpeed: number = ((await firestore()
        .collection('util')
        .doc('flags')
        .get()).data() as SystemFlags).maxAllowedSpeed;

      if (maxAllowedSpeed < (mostRecentLocation.speed || 0) / 3.6) {
        // do it
        await sendNotification('admin-users', {
          title: Localization.speed_warning_title(
            tripAfter.driver.fullName || '',
            maxAllowedSpeed
          ),
          message: Localization.speed_warning_body(
            tripAfter.driver.fullName || '',
            ((mostRecentLocation.speed || 0) / 3.6).toFixed(2),
            mostRecentLocation.accuracy || 0
          ),
          clickAction: `/cars-tracking/${tripAfter.id}`,
        });
      }
    }

    // extra payments
    if (!!!tripBefore.pricing.actualExtra && !!tripAfter.pricing.actualExtra) {
      const clientChange =
        tripAfter.pricing.collectedExtra - tripAfter.pricing.actualExtra;
      if (clientChange !== 0) {
        // update client wallet
        await firestore()
          .collection('users')
          .doc(tripAfter.client.uid)
          .collection('transactions')
          .add({
            amount: clientChange,
            method: 'cash',
            type: 'trip-extra-costs',
            dateTime: new Date().getTime(),
          });

        await sendNotification(tripAfter.client.uid as string, {
          title: `Collected ${tripAfter.pricing.collectedExtra} EGP`,
          message: `${Math.abs(clientChange)} Has been ${
            clientChange > 0 ? 'added to' : 'subtracted from'
          } your wallet.`,
          clickAction: `/trip/${tripAfter.id}`,
          color: '#b50003',
        });
      }

      // update driver wallet
      await firestore()
        .collection('users')
        .doc(tripAfter.driver.uid)
        .collection('transactions')
        .add({
          amount: tripAfter.pricing.collectedExtra,
          method: 'cash',
          type: 'trip-extra-costs',
          dateTime: new Date().getTime(),
        });
    }
  });

export const isEmailUsed = functions.https.onCall(async (data, _context) => {
  const usersSnapshot = await firestore()
    .collection('users')
    .where('email', '==', data.email)
    .get();

  if (usersSnapshot.size >= 1) {
    return {
      provider: (usersSnapshot.docs[0].data() as User).provider,
    };
  } else {
    return {};
  }
});

export const onCarWrite = functions.firestore
  .document('cars/{carId}')
  .onWrite(async (snapshot, context) => {
    const beforeData: Car = snapshot.before.data() as Car;
    const afterData: Car = snapshot.after.data() as Car;

    await updateStats(
      firestore().collection('cars'),
      context,
      snapshot,
      'type'
    );

    let beforeGarageRef: firestore.DocumentReference,
      afterGarageRef: firestore.DocumentReference;

    if (
      context.eventType === 'delete' ||
      (beforeData.garage as Garage).id !== (afterData.garage as Garage).id
    ) {
      beforeGarageRef = firestore()
        .collection('garages')
        .doc((beforeData.garage as Garage).id);

      await firestore().runTransaction(transaction => {
        return transaction.get(beforeGarageRef).then(garageDoc => {
          if (garageDoc.exists) {
            const garageCars: string[] = (
              (garageDoc.data() as Garage).cars || []
            ).filter(carId => carId !== beforeData.id);

            transaction.update(beforeGarageRef, {
              cars: garageCars,
            });
          }
        });
      });
    }

    if (
      context.eventType !== 'delete' &&
      (beforeData.garage as Garage).id !== (afterData.garage as Garage).id
    ) {
      afterGarageRef = firestore()
        .collection('garages')
        .doc((afterData.garage as Garage).id);

      await firestore().runTransaction(transaction => {
        return transaction.get(afterGarageRef).then(garageDoc => {
          const garageCars: string[] = (garageDoc.data() as Garage).cars || [];
          garageCars.push(afterData.id);
          transaction.update(afterGarageRef, {
            cars: garageCars,
          });
        });
      });
    }
  });

export const onDriverWrite = functions.firestore
  .document('users/{driverId}')
  .onWrite(async (snapshot, context) => {
    const beforeData: Driver = snapshot.before.data() as Driver;
    const afterData: Driver = snapshot.after.data() as Driver;

    await updateStats(
      firestore().collection('users'),
      context,
      snapshot,
      'role'
    );

    if (context.eventType !== 'create' && beforeData.role === 'driver') {
      let beforeGarageRef: firestore.DocumentReference,
        afterGarageRef: firestore.DocumentReference;

      if (
        context.eventType === 'delete' ||
        (beforeData.garage as Garage).id !== (afterData.garage as Garage).id
      ) {
        beforeGarageRef = firestore()
          .collection('garages')
          .doc((beforeData.garage as Garage).id);

        await firestore().runTransaction(transaction => {
          return transaction.get(beforeGarageRef).then(garageDoc => {
            if (garageDoc.exists) {
              const garageDrivers: string[] = (
                (garageDoc.data() as Garage).drivers || []
              ).filter(driverId => driverId !== beforeData.uid);

              transaction.update(beforeGarageRef, {
                drivers: garageDrivers,
              });
            }
          });
        });
      }

      if (
        context.eventType !== 'delete' &&
        (beforeData.garage as Garage).id !== (afterData.garage as Garage).id
      ) {
        afterGarageRef = firestore()
          .collection('garages')
          .doc((afterData.garage as Garage).id);

        await firestore().runTransaction(transaction => {
          return transaction.get(afterGarageRef).then(garageDoc => {
            const garageDrivers: string[] =
              (garageDoc.data() as Garage).drivers || [];
            garageDrivers.push(afterData.uid);
            transaction.update(afterGarageRef, {
              drivers: garageDrivers,
            });
          });
        });
      }
    }
  });

export const onGarageUpdate = functions.firestore
  .document('garages/{garageId}')
  .onUpdate(async (snapshot, context) => {
    const beforeData: Garage = snapshot.before.data() as Garage;
    const afterData: Garage = snapshot.after.data() as Garage;

    // We basically don't care about any other changes
    if (beforeData.name !== afterData.name) {
      (await firestore()
        .collection('cars')
        .where('garage.id', '==', afterData.id)
        .get()).docs.forEach(async carDoc => {
        await carDoc.ref.update({
          garage: {
            id: afterData.id,
            name: afterData.name,
          },
        });
      });

      (await firestore()
        .collection('users')
        .where('garage.id', '==', afterData.id)
        .get()).docs.forEach(async userDoc => {
        await userDoc.ref.update({
          garage: {
            id: afterData.id,
            name: afterData.name,
          },
        });
      });
    }
  });

export const sendMassNotifications = functions.https.onCall(
  async (data, _context) => {
    const targetsUids: string[] = data.targetsUids;

    targetsUids.forEach(async targetUid => {
      await sendNotification(targetUid, data.payload);
    });
  }
);
