export interface BackgroundGeolocation {
  /** ID of location as stored in DB (or null) */
  id: number;
  /**
   * Native provider reponsible for location.
   *
   * Possible values:
   * "gps", "network", "passive" or "fused"
   */
  provider: string;
  /** Configured location provider. */
  locationProvider: string;
  /** UTC time of this fix, in milliseconds since January 1, 1970. */
  time: number;
  /** Latitude, in degrees. */
  latitude: number;
  /** Longitude, in degrees. */
  longitude: number;
  /** Estimated accuracy of this location, in meters. */
  accuracy: number;
  /**
   * Speed if it is available, in meters/second over ground.
   *
   * Note: Not all providers are capable of providing speed.
   * Typically network providers are not able to do so.
   */
  speed: number;
  /** Altitude if available, in meters above the WGS 84 reference ellipsoid. */
  altitude: number;
  /** Bearing, in degrees. */
  bearing: number;
  /**
   * True if location was recorded by mock provider. (ANDROID ONLY)
   *
   * Note: this property is not enabled by default!
   * You can enable it "postTemplate" configure option.
   */
  isFromMockProvider?: boolean;
  /**
   * True if device has mock locations enabled. (ANDROID ONLY)
   *
   * Note: this property is not enabled by default!
   * You can enable it "postTemplate" configure option.
   */
  mockLocationsEnabled?: boolean;
  distanceMovedInKm?: number;
}
