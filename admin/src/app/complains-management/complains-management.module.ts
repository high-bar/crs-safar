import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';

import { ComplainsManagementPage } from './complains-management.page';
import { ComplainDetailsPage } from './pages/complain-details/complain-details.page';

const routes: Routes = [
  {
    path: '',
    component: ComplainsManagementPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ComplainsManagementPage, ComplainDetailsPage],
  entryComponents: [ComplainsManagementPage, ComplainDetailsPage],
})
export class ComplainsManagementPageModule {}
