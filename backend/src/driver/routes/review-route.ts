import { DriverReviewController } from '../controllers/review';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class DriverReviewRoutes {
  public DriverReviewController: DriverReviewController = new DriverReviewController();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      let token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //

    ///////////////
    // start api
    app
      .route('/driver/addreview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverReviewController.addDriverReview.bind(
        this.DriverReviewController
      ));
    //// end api

    ///////////////
    // start api get my reviews
    app
      .route('/driver/getmyreview')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverReviewController.GetMyDriverReview.bind(
        this.DriverReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/driver/getreviews')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverReviewController.GetDriverReview.bind(
        this.DriverReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/driver/updatereview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverReviewController.UpdateDriverReview.bind(
        this.DriverReviewController
      ));
    //// end api
    // start api Get User Reviews
    app
      .route('/driver/deletereview')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverReviewController.DeleteDriverReview.bind(
        this.DriverReviewController
      ));
    //// end api

    //end app route
  }

  //end class
}
