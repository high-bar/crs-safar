import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { initializeApp } from 'firebase';
import 'hammerjs';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

initializeApp({
  apiKey: 'AIzaSyBtJNibbJeb9ahwQFCTkcVL8Fdsjhb5AV0',
  authDomain: 'safar-231521.firebaseapp.com',
  projectId: 'safar-231521',
});


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
