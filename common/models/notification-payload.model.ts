export interface NotificationPayLoad {
  title: string;
  message: string;
  icon?: string;
  color?: string;
  clickAction: string;
}
