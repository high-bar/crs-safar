import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { TransactionType } from '@common/enums';
import { Transaction, User } from '@common/models';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  private _user: User;
  @Input() set user(newValue: User) {
    this._user = newValue;
    if (newValue) {
      this.loadTransactions(this.user.uid);
    }
  }
  get user(): User {
    return this._user;
  }

  public isTransactionsLoading: boolean;
  public transactions: Transaction[] = [];
  public isPaymentLoading: boolean;

  constructor(
    private authService: AuthService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  public loadTransactions(userID: string): void {
    this.isTransactionsLoading = true;
    this.authService.getTransactions(userID).subscribe(
      data => {
        this.transactions = data.docs.map(doc => {
          const transaction = doc.data() as Transaction;
          transaction.id = doc.id;
          return transaction;
        });
        this.isTransactionsLoading = false;
      },
      error => {
        console.error(error);
        this.isTransactionsLoading = false;
      }
    );
  }

  public async openPayUserPop(): Promise<void> {
    const payUserPop = await this.alertController.create({
      header: 'تم دفع:',
      inputs: [
        {
          name: 'amount',
          type: 'number',
          placeholder: '0.00',
        },
      ],
      buttons: [
        {
          text: 'إلغاء',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'تأكيد',
          handler: data => {
            if (!!data.amount && data.amount !== 0) {
              this.payUser(-data.amount);
            }
          },
        },
      ],
    });

    await payUserPop.present();
  }

  public payUser(amount: number): void {
    this.isPaymentLoading = true;
    try {
      this.authService.addTransaction(this.user.uid, {
        amount: amount,
        method: 'cash',
        type: TransactionType.PAYMENT,
        dateTime: new Date().getTime(),
      });
      this.user.balance += amount;
    } catch (error) {
      console.error(error);
    } finally {
      this.isPaymentLoading = false;
    }
  }
}
