import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { ProfileService } from '@app/shared/services/profile/profile.service';
import { Client } from '@common/models';
import { ModalController, PopoverController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

import { EditProfilePage } from './pages/edit-profile/edit-profile.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public user: Client;

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private modalController: ModalController
  ) {
    this.authService.change.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {}

  public async openEditProfileModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: EditProfilePage,
      componentProps: {
        profileData: this.user,
      },
    });
    return await modal.present();
  }

  public getInviteLink(): string {
    return `${environment.ROOT_API_URL}/invite/${
      this.user.email.split('@')[0]
    }`;
  }

  public async pickProfilePhoto(): Promise<void> {
    try {
      this.user.pictureURL = window['Ionic'].WebView.convertFileSrc(
        await this.profileService.initPhotoPicking()
      );
    } catch (_error) {}
  }

  // async openRatingModal() {
  //   const modal = await this.modalController.create({
  //     component: RatingComponent,
  //     animated: true,
  //     backdropDismiss: true,
  //     componentProps: {
  //       value: this.user.rate,
  //       readOnly: true,
  //       user: this.user,
  //     },
  //   });
  //   return await modal.present();
  // }
}
