import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilterDriverPage } from '@app/drivers-management/pages/filter-driver/filter-driver.page';
import { Driver, DriverFilter } from '@common/models';
import { ModalController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { DriverService } from '../../services/driver-service/driver.service';

@Component({
  selector: 'app-current-drivers',
  templateUrl: './current-drivers.component.html',
  styleUrls: ['./current-drivers.component.scss'],
})
export class CurrentDriversComponent implements OnInit {
  private _driverFilter: DriverFilter;

  public isLoading;
  public currentDrivers: Driver[] = [];
  @Input() public selectedDrivers: { [key: string]: boolean } = {};
  @Input() isMultiSelect: boolean;
  @Input() tripId: string;
  @Input() isModal = false;
  @Input() set driverFilter(newValue: DriverFilter) {
    this._driverFilter = newValue;
    this.loadCurrentDrivers();
  }
  get driverFilter() {
    return this._driverFilter;
  }

  public currentClientsSubscription: Subscription;

  constructor(
    private driverService: DriverService,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute
  ) {
    this.loadCurrentDrivers();
  }

  ngOnInit() {}

  private async loadCurrentDrivers(): Promise<void> {
    this.isLoading = true;
    if (!!this.currentClientsSubscription) {
      this.currentClientsSubscription.unsubscribe();
    }
    this.currentClientsSubscription = this.driverService
      .getCurrentDrivers(this.driverFilter)
      .subscribe(
        data => {
          this.currentDrivers = data.docs.map(doc => {
            const driver = doc.data() as Driver;
            driver.uid = doc.id;
            return driver;
          });
          this.isLoading = false;
        },
        error => {
          console.error(error);
          this.isLoading = false;
        }
      );
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async selectDriver(driver: Driver): Promise<void> {
    if (!this.isModal) {
      await this.navController.navigateForward(['/driver/' + driver.uid], {
        relativeTo: this.route,
      });
    } else if (!this.isMultiSelect) {
      if (!!this.tripId) {
        driver.currentTripId = this.tripId;
      }
      await this.modalController.dismiss(driver);
    }
  }

  public async openFilterDriverModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterDriverPage,
      componentProps: {
        value: this.driverFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.driverFilter = modalOutput.data;
    });
  }

  public async selectMultiDrivers(): Promise<void> {
    await this.modalController.dismiss(this.selectedDrivers);
  }

  public selectAllCurrentDrivers(isAllSelected: boolean): void {
    this.currentDrivers.forEach(driver => {
      this.selectedDrivers[driver.uid] = isAllSelected;
    });
  }

  public getSelectedCount() {
    return Object.keys(this.selectedDrivers).filter(driverUID => {
      return !!this.selectedDrivers[driverUID];
    }).length;
  }
}
