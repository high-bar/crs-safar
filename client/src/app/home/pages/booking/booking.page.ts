import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CarTypeItem } from '@app/home/models/car-type-item.model';
import { BookingService } from '@app/home/services/booking/booking.service';
import { AutocompletePrediction } from '@app/home/services/google-maps/auto-complete-prediction.model';
import { GoogleMapsService } from '@app/home/services/google-maps/google-maps.service';
import { UtilService } from '@app/home/services/util/util.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { FormBuilderHelper } from '@common/classes/form-builder-helper.class';
import { CarType } from '@common/enums/car-type.enum';
import {
  Car,
  Client,
  DistanceMatrix,
  Place,
  SystemFlags,
  Trip,
} from '@common/models';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { addHours, addYears, format, setHours, setMinutes } from 'date-fns';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage {
  public readonly dateFormat = 'YYYY-MM-DD';
  public readonly timeDisplayFormat = 'hh:mm A';
  public readonly timeFormat = 'HH:mm';
  public readonly maxPickupDate: string = format(
    addYears(new Date(), 1),
    this.dateFormat
  );
  public isLoading: boolean;
  public carTypes: CarTypeItem[] = [
    {
      type: CarType.SUV,
      icon: 'im icon-car-suv',
    },
    {
      type: CarType.SEDAN,
      icon: 'im icon-car-sedan',
    },
    {
      type: CarType.LUXURY,
      icon: 'im icon-car-luxury',
    },
  ];
  public bookingForm: FormGroup;

  public pickupPlace: AutocompletePrediction;
  public dropPlace: AutocompletePrediction;
  public systemFlags: SystemFlags;
  public triedSubmit = false;
  public minBookDateTime: Date = new Date();
  public isLoadingPriceEstimation: boolean;
  public distanceMatrix: DistanceMatrix;
  public getCarsSubscription: Subscription;
  public cars: Partial<Car>[] = [];
  public activePromo: any;

  constructor(
    private formBuilder: FormBuilder,
    private bookingService: BookingService,
    private utilService: UtilService,
    private translateService: TranslateService,
    private authService: AuthService,
    private navController: NavController,
    private googleMapsService: GoogleMapsService
  ) {
    const user: Client = JSON.parse(this.authService.getSavedUser());
    this.activePromo = user.activePromo;

    this.loadSystemFlags();
    const today = setMinutes(addHours(new Date(), 1), 0);
    this.bookingForm = this.formBuilder.group({
      carType: [CarType.SEDAN, Validators.required],
      pickupDate: [
        format(today, this.dateFormat),
        [
          FormBuilderHelper.minDateValue(
            setMinutes(setHours(this.minBookDateTime, 0), 0).getTime()
          ),
        ],
      ],
      pickupTime: [
        format(today, this.timeFormat),
        [
          FormBuilderHelper.minTimeValue(
            this.minBookDateTime.getTime(),
            'pickupDate'
          ),
        ],
      ],
      pickupPlace: [undefined, [Validators.required]],
      dropPlace: [undefined, [Validators.required]],
      isTwoWay: [false, [Validators.required]],
      stayingPeriodInHours: [6, []],
      paymentMethod: [undefined, [Validators.required]],
      types: [undefined],
    });
    this.loadCarInfo();
  }

  public loadSystemFlags(): void {
    this.bookingService.getSystemFlags().subscribe(response => {
      this.systemFlags = response.data() as SystemFlags;

      this.minBookDateTime = addHours(
        new Date(),
        this.systemFlags.minTimeBeforeOrder || 0
      );
      this.bookingForm.controls.pickupDate.setValidators(
        FormBuilderHelper.minDateValue(
          setMinutes(setHours(this.minBookDateTime, 0), 0).getTime()
        )
      );
      this.bookingForm.controls.pickupTime.setValidators(
        FormBuilderHelper.minTimeValue(
          this.minBookDateTime.getTime(),
          'pickupDate'
        )
      );
    });
  }

  public isSubmittedControllerInvalid(controllerName: string): boolean {
    return (
      this.triedSubmit && this.bookingForm.controls[controllerName].invalid
    );
  }

  public isTripWithValidState(): boolean {
    const pickupPlace: Place = this.bookingForm.value.pickupPlace;
    const dropPlace: Place = this.bookingForm.value.dropPlace;
    if (
      !!pickupPlace &&
      !!pickupPlace.state &&
      !!dropPlace &&
      !!dropPlace.state
    ) {
      pickupPlace.state = pickupPlace.state.toLowerCase();
      dropPlace.state = dropPlace.state.toLowerCase();

      return (
        (pickupPlace.state === 'cairo' && dropPlace.state !== 'cairo') ||
        (dropPlace.state === 'cairo' && pickupPlace.state !== 'cairo')
      );
    } else {
      return false;
    }
  }

  public getTripEstimation(): void {
    this.bookingForm.updateValueAndValidity();
    if (this.isTripWithValidState()) {
      this.isLoadingPriceEstimation = true;
      const pickupPlace: Place = this.bookingForm.value.pickupPlace;
      const dropPlace: Place = this.bookingForm.value.dropPlace;

      this.googleMapsService
        .getDistanceBetweenPlaces(pickupPlace, dropPlace)
        .pipe(
          finalize(() => {
            this.isLoadingPriceEstimation = false;
          })
        )
        .subscribe((distanceMatrix: DistanceMatrix) => {
          this.distanceMatrix = distanceMatrix;
          this.distanceMatrix.distance.value =
            distanceMatrix.distance.value / 1000;
          // also sets it in distanceMatrix.price
          this.getTotalEstimation();
        });
    }
  }

  public getTotalEstimation(
    isTwoWay: boolean = this.bookingForm.get('isTwoWay').value,
    stayingPeriodInHours: number = this.bookingForm.get('stayingPeriodInHours')
      .value,
    distance: number = this.distanceMatrix.distance.value,
    systemFlags: SystemFlags = this.systemFlags
  ): number {
    let price =
      distance *
        (this.bookingForm.value.carType === CarType.SUV
          ? systemFlags.kmPriceSUV
          : systemFlags.kmPrice) || 0;
    if (isTwoWay) {
      price = price * 1.5;
      price += systemFlags.waitingHourPrice * stayingPeriodInHours;
    }

    this.distanceMatrix.price = price;
    return price;
  }

  public async requestRide(): Promise<void> {
    this.bookingForm.updateValueAndValidity();
    this.triedSubmit = true;
    const tripRequest: Partial<Trip> = this.bookingForm.value;
    tripRequest.pickupDateTime = new Date(
      `${format(this.bookingForm.get('pickupDate').value, this.dateFormat)} ${
        this.bookingForm.get('pickupTime').value
      }`
    ).getTime();

    if (
      this.isTripWithValidState() &&
      _.get(this.distanceMatrix, 'distance.value', 0) >=
        this.systemFlags.minTripDistance
    ) {
      if (this.bookingForm.valid) {
        this.isLoading = true;

        delete tripRequest['pickupDate'];
        delete tripRequest['pickupTime'];

        const user: Client = JSON.parse(this.authService.getSavedUser());
        tripRequest.client = {
          fullName: user.fullName,
          uid: user.uid,
          email: user.email,
          rate: user.rate,
          pictureURL: user.pictureURL || '',
          phone: user.phone,
        };

        tripRequest.distanceMatrixEst = this.distanceMatrix;
        if (!!this.activePromo) {
          tripRequest.promo = this.activePromo;
          tripRequest.distanceMatrixEst.price = this.getAppliedPromotion(
            this.getTotalEstimation(
              this.bookingForm.value.isTwoWay,
              this.bookingForm.value.stayingPeriodInHours
            ),
            this.activePromo
          );
        }

        try {
          await this.bookingService.book(tripRequest);
          await this.navController.navigateRoot('home/overview');
        } catch (error) {
          console.error(error);
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        } finally {
          this.isLoading = false;
        }
      }
    }
  }

  public async loadCarInfo(): Promise<void> {
    this.bookingService.getCarInfo().subscribe(
      data => {
        this.cars = data.docs.map(doc => {
          const car = doc.data();
          return car;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
      }
    );
  }

  public getAppliedPromotion(price: number, promo: any): number {
    const percentage = (promo.percentage / 100) * price;
    if (promo.money < percentage) {
      return price - promo.money;
    } else {
      return price - percentage;
    }
  }
}
