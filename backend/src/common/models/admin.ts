import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const AdminSchema = new Schema({
  fullName: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  joiningDate: {
    type: String,
  },
});
