import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SettingsService } from '@app/settings/services/settings-service';
import { User } from '@common/models';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-blocked-users',
  templateUrl: './blocked-users.component.html',
  styleUrls: ['./blocked-users.component.scss'],
})
export class BlockedUsersComponent implements OnInit {
  public currentBlocked: User[] = [];
  public user: User;
  public isLoading: boolean;
  public isUnBlockLoading: boolean;

  constructor(
    private settingsService: SettingsService,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute
  ) {
    this.loadBlockedUsers();
  }

  ngOnInit() {}

  public async unBlockUser(user: User): Promise<void> {
    this.isUnBlockLoading = true;
    try {
      await this.settingsService.unBlockUser(user);
    } catch (error) {
      console.error(error);
    } finally {
      this.isUnBlockLoading = false;
    }
  }

  private async loadBlockedUsers(): Promise<void> {
    this.isLoading = true;
    this.settingsService.getBlockedUsers().subscribe(
      data => {
        this.currentBlocked = data.docs.map(doc => {
          const blocked = doc.data() as User;
          blocked.uid = doc.id;
          return blocked;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }
  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  // public async selectBlockedUser(user: User): Promise<void> {
  //   await this.navController.navigateForward(['/driver/' + user.uid], {
  //     relativeTo: this.route,
  //   });
  //   await this.modalController.dismiss(user);
  // }
}
