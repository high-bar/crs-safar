import {
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { GoogleMapsService } from '@app/home/services/google-maps/google-maps.service';
import { UtilService } from '@app/home/services/util/util.service';
import { Place } from '@common/models';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ModalController, Platform } from '@ionic/angular';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

import { PickOnMapModalComponent } from '../pick-on-map-modal/pick-on-map-modal.component';

@Component({
  selector: 'app-place-search-select',
  templateUrl: './place-search-select.component.html',
  styleUrls: ['./place-search-select.component.scss'],
})
export class PlaceSearchSelectComponent implements OnDestroy {
  private _value: Partial<Place>;
  private backButtonSubscription: Subscription;

  @Input()
  set value(newValue: Partial<Place>) {
    if (!!!newValue) {
      this._value = {
        description: '',
      };
    } else {
      this._value = newValue;
    }
  }
  get value() {
    return this._value;
  }

  @Output() public valueChange: EventEmitter<Partial<Place>> = new EventEmitter<
    Partial<Place>
  >();

  public isLoading = false;
  public isTypingAhead = false;
  @HostBinding('class') get hostClasses(): string {
    return [
      'place-search-select',
      this.isTypingAhead ? 'place-search-select--active' : '',
    ].join(' ');
  }

  public searchResults: Partial<Place>[] = [];

  public timeoutHandler;

  constructor(
    private elementRef: ElementRef,
    private googleMapsService: GoogleMapsService,
    private modalController: ModalController,
    private keyboard: Keyboard,
    private utilService: UtilService
  ) {
    this.keyboard.onKeyboardWillHide().subscribe(() => {
      if (this.isTypingAhead) {
        const el = document.querySelector(':focus');
        if (el) {
          el['blur']();
        }
      }
      this.onSearchBlur();
    });
  }

  ngOnDestroy() {
    if (this.backButtonSubscription) {
      this.backButtonSubscription.unsubscribe();
    }
  }

  private async getPlaceState(
    place: google.maps.places.PlaceResult
  ): Promise<string> {
    return ((await this.utilService.getCountryData()).data()
      .states as string[]).find(state => {
      return !!place.address_components.find(address_component => {
        return address_component.long_name.indexOf(state) >= 0;
      });
    });
  }

  public onSearchFocus(): void {
    this.isTypingAhead = true;

    this.keyboard.onKeyboardShow().subscribe(() => {
      this.elementRef.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    });
  }

  public onSearchBlur(): void {
    // needs this delay to allow for openPickOnMapModal event to trigger
    setTimeout(() => {
      this.isTypingAhead = false;
      this.searchResults = [];
    }, 200);
  }

  public searchPlaces(searchValue: string): void {
    // fallback for old devices where keyboard plugin might misbehave
    this.isTypingAhead = true;
    if (searchValue.length >= 3) {
      this.isLoading = true;
      this.googleMapsService.searchPlaces(searchValue).subscribe(
        places => {
          this.searchResults = places.filter(place => {
            return !!place.place_id;
          });
        },
        () => {},
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.selectPlace(undefined);
    }
  }

  public selectPlace(newPlace: Place): void {
    this.value = newPlace;
    this.valueChange.emit(this.value);
    if (!!newPlace) {
      this.value = {
        place_id: newPlace.place_id,
        name: newPlace.description,
        description: newPlace.description,
      };
      this.valueChange.emit(this.value);
      this.googleMapsService
        .getPlace(newPlace.place_id)
        .subscribe(async newPlaceDetails => {
          const updatedPlace: Partial<Place> = {
            ...this.value,
            ...newPlaceDetails.geometry.location,
            state: await this.getPlaceState(newPlaceDetails),
          };
          updatedPlace.name = newPlaceDetails.name;
          updatedPlace.country = !!updatedPlace.state ? 'egypt' : undefined;
          this.valueChange.emit(updatedPlace);
        });
    }
  }

  public async openPickOnMapModal(): Promise<void> {
    const pickOnMapModal = await this.modalController.create({
      component: PickOnMapModalComponent,
      cssClass: 'modal--no-backdrop',
      componentProps: {
        searchPlace: this.value,
      },
    });
    pickOnMapModal.onWillDismiss().then(modalDismissEvent => {
      this.selectPlace(_.get(modalDismissEvent, 'data', this.value));
    });
    await pickOnMapModal.present();
  }
}
