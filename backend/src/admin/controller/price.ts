import * as mongoose from 'mongoose';
import { PricesSchema } from '../../common/models/prices';
import { Request, Response } from 'express';

const Prices = mongoose.model('Prices', PricesSchema);

export class PricesController {
  public getPrices(req: Request, res: Response) {
    // find user
    Prices.find({}, function(err: any, prices: any) {
      if (err) {
        res.send(err);
      } else {
        res.json(prices);
      }
    }); // end find all prices
  }
  public updatePrices(req: Request, res: Response) {
    //set values
    // let newPrices = new Prices({
    //   from: req.body.from,
    //   to: req.body.to,
    //   price: req.body.price
    // });
    // //update price
    Prices.findOneAndUpdate(
      { '_id': req.body.id },
      { price: req.body.price },
      function(err, price) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json(price);
        }
      }
    );
  }
  //
  public newPrices(req: Request, res: Response) {
    //set values
    let newPrices = new Prices({
      from: req.body.from,
      to: req.body.to,
      price: req.body.price,
    });
    //
    newPrices.save((err, Price) => {
      if (err) {
        res.status(401).json({
          error: err,
        });
      } else {
        res.json({
          message: 'Price Added!',
          price: Price,
        });
      } //end else
    }); //end save Prices
    //
  }
  //


  public deletePrices(req: Request, res: Response) {
    let priceid = req.body.id;
    Prices.deleteOne(
      { _id: priceid },
      function(err:any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json("Price Deleted!");
        }
      }
    );
  }
  //

  public replace1(req: Request, res: Response) {
    let old = req.body.old;
    let snew = req.body.new;
    Prices.update(
      { from: old },
      {$set :{"from" :snew}},
      {upsert: true},
      function(err:any) {
        if (err) {
          res.status(401).json({
            error: err,
          });
        } else {
          res.json("Price Updated!");
        }
      }
    );
  }
  //
    public replace2(req: Request, res: Response) {
      let old = req.body.old;
      let snew = req.body.new;
      Prices.update(
        { to: old },
        {$set :{"to" :snew}},
        {upsert: true},
        function(err:any) {
          if (err) {
            res.status(401).json({
              error: err,
            });
          } else {
            res.json("Price Updated!");
          }
        }
      );
    }
    //


}
