import { TransactionType } from '@common/enums';

export interface Transaction {
  id: string;
  amount: number;
  dateTime: number;
  type: TransactionType;
  method: string;
}
