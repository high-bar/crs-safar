import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComplainsService } from '@app/shared/services/complains/complains.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-complains',
  templateUrl: './complains.page.html',
  styleUrls: ['./complains.page.scss'],
})
export class ComplainsPage implements OnInit {
  public addComplainForm: FormGroup = this.formBuilder.group({
    subject: [undefined, [Validators.required]],
    note: [undefined, [Validators.required]],
  });
  isLoading: boolean;
  isComplainSent: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private complainsService: ComplainsService
  ) {}

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitComplains(): Promise<void> {
    if (this.addComplainForm.valid) {
      this.isLoading = true;
      try {
        await this.complainsService.addNewComplain(this.addComplainForm.value);
        this.isComplainSent = true;
      } catch (error) {
        console.log(error);
      } finally {
        this.isLoading = false;
      }
    }
  }
}
