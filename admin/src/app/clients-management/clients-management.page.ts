import { Component, OnInit } from '@angular/core';
import { Client, ClientFilter } from '@common/models';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { FilterClientPage } from './pages/filter-client/filter-client/filter-client.page';
import { ClientService } from './services/client-service/client.service';

@Component({
  selector: 'app-clients-management',
  templateUrl: './clients-management.page.html',
  styleUrls: ['./clients-management.page.scss'],
})
export class ClientsManagementPage implements OnInit {
  private backButtonSubscription: Subscription;
  private clientFilter: ClientFilter;
  public isLoading;
  public currentClients: Client[] = [];

  public client = {
    pictureURL: '/assets/profile-picture-sample.jpg',
    fullName: 'Ahmed Ibrahim',
    rate: 4.2,
  };
  public arr = new Array(5).fill(1);
  users: any;
  public isCurrentLoading: boolean;
  public currentClientsSubscription: Subscription;

  constructor(
    private clientService: ClientService,
    private modalController: ModalController,
    private platform: Platform,
    private navController: NavController
  ) {
    this.loadCurrentClients();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  private async loadCurrentClients(): Promise<void> {
    this.isCurrentLoading = true;
    if (!!this.currentClientsSubscription) {
      this.currentClientsSubscription.unsubscribe();
    }
    this.currentClientsSubscription = this.clientService
      .getCurrentClient(this.clientFilter)
      .subscribe(
        data => {
          this.currentClients = data.docs.map(doc => {
            const client = doc.data() as Client;
            client.uid = doc.id;
            return client;
          });
          this.isCurrentLoading = false;
        },
        error => {
          console.error(error);
          this.isCurrentLoading = false;
        }
      );
  }
  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async openFilterClientModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterClientPage,
      componentProps: {
        value: this.clientFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.clientFilter = modalOutput.data;
      this.loadCurrentClients();
    });
  }
}
