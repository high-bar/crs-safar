import { Client } from '@common/models';
import { Driver } from '@common/models';

export interface TripFilter {
  driver: Driver;
  client: Client;
  fromPrice: number;
  toPrice: number;
  fromState: string;
  toState: string;
  fromRequestDateTime: number;
  toRequestDateTime: number;
}
