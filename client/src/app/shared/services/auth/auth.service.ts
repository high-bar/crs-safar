import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Client, GoogleLoginResponse, UserFacebookData } from '@common/models';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NavController } from '@ionic/angular';
import { auth, firestore, functions } from 'firebase';
import { Observable, Observer } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  private authChangeObserver: Observer<any>;
  public change: Observable<any>;

  constructor(
    private router: Router,
    private splashScreen: SplashScreen,
    private navController: NavController,
    private googlePlus: GooglePlus,
    private facebook: Facebook
  ) {
    let user: any = this.getSavedUser();
    if (!!user) {
      user = JSON.parse(user);
      this.onUserUpdate(user.uid);
    }

    this.change = Observable.create((observer: Observer<any>) => {
      this.authChangeObserver = observer;
      this.authChangeObserver.next(user);
    }).pipe(shareReplay());
    this.change.subscribe();

    auth().onAuthStateChanged(authUser => {
      user = undefined;
      if (!!authUser) {
        user = JSON.parse(this.getSavedUser());
      }
      this.authChangeObserver.next(user);
    });
  }

  private saveUser(user): void {
    if (!!user) {
      localStorage.setItem('user', JSON.stringify(user));
      this.authChangeObserver.next(user);
    }
  }

  private async userSignupHook(userData: Partial<Client>): Promise<void> {
    delete userData.password;
    delete userData['passwordConfirm'];
    delete userData['emailConfirm'];

    await this.addNewUser(userData);
    this.saveUser((await this.getUserOnce(userData.uid)).data());
    this.onUserUpdate(userData.uid);
  }

  public getSavedUser(): string {
    return localStorage.getItem('user');
  }

  public getUserOnce(uid: string): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('users')
      .doc(uid)
      .get();
  }

  private onUserUpdate(uid: string): void {
    firestore()
      .collection('users')
      .doc(uid)
      .onSnapshot(querySnapshot => {
        if (querySnapshot.exists) {
          this.saveUser(querySnapshot.data());
        }
      });
  }

  private addNewUser(user: Partial<Client>): Promise<void> {
    user.joiningDate = new Date().getTime();

    return firestore()
      .collection('users')
      .doc(user.uid)
      .set(user);
  }

  public canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!!!this.getSavedUser()) {
      this.router.navigate(['home/login'], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
    return true;
  }

  public signUp(userData: Partial<Client>): Promise<any> {
    return auth()
      .createUserWithEmailAndPassword(userData.email, userData.password)
      .then(async signUpData => {
        userData.uid = signUpData.user.uid;
        userData.provider = 'password';
        this.userSignupHook(userData);
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public login(loginReq: Partial<Client>): Promise<any> {
    return auth()
      .signInWithEmailAndPassword(loginReq.email, loginReq.password)
      .then(async loginData => {
        try {
          this.saveUser((await this.getUserOnce(loginData.user.uid)).data());
          this.onUserUpdate(loginData.user.uid);
        } catch (error) {
          console.error(error);
          throw error;
        }
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public firebaseGoogleSignup(
    googleLoginResponse: GoogleLoginResponse,
    userData: Partial<Client>
  ): Promise<any> {
    return auth()
      .signInWithCredential(
        auth.GoogleAuthProvider.credential(googleLoginResponse.idToken)
      )
      .then(async signUpData => {
        userData.uid = signUpData.user.uid;
        userData.provider = 'google';
        localStorage.removeItem('firebaseGoogleSignup');
        this.userSignupHook(userData);
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public firebaseFacebookSignup(
    userFacebookData: UserFacebookData,
    userData: Partial<Client>
  ): Promise<any> {
    return auth()
      .signInWithCredential(
        auth.FacebookAuthProvider.credential(userFacebookData.accessToken)
      )
      .then(async signUpData => {
        userData.uid = signUpData.user.uid;
        userData.provider = 'facebook';
        localStorage.removeItem('userFacebookData');
        this.userSignupHook(userData);
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public async firebaseGoogleLogin(
    googleLoginResponse: GoogleLoginResponse
  ): Promise<any> {
    return auth()
      .signInWithCredential(
        auth.GoogleAuthProvider.credential(googleLoginResponse.idToken)
      )
      .then(async loginData => {
        try {
          this.saveUser((await this.getUserOnce(loginData.user.uid)).data());
          this.onUserUpdate(loginData.user.uid);
        } catch (error) {
          console.error(error);
          throw error;
        }
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public async firebaseFacebookLogin(
    userFacebookData: UserFacebookData
  ): Promise<any> {
    return auth()
      .signInWithCredential(
        auth.FacebookAuthProvider.credential(userFacebookData.accessToken)
      )
      .then(async loginData => {
        try {
          this.saveUser((await this.getUserOnce(loginData.user.uid)).data());
          this.onUserUpdate(loginData.user.uid);
        } catch (error) {
          console.error(error);
          throw error;
        }
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  public async logout(): Promise<void> {
    return auth()
      .signOut()
      .then(async () => {
        this.splashScreen.show();
        try {
          await this.googlePlus.logout();
        } catch (error) {}
        try {
          await this.facebook.logout();
        } catch (error) {}
        await auth().signOut();
        localStorage.clear();
        await this.navController.navigateRoot('home');
        this.splashScreen.hide();
      })
      .catch(_error => {
        // An error happened.
      });
  }

  public resetPassword(email: string): Promise<void> {
    return auth().sendPasswordResetEmail(email);
  }

  public editProfile(profile?: Partial<Client>): Promise<void> {
    return firestore()
      .collection('users')
      .doc(profile.uid)
      .update(profile);
  }

  public async googleLogin(): Promise<any> {
    try {
      await this.googlePlus.logout();
    } catch (error) {}

    return this.googlePlus
      .login({
        webClientId:
          '648380568339-eeopt53bv03dfh6pm7ksctqehrjnpoe7.apps.googleusercontent.com',
      })
      .then(async (googleLoginResponse: GoogleLoginResponse) => {
        const emailProvider = (await this.isEmailUsed(
          googleLoginResponse.email
        )).provider;
        if (emailProvider === 'google') {
          try {
            return new Promise(async (resolve, _reject) => {
              await this.firebaseGoogleLogin(googleLoginResponse);
              resolve(true);
            });
          } catch (error) {
            throw error;
          }
        } else if (!!!emailProvider) {
          localStorage.setItem(
            'googleLoginResponse',
            JSON.stringify(googleLoginResponse)
          );
          await this.navController.navigateRoot('home/sign-up');
        } else {
          // tslint:disable-next-line: no-string-throw
          throw 'not-google-email';
        }
      })
      .catch(error => {
        throw error;
      });
  }

  public async facebookLogin(): Promise<any> {
    try {
      await this.facebook.logout();
    } catch (error) {}

    try {
      const facebookLoginResponse: FacebookLoginResponse = await this.facebook.login(
        ['public_profile', 'email']
      );
      const userFacebookData: UserFacebookData | any = await this.facebook.api(
        'me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)',
        []
      );
      userFacebookData.displayName = userFacebookData.name;
      userFacebookData.accessToken =
        facebookLoginResponse.authResponse.accessToken;
      userFacebookData.expires_in =
        facebookLoginResponse.authResponse.expiresIn;
      userFacebookData.userId = facebookLoginResponse.authResponse.userID;
      userFacebookData.imageUrl = userFacebookData.picture_large.data.url;

      const emailProvider = (await this.isEmailUsed(userFacebookData.email))
        .provider;
      if (emailProvider === 'facebook') {
        try {
          return new Promise(async (resolve, _reject) => {
            await this.firebaseFacebookLogin(userFacebookData);
            resolve(true);
          });
        } catch (error) {
          throw error;
        }
      } else if (!!!emailProvider) {
        localStorage.setItem(
          'userFacebookData',
          JSON.stringify(userFacebookData)
        );
        await this.navController.navigateRoot('home/sign-up');
      } else {
        // tslint:disable-next-line: no-string-throw
        throw 'not-facebook-email';
      }
      console.log(userFacebookData);
    } catch (error) {
    } finally {
    }
  }

  public async isEmailUsed(email: string): Promise<any> {
    const isEmailUsedFunction = functions().httpsCallable('isEmailUsed');
    return (await isEmailUsedFunction({ email: email })).data;
  }
}
