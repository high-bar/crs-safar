import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-add-card-modal',
  templateUrl: './add-card-modal.component.html',
  styleUrls: ['./add-card-modal.component.scss'],
})
export class AddCardModalComponent implements OnInit {
  constructor(
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalCtrl.dismiss();
  }
}
