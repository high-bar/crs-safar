import * as mongoose from 'mongoose';
import { CarStatus, CarType } from '../../../../common/enums';

const Schema = mongoose.Schema;

export const CarSchema = new Schema( {
  plateNumber: {
    type:String
  },
  type: {
    type:CarType
  },
  model: {
    type:String
  },
  brand: {
    type:String
  },
  year: {
    type:Number
  },
  status: {
    type:CarStatus
  },
  notes: {
    type:String
  },
  driver: {
     type:Schema.Types.ObjectId, ref:'Driver'
  },

});
