import { AdminSignUpController } from '../controller/add';
import { AdminSignInController } from '../controller/login';
import { ControllUsers } from '../controller/controll-users';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;
export class AdminRoutes {
  public AdminSignUpController: AdminSignUpController = new AdminSignUpController();
  public AdminSignInController: AdminSignInController = new AdminSignInController();
  public ControllUsers: ControllUsers = new ControllUsers();

  public routes(app: any): void {
    // start client route
    // Create a new client
    app.route('/admin/signup').post(this.AdminSignUpController.addAdmin);
    ////
    // client Login
    app.route('/admin/signin').post(this.AdminSignInController.getAdmin);
    ////
    // end client route
    app.route('/admin/getalladmins').post(this.ControllUsers.Admins);
    app.route('/admin/getalldrivers').post(this.ControllUsers.Drivers);
    app.route('/admin/getallusers').post(this.ControllUsers.Users);
    ////
    // get drivers 1
    app
      .route('/admin/getdrivers1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.Drivers1.bind(this.ControllUsers));
    ////
    // get drivers 2
    app
      .route('/admin/getdrivers2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.Drivers2.bind(this.ControllUsers));
    ////
    ////
    // get Users 1
    app
      .route('/admin/getusers1')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.Users1.bind(this.ControllUsers));
    ////
    // get Users 2
    app
      .route('/admin/getusers2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.User2.bind(this.ControllUsers));
    ////
    /////////////////////////
    ////
    // delete driver
    app
      .route('/admin/deletedriver')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.DeleteDriver.bind(this.ControllUsers));
    ////
    /////////////////////////
    ////
    // delete user
    app
      .route('/admin/deleteuser')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.DeleteUser.bind(this.ControllUsers));
    ////
    /////////////////////////
    // Approve driver
    app
      .route('/admin/approvedriver')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: 'Failed to authenticate',
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.ControllUsers.ApproveDriver.bind(this.ControllUsers));
    ////
    /////////////////////////
  }

  //end class
}
