import { Injectable } from '@angular/core';
import { DriverTripStatus, TripStatus } from '@common/enums';
import { Trip, TripFilter } from '@common/models';
import { firestore } from 'firebase';
import * as _ from 'lodash';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TripsService {
  constructor() {}

  public getTrips(
    tripStatus: TripStatus,
    tripFilter?: TripFilter
  ): Observable<{ snapshot: firestore.QuerySnapshot; isFiltered: boolean }> {
    let query: firestore.Query = firestore()
      .collection('trips')
      .where('status', '==', tripStatus);
    let isFiltered = false;
    if (!!tripFilter) {
      if (!!tripFilter.client) {
        query = query.where('client.uid', '==', tripFilter.client.uid);
      }

      if (!!tripFilter.driver) {
        query = query.where('driver.uid', '==', tripFilter.driver.uid);
        isFiltered = true;
      }
      if (!!tripFilter.fromState) {
        query = query.where('pickupPlace.state', '==', tripFilter.fromState);
        isFiltered = true;
      }
      if (!!tripFilter.toState) {
        query = query.where('dropPlace.state', '==', tripFilter.toState);
        isFiltered = true;
      }
      if (!!tripFilter.toRequestDateTime) {
        // query = query.where('dropPlace.state', '==', tripFilter.toState);
        // isFiltered = true;
      }
      if (!!tripFilter.fromRequestDateTime) {
        // query = query.where('dropPlace.state', '==', tripFilter.toState);
        // isFiltered = true;
      }
    }
    return Observable.create(
      (
        observer: Observer<{
          snapshot: Partial<firestore.QuerySnapshot>;
          isFiltered: boolean;
        }>
      ) => {
        query.orderBy('requestDateTime', 'asc').onSnapshot(
          querySnapshot => {
            observer.next({
              isFiltered: isFiltered,
              snapshot: {
                docs: querySnapshot.docs,
              },
            });
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      }
    );
  }

  public getPastTrips(
    tripFilter?: Partial<TripFilter>
  ): Observable<firestore.QuerySnapshot> {
    let query: firestore.Query = firestore()
      .collection('trips')
      .where('status', '==', TripStatus.PAST);
    if (!!tripFilter) {
      if (!!tripFilter.client) {
        query = query.where('client.uid', '==', tripFilter.client.uid);
      }

      if (!!tripFilter.driver) {
        query = query.where('driver.uid', '==', tripFilter.driver.uid);
      }
      if (!!tripFilter.fromState) {
        query = query.where('pickupPlace.state', '==', tripFilter.fromState);
      }
      if (!!tripFilter.toState) {
        query = query.where('dropPlace.state', '==', tripFilter.toState);
      }
      if (!!tripFilter.fromPrice) {
        query = query.where('pricing.base', '>=', tripFilter.fromPrice);
      }
      if (!!tripFilter.toPrice) {
        query = query.where('pricing.base', '<=', tripFilter.toPrice);
      }

      // if (!!tripFilter.fromPrice || !!tripFilter.toPrice) {
      //   query = query.orderBy('pricing.base', 'desc');
      // }
    }
    return Observable.create(
      (observer: Observer<Partial<firestore.QuerySnapshot>>) => {
        query.orderBy('conclusionDateTime', 'desc').onSnapshot(
          querySnapshot => {
            const fromDateTime = _.get(tripFilter, 'conclusionDateTime', 0);
            const toDateTime =
              !!tripFilter && !!tripFilter.toRequestDateTime
                ? tripFilter.toRequestDateTime
                : Number.MAX_SAFE_INTEGER;

            observer.next({
              docs: querySnapshot.docs.filter(doc => {
                const trip: Trip = doc.data() as Trip;
                return (
                  trip.conclusionDateTime >= fromDateTime &&
                  trip.conclusionDateTime <= toDateTime
                );
              }),
            });
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      }
    );
  }

  public getReturningTrips(): Observable<firestore.QuerySnapshot> {
    return Observable.create(
      (observer: Observer<Partial<firestore.QuerySnapshot>>) => {
        firestore()
          .collection('trips')
          .where('status', '==', TripStatus.PAST)
          .where('driverStatus', '==', DriverTripStatus.RETURNING)
          .orderBy('conclusionDateTime', 'desc')
          .onSnapshot(
            querySnapshot => {
              observer.next(querySnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }

  public getTripByID(tripId): Promise<firestore.DocumentSnapshot> {
    return firestore()
      .collection('trips')
      .doc(tripId)
      .get();
  }

  public updateTripInfo(newTrip: Partial<Trip>): Promise<void> {
    return firestore()
      .collection('trips')
      .doc(newTrip.id)
      .update(newTrip);
  }

  public deleteSelectedTrips(tripId: string): Promise<void> {
    return firestore()
      .collection('trips')
      .doc(tripId)
      .update({ status: TripStatus.ARCHIVED });
  }
}
