import { CarStatus, CarType } from '@common/enums';

export interface CarFilter {
  brand: string;
  model: string;
  color: string;
  plateNumber: string;
  type: CarType;
  status: CarStatus;
}
