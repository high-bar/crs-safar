import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TripSchema = new Schema( {
  FromPlaceId: {
    type:String
  },
  ToPlaceId: {
    type:String
  },
  goingDateTime: {
    type:Date
  },
  goingEstimatedDurationInMinutes: {
    type:String
  },
  goingEstimatedDistance: {
    type:String
  },
  priceInEGP: {
    type:Number
  },
  client: {
     type:Schema.Types.ObjectId, ref:'User'
  },
  driver: {
    type:Schema.Types.ObjectId, ref:'Driver'
  },
  driverStatus: {
    type:String
  },
  driverLocation: {
    type:String
  },
  clientReview: {
    type:Number, ref:'User'
  },
  driverReview: {
   type:Number, ref:'Driver'
  },
  car: {
    type:Schema.Types.ObjectId, ref:'Car'
   },
   status: {
    type:String
   },
});
