import { DriverStatus } from '@common/enums';
import { Garage, User } from '@common/models';

export interface Driver extends User {
  address: string;
  isApproved: boolean;
  status: DriverStatus;
  garage?: Partial<Garage>;
}
