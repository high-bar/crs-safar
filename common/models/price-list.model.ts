export interface PriceList {
  [key: string]: {
    toDriverSalary: number;
    fromDriverSalary: number;
  };
}
