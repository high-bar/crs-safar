import { CONFIGS } from './configs.const';
import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  ROOT_API_URL: 'https://safarprivatetaxi.com',
  configs: CONFIGS,
};
