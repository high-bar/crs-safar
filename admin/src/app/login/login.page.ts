import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { UtilService } from '@app/shared/services/util/util.service';
import { Toast } from '@ionic-native/toast/ngx';
import { NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  private backButtonSubscription: Subscription;
  private lastTimeBackPress = 0;
  private exitBackTimeFrame = 2000;

  public loginForm: FormGroup = this.formBuilder.group({
    email: [undefined, [Validators.required, Validators.email]],
    password: [undefined, [Validators.required]],
  });

  public isLoading: boolean;

  constructor(
    private platform: Platform,
    private toast: Toast,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private translateService: TranslateService,
    private utilService: UtilService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController
  ) {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        if (
          new Date().getTime() - this.lastTimeBackPress <
          this.exitBackTimeFrame
        ) {
          navigator['app'].exitApp();
        } else {
          this.toast
            .show(`Press back again to exit`, '2000', 'bottom')
            .subscribe();

          this.lastTimeBackPress = new Date().getTime();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async submitLogin() {
    if (this.loginForm.valid) {
      this.isLoading = true;

      try {
        await this.authService.login(this.loginForm.value);
        const returnUrl =
          this.activatedRoute.snapshot.queryParams['returnUrl'] || 'home';
        await this.navController.navigateRoot(returnUrl);
      } catch (error) {
        if (
          error.code === 'auth/user-not-found' ||
          error.code === 'auth/wrong-password'
        ) {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.${error.code}`)
          );
        } else {
          await this.utilService.errorAlert(
            this.translateService.instant(`errors.server`)
          );
        }
      } finally {
        this.isLoading = false;
      }
    }
  }
}
