import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverProfilePage } from './driver-profile.page';

describe('DriverProfilePage', () => {
  let component: DriverProfilePage;
  let fixture: ComponentFixture<DriverProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverProfilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
