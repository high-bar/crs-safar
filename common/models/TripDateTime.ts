export interface TripDateTime {
  dateTime: Date;
  estimatedDurationInMinutes?: number;
}
