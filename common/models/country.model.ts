import { CountryState } from '@common/models';

export interface Country {
  code: string;
  name: string;
  states?: CountryState[];
}
