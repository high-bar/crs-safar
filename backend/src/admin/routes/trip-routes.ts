import { TripController } from '../controller/trip';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class TripRoutes {
  public TripController: TripController = new TripController();

  public routes(app: any): void {
    // start admin trip
    // AssignTrip
    app
      .route('/admin/assigntrip')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.AssignTrip.bind(this.TripController));
    ////
    // deleteTrip
    app
      .route('/admin/deletetrip')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.DeleteTrip.bind(this.TripController));
    ////

    //////////////
    // deleteTrip
    // app
    //   .route('/admin/deletetrip')
    //   .post((req: Request, res: Response, next: NextFunction) => {
    //     // middleware
    //     let token = req.headers['x-access-token'];
    //     if (!token) {
    //       res.status(403).send('No Token Provided');
    //     } else {
    //       /////////
    //       jwt.verify(token, secretKey, function(err: any, decoded: any) {
    //         if (err) {
    //           res
    //             .status(403)
    //             .send({ success: false, message: 'Failed to authenticate' });
    //         } else {
    //           //
    //           (<any>req).decoded = decoded;

    //           next();
    //         }
    //       });
    //       ///////////
    //     }
    //   }, this.TripController.DeleteTrip.bind(this.TripController));
    ////
    //////////////
    app
      .route('/admin/gettrips1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetTrips1.bind(this.TripController));
    //////////////
    //////////////
    app
      .route('/admin/gettrips2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetTrips2.bind(this.TripController));
    //////////////

    //////////////get active trips
    app
      .route('/admin/getactivetrips1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetActiveTrips1.bind(this.TripController));
    //////////////
    app
      .route('/admin/getactivetrips2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetActiveTrips2.bind(this.TripController));
    //////////////

    //////////////get Pending trips
    app
      .route('/admin/getpendingtrips1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetPendingTrips1.bind(this.TripController));
    //////////////
    app
      .route('/admin/getpendingtrips2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetPendingTrips2.bind(this.TripController));
    //////////////
    //////////////get next trips
    app
      .route('/admin/getnexttrips1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetNextTrips1.bind(this.TripController));
    //////////////
    app
      .route('/admin/getnexttrips2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetNextTrips2.bind(this.TripController));
    //////////////
    ////////////// Get trip details
    app
      .route('/admin/tripdetails')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.adminTripDetails.bind(this.TripController));
    //////////////

    //////////////get next trips
    app
      .route('/admin/gettripsdriverstatus1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetTripsDriverStatus1.bind(this.TripController));
    //////////////
    app
      .route('/admin/gettripsdriverstatus2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.TripController.GetTripsDriverStatus2.bind(this.TripController));
    //////////////
  }
  //end class
}
