import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrentDriversComponent } from '@app/drivers-management/components/current-drivers/current-drivers.component';
import { CurrentClientsComponent } from '@app/shared/components/current-clients/current-clients.component';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { NotificationService } from './services/notification.service';

@Component({
  selector: 'app-send-notifications',
  templateUrl: './send-notifications.page.html',
  styleUrls: ['./send-notifications.page.scss'],
})
export class SendNotificationsPage implements OnInit {
  private backButtonSubscription: Subscription;
  public notificationForm: FormGroup = this.formBuilder.group({
    title: [undefined, [Validators.required]],
    message: [undefined, [Validators.required]],
    clients: [undefined],
    drivers: [undefined],
  });
  public message = '';
  public isLoading: boolean;
  public selectedClients: { [key: string]: boolean } = {};
  public selectedDrivers: { [key: string]: boolean } = {};
  public isMultiSelect = false;

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private modalController: ModalController,
    private platform: Platform,
    private navController: NavController
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async openCurrentClientsModal(): Promise<void> {
    const selectClientModal = await this.modalController.create({
      component: CurrentClientsComponent,
      componentProps: {
        isMultiSelect: true,
        selectedClients: this.selectedClients,
      },
    });
    await selectClientModal.present();
    const selectedClients = (await selectClientModal.onWillDismiss()).data;
    if (selectedClients) {
      this.selectedClients = selectedClients;
      this.notificationForm.patchValue({
        clients: this.selectedClients,
      });
    }
  }

  public async openCurrentDriversModal(): Promise<void> {
    const selectDriverModal = await this.modalController.create({
      component: CurrentDriversComponent,
      componentProps: {
        isModal: true,
        isMultiSelect: true,
        selectedDrivers: this.selectedDrivers,
      },
    });
    await selectDriverModal.present();
    const selectedDrivers = (await selectDriverModal.onWillDismiss()).data;
    if (selectedDrivers) {
      this.selectedDrivers = selectedDrivers;
      this.notificationForm.patchValue({
        drivers: this.selectedDrivers,
      });
    }
  }

  public async notificationSubmit() {
    if (this.notificationForm.valid) {
      this.isLoading = true;

      Object.keys(this.selectedClients).forEach(clientUID => {
        if (!!this.selectedClients[clientUID]) {
          this.notificationService
            .addNotification(this.notificationForm.value, clientUID)
            .subscribe();
        }
      });

      Object.keys(this.selectedDrivers).forEach(driverUID => {
        if (!!this.selectedDrivers[driverUID]) {
          this.notificationService
            .addNotification(this.notificationForm.value, driverUID)
            .subscribe();
        }
      });
      this.isLoading = false;
      this.message = '';
    }
  }

  public getSelectedClientsCount() {
    return Object.keys(this.selectedClients).filter(clientUID => {
      return !!this.selectedClients[clientUID];
    }).length;
  }

  public getSelectedDriversCount() {
    return Object.keys(this.selectedDrivers).filter(driverUID => {
      return !!this.selectedDrivers[driverUID];
    }).length;
  }
}
