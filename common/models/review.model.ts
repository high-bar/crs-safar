export interface Review {
  scoreOfFive: number;
  comment: string;
  reviewerId: string;
  dateTime: Date;
}
