import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCollectingComponent } from './payment-collecting.component';

describe('PaymentCollectingComponent', () => {
  let component: PaymentCollectingComponent;
  let fixture: ComponentFixture<PaymentCollectingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentCollectingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCollectingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
