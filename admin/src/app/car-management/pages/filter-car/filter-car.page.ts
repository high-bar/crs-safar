import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CarStatus, CarType } from '@common/enums';
import { CarFilter } from '@common/models';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-filter-car',
  templateUrl: './filter-car.page.html',
  styleUrls: ['./filter-car.page.scss'],
})
export class FilterCarPage implements OnInit {
  public readonly CarTypeEnum: typeof CarType = CarType;
  public readonly CarStatusEnum: typeof CarStatus = CarStatus;
  @Input() public value: string;

  public filterCarForm: FormGroup = this.formBuilder.group({
    query: [undefined],
    // model: [undefined],
    // color: [undefined],
    // plateNumber: [undefined],
    // type: [undefined],
    // status: [undefined],
  });

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController
  ) {}

  public isLoading: boolean;

  ngOnInit() {
    if (!!this.value) {
      // pass only values used in the form
      this.filterCarForm.patchValue({
        query: this.value,
        // model: this.value.model,
        // color: this.value.color,
        // plateNumber: this.value.plateNumber,
        // type: this.value.type,
        // status: this.value.status,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitFilterCar(): Promise<void> {
    await this.modalController.dismiss(this.filterCarForm.value.query);
  }
}
