import { Injectable } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { TripStatus } from '@common/enums';
import { Driver, Transaction, Trip } from '@common/models';
import { firestore, Observer } from 'firebase';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TripsService {
  constructor(private authService: AuthService) {}

  public getFutureTrips(): Observable<firestore.QuerySnapshot> {
    const user: Driver = JSON.parse(this.authService.getSavedUser());
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('driver.uid', '==', user.uid)
        .where('status', '==', TripStatus.FUTURE)
        .orderBy('requestDateTime', 'desc')
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getTripById(tripId: string): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('trips')
          .doc(tripId)
          .onSnapshot(
            documentSnapshot => {
              observer.next(documentSnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }

  public updateDriverTripStatus(updatedTrip: Trip): Promise<void> {
    const newTrip: Partial<Trip> = {
      status: updatedTrip.status,
      driverStatus: updatedTrip.driverStatus,
    };
    if (!!_.get(updatedTrip, 'pricing.actualExtra')) {
      newTrip.pricing = {
        actualExtra: updatedTrip.pricing.actualExtra,
        collectedExtra: updatedTrip.pricing.collectedExtra,
      };
    }

    return firestore()
      .collection('trips')
      .doc(updatedTrip.id)
      .update(newTrip);
  }

  public updateTripRoute(updatedTrip: Trip): Promise<void> {
    return firestore()
      .collection('trips')
      .doc(updatedTrip.id)
      .update({
        route: updatedTrip.route,
      });
  }

  public getActiveTrip(): Observable<firestore.QuerySnapshot> {
    const user: Driver = JSON.parse(this.authService.getSavedUser());
    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('driver.uid', '==', user.uid)
        .where('status', '==', TripStatus.CURRENT)
        .orderBy('requestDateTime', 'desc')
        .limit(1)
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getPastTrips(): Observable<firestore.QuerySnapshot> {
    const user: Driver = JSON.parse(this.authService.getSavedUser());

    return Observable.create((observer: Observer<firestore.QuerySnapshot>) => {
      firestore()
        .collection('trips')
        .where('status', '==', TripStatus.PAST)
        .where('driver.uid', '==', user.uid)
        .orderBy('conclusionDateTime', 'desc')
        .onSnapshot(
          querySnapshot => {
            observer.next(querySnapshot);
          },
          error => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  public getActiveTripOnce(): Promise<firestore.QuerySnapshot> {
    const user: Driver = JSON.parse(this.authService.getSavedUser());

    return firestore()
      .collection('trips')
      .where('driver.uid', '==', user.uid)
      .where('status', '==', TripStatus.CURRENT)
      .orderBy('requestDateTime', 'desc')
      .limit(1)
      .get();
  }

  public updateTripDriverRequest(
    tripId: string,
    driver: Partial<Driver>
  ): Promise<void> {
    const tripRef = firestore()
      .collection(`trips`)
      .doc(tripId);

    return firestore().runTransaction(transaction => {
      return transaction.get(tripRef).then(tripDoc => {
        if (!!!(tripDoc.data() as Trip).driver) {
          transaction.update(tripRef, {
            driver: driver,
          });
        } else {
          return Promise.reject('trip/driver-already-assigned');
        }
      });
    });
  }

  public getSystemFlags(): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('util')
          .doc('flags')
          .onSnapshot(
            querySnapshot => {
              observer.next(querySnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }

  public addClientTransaction(
    uid,
    transaction: Partial<Transaction>
  ): Promise<firestore.DocumentReference> {
    return firestore()
      .collection('users')
      .doc(uid)
      .collection('transactions')
      .add(transaction);
  }
}
