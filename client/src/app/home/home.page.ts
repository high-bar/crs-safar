import { Component } from '@angular/core';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { Toast } from '@ionic-native/toast/ngx';
import { NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { TripsService } from './services/trips.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private backButtonSubscription: Subscription;
  private lastTimeBackPress = 0;
  private exitBackTimeFrame = 2000;

  public isAuthorized: boolean;

  constructor(
    private platform: Platform,
    private toast: Toast,
    private authService: AuthService,
    private tripsService: TripsService,
    private navController: NavController
  ) {
    this.authService.change.subscribe(user => {
      this.isAuthorized = !!user;
    });

    this.userHasNonPastTrips();
  }

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        if (
          new Date().getTime() - this.lastTimeBackPress <
          this.exitBackTimeFrame
        ) {
          navigator['app'].exitApp();
        } else {
          this.toast
            .show(`Press back again to exit`, '2000', 'bottom')
            .subscribe();

          this.lastTimeBackPress = new Date().getTime();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  private async userHasNonPastTrips(): Promise<void> {
    this.tripsService.getActiveTrip().subscribe(async data => {
      if (data.size > 0) {
        await this.navController.navigateRoot('/home/overview');
      }
    });
    this.tripsService.getNewTrips().subscribe(async data => {
      if (data.size > 0) {
        await this.navController.navigateRoot('/home/overview');
      }
    });
    this.tripsService.getFutureTrips().subscribe(async data => {
      if (data.size > 0) {
        await this.navController.navigateRoot('/home/overview');
      }
    });
  }
}
