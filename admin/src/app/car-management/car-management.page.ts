import { Component, Input, OnInit } from '@angular/core';
import { Car, CarFilter } from '@common/models';
import {
  AlertController,
  ModalController,
  NavController,
  Platform,
} from '@ionic/angular';
import { Subscription } from 'rxjs';

import { AddCarPage } from './pages/add-car/add-car.page';
import { FilterCarPage } from './pages/filter-car/filter-car.page';
import { CarManagementService } from './services/car-management.service';

@Component({
  selector: 'app-car-management',
  templateUrl: './car-management.page.html',
  styleUrls: ['./car-management.page.scss'],
})
export class CarManagementPage implements OnInit {
  private backButtonSubscription: Subscription;
  private carFilter: string;

  public cars: Partial<Car>[] = [];
  public isLoading: boolean;
  @Input() tripId: string;
  public getCarsSubscription: Subscription;

  constructor(
    private carManagementService: CarManagementService,
    private modalController: ModalController,
    private alertController: AlertController,
    private platform: Platform,
    private navController: NavController
  ) {
    this.getCars();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        if (!!!this.tripId) {
          await this.navController.navigateBack('home');
        } else {
          this.close();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  public async getCars(): Promise<void> {
    this.isLoading = true;
    if (!!this.getCarsSubscription) {
      this.getCarsSubscription.unsubscribe();
    }
    this.getCarsSubscription = this.carManagementService.getCars().subscribe(
      data => {
        this.cars = data.docs.map(doc => {
          const car = doc.data() as Car;
          car.id = doc.id;
          return car;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  public async searchCars() {
    this.isLoading = true;
    try {
      this.cars = (await this.carManagementService.searchCars(
        this.carFilter
      )).data.map(car => {
        console.log(car);
        return car;
      });
    } catch (error) {
    } finally {
      this.isLoading = false;
    }
  }

  public async openAddEditCarModal(car?: Car): Promise<void> {
    const modal = await this.modalController.create({
      component: AddCarPage,
      componentProps: {
        car: car,
      },
    });
    return await modal.present();
  }

  public async openFilterCarModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterCarPage,
      componentProps: {
        value: this.carFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.carFilter = modalOutput.data;
      this.searchCars();
    });
  }

  public async close(car?: Car): Promise<void> {
    await this.modalController.dismiss(car);
  }

  public async selectCar(car: Car): Promise<void> {
    if (!!this.tripId) {
      car.currentTripId = this.tripId;
      await this.close(car);
    }
  }

  public async deleteCar(car: Car) {
    const alert = await this.alertController.create({
      header: 'تأكيد',
      message: `حذف السيارة رقم: <strong>${car.plateNumber}</strong>`,
      buttons: [
        {
          text: 'إلغاء',
          role: 'cancel',
          cssClass: 'color-dark',
        },
        {
          text: 'حذف',
          role: '',
          cssClass: 'color-danger',
          handler: async () => {
            await this.carManagementService.deleteCar(car.id);
          },
        },
      ],
    });

    await alert.present();
  }
}
