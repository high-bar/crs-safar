import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { TripStatus } from '@common/enums';
import { BackgroundGeolocation, Place, Trip } from '@common/models';
import {
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  LocationService,
  Marker,
  MyLocation,
  Polyline,
} from '@ionic-native/google-maps/ngx';
import { AlertController, NavController, Platform } from '@ionic/angular';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cars-tracking',
  templateUrl: './cars-tracking.page.html',
  styleUrls: ['./cars-tracking.page.scss'],
})
export class CarsTrackingPage {
  private backButtonSubscription: Subscription;
  private map: GoogleMap;

  @ViewChild('mapCanvas', { static: true }) public mapCanvas: ElementRef;
  public isLoading: boolean;
  public currentTrips: Partial<Trip>[] = [];
  public returningTrips: Partial<Trip>[] = [];
  public currentTripsMarkers: { [key: string]: Marker } = {};
  public displayedTripRoute: {
    pickup?: Marker;
    drop?: Marker;
    routePolyline?: Polyline;
  };

  constructor(
    private platform: Platform,
    private alertController: AlertController,
    private ngZone: NgZone,
    private tripsService: TripsService,
    private navController: NavController
  ) {}

  async ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        await this.navController.navigateBack('home');
      }
    );

    await this.platform.ready();
    await this.initMap();
    this.loadCurrentTrips();
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();

    // To fix map being in a modal
    const nodeList = document.querySelectorAll('._gmaps_cdv_');
    for (let k = 0; k < nodeList.length; ++k) {
      nodeList.item(k).classList.remove('_gmaps_cdv_');
    }
  }

  private async initMap(): Promise<void> {
    let currentLocation: MyLocation;
    try {
      currentLocation = await LocationService.getMyLocation();
    } catch (error) {}

    this.map = GoogleMaps.create(this.mapCanvas.nativeElement, {
      controls: {
        compass: true,
        myLocationButton: true,
        myLocation: true,
      },
      camera: {
        zoom: 15,
        target: _.get(currentLocation, 'latLng'),
      },
    });

    this.map
      .on(GoogleMapsEvent.MY_LOCATION_BUTTON_CLICK)
      .subscribe(this.handleMyLocationBtn.bind(this));

    this.map
      .on(GoogleMapsEvent.MAP_CLICK)
      .subscribe(this.clearAllRoutes.bind(this));
  }

  private async handleMyLocationBtn(): Promise<void> {
    this.ngZone.run(async () => {
      try {
        await LocationService.getMyLocation();
      } catch (error) {
        (await this.alertController.create({
          message: 'Please turn on your GPS',
          buttons: ['OK'],
        })).present();
      }
    });
  }

  private async loadCurrentTrips(): Promise<void> {
    this.isLoading = true;

    this.tripsService.getTrips(TripStatus.CURRENT).subscribe(
      data => {
        this.currentTrips = data.snapshot.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          this.updateTripMarker(trip);
          return trip;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );

    this.tripsService.getTrips(TripStatus.PENDING_PAST).subscribe(
      data => {
        this.returningTrips = data.snapshot.docs.map(doc => {
          const trip = doc.data() as Trip;
          trip.id = doc.id;
          this.updateTripMarker(trip);
          return trip;
        });
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  private async updateTripMarker(trip: Partial<Trip>): Promise<void> {
    const tripRoute = _.get(trip, 'route', []);
    const mostRecentLocation: BackgroundGeolocation =
      tripRoute[tripRoute.length - 1];

    if (!!this.currentTripsMarkers[trip.id]) {
      // update marker position
      this.currentTripsMarkers[trip.id].setPosition({
        lat: mostRecentLocation.latitude,
        lng: mostRecentLocation.longitude,
      });
      this.currentTripsMarkers[trip.id].setSnippet(
        this.getTripMarkerSnippet(trip, mostRecentLocation)
      );
    } else {
      // create marker
      const marker: Marker = await this.map.addMarker({
        position: {
          lat: mostRecentLocation.latitude,
          lng: mostRecentLocation.longitude,
        },
        title: this.getTripMarkerTitle(trip),
        snippet: this.getTripMarkerSnippet(trip, mostRecentLocation),
        icon: {
          url: 'assets/icon/car-on-road-marker.png',
          size: {
            width: 40,
            height: 40,
          },
        },
      });

      this.currentTripsMarkers[trip.id] = marker;
    }

    this.currentTripsMarkers[trip.id].removeEventListener(
      GoogleMapsEvent.INFO_CLICK
    );
    this.currentTripsMarkers[trip.id].removeEventListener(
      GoogleMapsEvent.MARKER_CLICK
    );
    this.currentTripsMarkers[trip.id]
      .on(GoogleMapsEvent.INFO_CLICK)
      .subscribe(this.openTripPage.bind(this, trip));
    this.currentTripsMarkers[trip.id]
      .on(GoogleMapsEvent.MARKER_CLICK)
      .subscribe(this.drawTripRoute.bind(this, trip));
  }

  private getTripMarkerTitle(trip: Partial<Trip>): string {
    return `${trip.driver.fullName.toUpperCase()}\n${trip.car.brand.toUpperCase()}-${trip.car.model.toUpperCase()}\n${
      trip.car.plateNumber
    }`;
  }

  private getTripMarkerSnippet(
    trip: Partial<Trip>,
    mostRecentLocation: BackgroundGeolocation
  ): string {
    let averageSpeed = 0;
    trip.route.forEach(location => {
      averageSpeed += location.speed;
    });
    averageSpeed = averageSpeed / trip.route.length;
    return `${this.getSpeedInKMH(
      _.get(mostRecentLocation, 'speed', 0)
    )} km/h\nAvg: ${this.getSpeedInKMH(averageSpeed)} km/h`;
  }

  private async drawTripRoute(trip: Partial<Trip>): Promise<void> {
    if (!!_.get(this.displayedTripRoute, 'routePolyline')) {
      this.displayedTripRoute.routePolyline.remove();
      this.displayedTripRoute.pickup.remove();
      this.displayedTripRoute.drop.remove();
    }
    this.displayedTripRoute = {};
    this.displayedTripRoute.routePolyline = await this.map.addPolyline({
      color: '#000000',
      width: 3,
      points: trip.route
        .filter(location => location.accuracy < 1000)
        .map(location => {
          return {
            lat: location.latitude,
            lng: location.longitude,
          };
        }),
    });

    this.displayedTripRoute.pickup = await this.map.addMarker({
      position: {
        lat: trip.pickupPlace.lat,
        lng: trip.pickupPlace.lng,
      },
      title: trip.pickupPlace.description,
      icon: {
        url: 'assets/icon/pickup-marker.png',
        size: {
          width: 40,
          height: 40,
        },
      },
    });

    this.displayedTripRoute.drop = await this.map.addMarker({
      position: {
        lat: trip.dropPlace.lat,
        lng: trip.dropPlace.lng,
      },
      title: trip.dropPlace.description,
      icon: {
        url: 'assets/icon/drop-marker.png',
        size: {
          width: 40,
          height: 40,
        },
      },
    });
  }

  private clearAllRoutes(): void {
    if (!!_.get(this.displayedTripRoute, 'routePolyline')) {
      this.displayedTripRoute.routePolyline.remove();
      this.displayedTripRoute.pickup.remove();
      this.displayedTripRoute.drop.remove();
    }
  }

  private getSpeedInKMH(speed: number): number {
    return parseFloat(`${(speed / 3.6).toFixed(2)}`);
  }

  private async openTripPage(trip: Trip) {
    await this.navController.navigateForward(`/trip/${trip.id}`);
  }
}
