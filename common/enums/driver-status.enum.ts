export enum DriverStatus {
  IDLE = 'idle',
  IN_TRIP = 'in trip',
  OFF = 'off',
  BLOCKED = 'blocked',
  REJECTED = 'rejected',
}
