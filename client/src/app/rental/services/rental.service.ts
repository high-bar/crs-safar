import { Injectable } from '@angular/core';
import { firestore } from 'firebase';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RentalService {
  constructor() {}

  public getSystemFlags(): Observable<firestore.DocumentSnapshot> {
    return Observable.create(
      (observer: Observer<firestore.DocumentSnapshot>) => {
        firestore()
          .collection('util')
          .doc('flags')
          .onSnapshot(
            querySnapshot => {
              observer.next(querySnapshot);
            },
            error => {
              observer.error(error);
            },
            () => {
              observer.complete();
            }
          );
      }
    );
  }
}
