import { Component, ViewChild } from '@angular/core';
import { FilterTripComponent } from '@app/shared/components/filter-trip/filter-trip.component';
import { TripsService } from '@app/shared/services/trips/trips.service';
import { TripStatus } from '@common/enums';
import { Trip, TripFilter } from '@common/models';
import { Toast } from '@ionic-native/toast/ngx';
import {
  AlertController,
  IonSlides,
  ModalController,
  Platform,
} from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage {
  private backButtonSubscription: Subscription;
  private lastTimeBackPress = 0;
  private exitBackTimeFrame = 2000;
  private tripFilter: TripFilter;
  private newTripsSubscription: Subscription;
  private futureTripsSubscription: Subscription;
  private currentTripsSubscription: Subscription;
  private finishedTripsSubscription: Subscription;
  public newTrips: Partial<Trip>[] = [];
  public futureTrips: Partial<Trip>[] = [];
  public currentTrips: Partial<Trip>[] = [];
  public finishedTrips: Partial<Trip>[] = [];
  public isNewLoading: boolean;
  public isCurrentLoading: boolean;
  public isFutureLoading: boolean;
  public isDeleteMode: boolean;
  public selectedTrips: { [key: string]: boolean } = {};

  @ViewChild('slider', { static: true }) slider: IonSlides;
  public currentTab = 0;
  public isDeleteSelectedLoading: boolean;

  constructor(
    private platform: Platform,
    private toast: Toast,
    private tripsService: TripsService,
    private modalController: ModalController,
    private alertController: AlertController
  ) {
    this.loadTrips();
  }

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(
      0,
      () => {
        if (
          new Date().getTime() - this.lastTimeBackPress <
          this.exitBackTimeFrame
        ) {
          navigator['app'].exitApp();
        } else {
          this.toast
            .show(`Press back again to exit`, '2000', 'bottom')
            .subscribe();

          this.lastTimeBackPress = new Date().getTime();
        }
      }
    );
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  private loadTrips(): void {
    this.loadNewTrips();
    this.loadCurrentTrips();
    this.loadFutureTrips();
  }

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  private async loadNewTrips(): Promise<void> {
    this.isNewLoading = true;
    if (!!this.newTripsSubscription) {
      this.newTripsSubscription.unsubscribe();
    }
    this.newTripsSubscription = this.tripsService
      .getTrips(TripStatus.NEW, this.tripFilter)
      .subscribe(
        data => {
          this.isDeleteMode = data.isFiltered;
          this.newTrips = data.snapshot.docs.map(doc => {
            const trip = doc.data() as Trip;
            trip.id = doc.id;
            return trip;
          });
          this.isNewLoading = false;
        },
        error => {
          console.error(error);
          this.isNewLoading = false;
        }
      );
  }

  private async loadFutureTrips(): Promise<void> {
    this.isFutureLoading = true;
    if (!!this.futureTripsSubscription) {
      this.futureTripsSubscription.unsubscribe();
    }
    this.futureTripsSubscription = this.tripsService
      .getTrips(TripStatus.FUTURE, this.tripFilter)
      .subscribe(
        data => {
          this.isDeleteMode = data.isFiltered;
          this.futureTrips = data.snapshot.docs.map(doc => {
            const trip = doc.data() as Trip;
            trip.id = doc.id;
            return trip;
          });
          this.isFutureLoading = false;
        },
        error => {
          console.error(error);
          this.isFutureLoading = false;
        }
      );
  }

  private async loadCurrentTrips(): Promise<void> {
    this.isCurrentLoading = true;
    if (!!this.currentTripsSubscription) {
      this.currentTripsSubscription.unsubscribe();
    }
    if (!!this.finishedTripsSubscription) {
      this.finishedTripsSubscription.unsubscribe();
    }


    this.currentTripsSubscription = this.tripsService
      .getTrips(TripStatus.CURRENT, this.tripFilter)
      .subscribe(
        data => {
          this.isDeleteMode = data.isFiltered;
          this.currentTrips = data.snapshot.docs.map(doc => {
            const trip = doc.data() as Trip;
            trip.id = doc.id;
            return trip;
          });
          this.isCurrentLoading = false;
        },
        error => {
          console.error(error);
          this.isCurrentLoading = false;
        }
      );

    this.finishedTripsSubscription = this.tripsService
      .getTrips(TripStatus.PENDING_PAST, this.tripFilter)
      .subscribe(
        data => {
          this.isDeleteMode = data.isFiltered;
          this.finishedTrips = data.snapshot.docs.map(doc => {
            const trip = doc.data() as Trip;
            trip.id = doc.id;
            return trip;
          });
          this.isCurrentLoading = false;
        },
        error => {
          console.error(error);
          this.isCurrentLoading = false;
        }
      );
  }

  public async openFilterTripModal(): Promise<void> {
    const tripFilterModal = await this.modalController.create({
      component: FilterTripComponent,
      componentProps: {
        value: this.tripFilter,
      },
    });
    await tripFilterModal.present();

    tripFilterModal.onWillDismiss().then(modalOutput => {
      this.tripFilter = modalOutput.data;
      this.loadTrips();
    });
  }

  public selectAllNewTrips(isAllSelected: boolean): void {
    this.newTrips.forEach(trip => {
      this.selectedTrips[trip.id] = isAllSelected;
    });
  }
  public selectAllFutureTrips(isAllSelected: boolean): void {
    this.futureTrips.forEach(trip => {
      this.selectedTrips[trip.id] = isAllSelected;
    });
  }
  public selectAllCurrentTrips(isAllSelected: boolean): void {
    this.currentTrips.forEach(trip => {
      this.selectedTrips[trip.id] = isAllSelected;
    });
  }

  public async deleteSelected() {
    const alert = await this.alertController.create({
      header: 'تأكيد',
      message: `حذف الرحلات المختارة : <strong></strong>`,
      buttons: [
        {
          text: 'إلغاء',
          role: 'cancel',
          cssClass: 'color-dark',
        },
        {
          text: 'حذف',
          role: '',
          cssClass: 'color-danger',
          handler: async () => {
            this.isDeleteSelectedLoading = true;
            Object.keys(this.selectedTrips).forEach(async tripId => {
              await this.tripsService.deleteSelectedTrips(tripId);
              this.isDeleteSelectedLoading = false;
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
