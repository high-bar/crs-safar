import { Injectable } from '@angular/core';
import { Driver } from '@common/models';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActionSheetController } from '@ionic/angular';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(
    private camera: Camera,
    private actionSheetController: ActionSheetController
  ) {}

  public async initPhotoPicking(): Promise<string> {
    const cameraOptions: CameraOptions = {
      quality: 70,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      saveToPhotoAlbum: true,
      targetWidth: 500,
      targetHeight: 500,
    };

    return new Promise<string>(async (resolve, reject) => {
      const actionSheet = await this.actionSheetController.create({
        header: 'Choose photo source',
        cssClass: 'action-sheet',
        buttons: [
          {
            text: 'Camera',
            role: 'destructive',
            icon: 'camera',
            handler: () => {
              cameraOptions.sourceType = this.camera.PictureSourceType.CAMERA;
              this.pickPhoto(cameraOptions, resolve, reject);
            },
          },
          {
            text: 'Gallery',
            icon: 'images',
            handler: () => {
              cameraOptions.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
              this.pickPhoto(cameraOptions, resolve, reject);
            },
          },
          {
            text: 'Cancel',
            role: 'cancel',
            icon: 'close',
            handler: () => {
              reject();
            },
          },
        ],
      });
      await actionSheet.present();
    });
  }

  public async pickPhoto(
    cameraOptions: CameraOptions,
    resolve?: Function,
    reject?: Function
  ): Promise<string | void> {
    if (!!!reject) {
      return this.camera.getPicture(cameraOptions);
    } else {
      try {
        resolve(await this.camera.getPicture(cameraOptions));
      } catch (error) {
        reject(error);
        console.error('pickPhoto error', error);
      }
    }
  }

  public editProfile(profile?: Partial<Driver>): Promise<void> {
    return firestore()
      .collection('users')
      .doc(profile.uid)
      .update(profile);
  }
}
