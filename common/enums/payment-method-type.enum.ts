export enum PaymentMethodType {
  CARD = 'card',
  FAWRY = 'fawry',
  CASH = 'cash',
}
