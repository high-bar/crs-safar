import app from "./app";
//import DbClient = require("../const/DbClient");
import { config } from "./const/config";
import * as https from "https";
import * as fs from "fs";

const httpsOptions = {
    key: fs.readFileSync('src/cert/safarprivatetaxi_com.key'),
    cert: fs.readFileSync('src/cert/safarprivatetaxi.csr'),
    ca: fs.readFileSync('src/cert/safarprivatetxai.ca'),
}

https.createServer(httpsOptions, app).listen(config.port, () => {
    console.log('node server https started and listening on port ' + config.port);
})

