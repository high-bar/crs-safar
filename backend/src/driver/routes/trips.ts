import { DriverTrips } from '../controllers/my-trips';

import { Request, Response, NextFunction } from 'express';
import { config } from '../../const/config';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let secretKey = config.secretKey;

export class DriverTripRoutes {
  public DriverTrips: DriverTrips = new DriverTrips();

  public routes(app: any): void {
    //
    function createToken(User: any) {
      var token = jwt.sign(
        {
          id: User._id,
          fullName: User.fullName,
          email: User.email,
        },
        secretKey
      );
      return token;
    }
    //

    // start Driver route
    // driver trips history
    app
      .route('/driver/triphistory')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });

          ///////////
        }
      }, this.DriverTrips.DriverTripHistory.bind(this.DriverTrips));
    ////

    // Driver Active Trips
    app
      .route('/driver/getactivetrips')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.DriverTrips.DriverActiveTrips.bind(this.DriverTrips));
    ////
    ////

    // get driver trip details
    app
      .route('/driver/tripdetails')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.DriverTrips.DriverTripDetails.bind(this.DriverTrips));
    ////

    // get driver trips pending
    app
      .route('/driver/pendingtrips')
      .get((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;
              next();
            }
          });

          ///////////
        }
      }, this.DriverTrips.DriverpendingTrips.bind(this.DriverTrips));
    ////

    // history with index //
    app
      .route('/driver/gettriphistory1')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.DriverTrips.GetDriverTripHistory1.bind(this.DriverTrips));
    //////////////
    app
      .route('/driver/gettriphistory2')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.DriverTrips.GetDriverTripHistory2.bind(this.DriverTrips));
    //////////////
    app
      .route('/driver/updatetrip')
      .post((req: Request, res: Response, next: NextFunction) => {
        // middleware
        let token = req.headers['x-access-token'];
        if (!token) {
          res.status(403).json({
            error: 'No Token Provided',
          });
        } else {
          /////////
          jwt.verify(token, secretKey, function(err: any, decoded: any) {
            if (err) {
              res.status(403).json({
                error: "Failed to authenticate"
              });
            } else {
              //
              (<any>req).decoded = decoded;

              next();
            }
          });
          ///////////
        }
      }, this.DriverTrips.DriverUpdateTrip.bind(this.DriverTrips));
    //////////////

    // end route
  }

  //end class
}
