import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ClientFilter } from '@common/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-filter-client',
  templateUrl: './filter-client.page.html',
  styleUrls: ['./filter-client.page.scss'],
})
export class FilterClientPage implements OnInit {
  public isLoading: boolean;
  [x: string]: any;
  @Input() public value: ClientFilter;
  // public readonly CarTypeEnum: typeof CarType = CarType;

  public filterClientForm: FormGroup = this.formBuilder.group({
    fullName: [undefined],
    email: [undefined],
    phone: [undefined],
    rate: [undefined],
  });
  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    if (!!this.value) {
      // pass only values used in the form
      this.filterClientForm.patchValue({
        fullName: this.value.fullName,
        email: this.value.email,
        phone: this.value.phone,
        rate: this.value.rate,
      });
    }
  }

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async submitFilterClient(): Promise<void> {
    this.isLoading = true;
    const clientFilter: ClientFilter = {
      fullName: this.filterClientForm.value.fullName,
      email: this.filterClientForm.value.email,
      phone: this.filterClientForm.value.phone,
      rate: this.filterClientForm.value.rate,
    };
    await this.modalController.dismiss(clientFilter);
    this.isLoading = true;
  }
}
