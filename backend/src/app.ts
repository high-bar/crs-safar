import express from 'express';
import * as bodyParser from 'body-parser';
let path = require('path');

import { ClientRoutes } from './client/routes/user-routes';
import { CountryRoutes } from './client/routes/country-routes';
import { BookingTripRoutes } from './client/routes/bookingtrip-routes';
import { PricesRoutes } from './admin/routes/prices-route';
import { GovernorateRoutes } from './client/routes/governorate-route';
import { DriverRoutes } from './driver/routes/driver-routes';
import { AdminRoutes } from './admin/routes/admin-routes';
import { DriverTripRoutes } from './driver/routes/trips';
import { UserReviewRoutes } from './client/routes/review-route';
import { DriverReviewRoutes } from './driver/routes/review-route';
import { TripRoutes } from './admin/routes/trip-routes';
import { CarRoutes } from './admin/routes/car-routes';

// import cors from "@types/cors";
// let cors = require('@types/cors');
// const cors = require('cors');
import cors from 'cors';

import mongoose = require('mongoose');

import { config } from './const/config';

class App {
  public app: express.Application;
  public ClientRoutes: ClientRoutes = new ClientRoutes();
  public CountryRoutes: CountryRoutes = new CountryRoutes();
  public BookingTripRoutes: BookingTripRoutes = new BookingTripRoutes();
  public PricesRoutes: PricesRoutes = new PricesRoutes();
  public GovernorateRoutes: GovernorateRoutes = new GovernorateRoutes();
  public DriverRoutes: DriverRoutes = new DriverRoutes();
  public AdminRoutes: AdminRoutes = new AdminRoutes();
  public DriverTripRoutes: DriverTripRoutes = new DriverTripRoutes();
  public UserReviewRoutes: UserReviewRoutes = new UserReviewRoutes();
  public DriverReviewRoutes: DriverReviewRoutes = new DriverReviewRoutes();
  public TripRoutes: TripRoutes = new TripRoutes();
  public CarRoutes: CarRoutes = new CarRoutes();

  public mongoUrl: string = config.database;

  constructor() {
    this.app = express();
    this.config();
    this.mongoSetup();
    this.ClientRoutes.routes(this.app);
    this.CountryRoutes.routes(this.app);
    this.BookingTripRoutes.routes(this.app);
    this.PricesRoutes.routes(this.app);
    this.GovernorateRoutes.routes(this.app);
    this.DriverRoutes.routes(this.app);
    this.AdminRoutes.routes(this.app);
    this.DriverTripRoutes.routes(this.app);
    this.UserReviewRoutes.routes(this.app);
    this.DriverReviewRoutes.routes(this.app);
    this.TripRoutes.routes(this.app);
    this.CarRoutes.routes(this.app);
  }

  private config(): void {
    // support application/json type post data
    this.app.use(bodyParser.json());
    //support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }));
    // cors
    //options for cors midddleware
    const options: cors.CorsOptions = {
      allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
      ],
      methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
      origin: "*",
      preflightContinue: false,
    };

    this.app.use(cors(options));

    this.app.use(express.static(__dirname + '/public'));
  }

  private mongoSetup(): void {
    mongoose.Promise = global.Promise;
    mongoose.connect(this.mongoUrl);
  }
}

export default new App().app;
