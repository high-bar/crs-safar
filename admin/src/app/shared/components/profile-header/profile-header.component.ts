import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '@common/models';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss'],
})
export class ProfileHeaderComponent {
  public readonly avatarPlaceHolder =
    '/assets/images/profile-picture-placeholder.png';
  @Input() public user: User;
  @Input() public size: string;
  @Input() public isConnectable: boolean;

  constructor(
    public sanitizer: DomSanitizer,
    private inAppBrowser: InAppBrowser
  ) {}

  public avatarLoadError(): void {
    this.user.pictureURL = this.avatarPlaceHolder;
  }

  public async openProfilePicture(picture): Promise<void> {
    if (this.size !== 'small' && !!picture) {
      this.inAppBrowser.create(picture, '_blank', {
        hideurlbar: 'yes',
        hidenavigationbuttons: 'yes',
        toolbarcolor: '#b50003',
        closebuttoncolor: '#ffffff',
        zoom: 'no',
      });
    }
  }
}
