import { CONFIGS } from './configs.const';
import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  ROOT_API_URL: 'https://safarprivatetaxi.com',
  googleAPIKey: 'AIzaSyAMeIayb9KnPXwK-VJ6Poq4vv-h5n9LkQE',
  configs: CONFIGS,
};
