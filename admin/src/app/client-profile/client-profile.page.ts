import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '@app/clients-management/services/client-service/client.service';
import { AuthService } from '@app/shared/services/auth/auth.service';
import { Client } from '@common/models';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.page.html',
  styleUrls: ['./client-profile.page.scss'],
})
export class ClientProfilePage implements OnInit {
  public client: Partial<Client>;
  public isLoading: boolean;
  public currentClients: Client[];
  public isBlockLoading: boolean;
  public currentTab = 0;
  @ViewChild('slider', { static: true }) slider: IonSlides;

  constructor(
    private clientService: ClientService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {
    this.loadClientData(this.activatedRoute.snapshot.params.id);
  }

  ngOnInit() {}

  public async tabChange(newTabIndex?) {
    if (!!newTabIndex) {
      this.slider.slideTo(newTabIndex);
    } else {
      this.currentTab = await this.slider.getActiveIndex();
    }
  }

  public loadClientData(uid): void {
    this.isLoading = true;
    this.clientService.getClientByID(uid).subscribe(
      doc => {
        this.client = doc.data() as Client;
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  public async toggleBlock(isBlocked: boolean): Promise<void> {
    this.isBlockLoading = true;
    try {
      await this.clientService.blockDriver(this.client, isBlocked);
    } catch (error) {
      console.error(error);
    } finally {
      this.isBlockLoading = false;
    }
  }

  public async submitForgotPassword(): Promise<void> {
    this.isLoading = true;

    try {
      await this.authService.resetPassword(this.client.email);
    } catch (error) {
    } finally {
      this.isLoading = false;
    }
  }
}
